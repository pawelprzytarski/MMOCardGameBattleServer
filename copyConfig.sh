if [ ! -e "${GAME_SERVER_CONF_DIR}/settings.json" ]; then
	cp ${GAME_SERVER_DEFAULT_CONF_DIR}/*.json ${GAME_SERVER_CONF_DIR}/ &&
	cat ${GAME_SERVER_CONF_DIR}/settings.json | grep "innerServiceToken" &&
	echo "Files copied" &&
	exit 0;
	exit 1;
fi;