FROM openjdk:10-jdk

ARG START_ARG

ENV START_ARG $START_ARG

VOLUME [ "/volume/tmp", "/volume/dump", "/volume/conf"]

ADD ./target/*.jar /usr/bin/mmocardgame/
ADD ./*.json /volume/conf/
ADD ./*.json ./*.sh /volume/defaultConf/
ADD ./startScript.sh /usr/bin/mmocardgame/

RUN /bin/bash -c "cat /volume/defaultConf/preSettings.json | sed s/#SECURE_TOKEN#/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)/ >> /volume/defaultConf/settings.json;chmod +x /volume/defaultConf/copyConfig.sh && chmod +x /usr/bin/mmocardgame/startScript.sh"

ENV GAME_SERVER_CONF_DIR=/volume/conf \
    GAME_SERVER_DEFAULT_CONF_DIR=/volume/defaultConf \
    GAME_SERVER_TEMP_DIR=/volume/tmp \
    GAME_SERVER_DUMP_DIR=/volume/dump

EXPOSE 1410 1914

ENTRYPOINT ["/usr/bin/mmocardgame/startScript.sh", "$START_ARG"]
