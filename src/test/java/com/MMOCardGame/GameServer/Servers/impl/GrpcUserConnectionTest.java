package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionService;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class GrpcUserConnectionTest {

    @Test
    void close_nothingRegistered_completeWithoutThrowing() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");

        connection.close();
    }

    @Test
    void sendMessage_registerStreamObserver_invokeOnNext() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");
        StreamObserver<StreamingMessage> streamObserver = mock(StreamObserver.class);
        connection.registerReturningMessagesStream(streamObserver);
        AttackExecution gameStateMessage = AttackExecution.newBuilder().build();

        connection.sendMessage(gameStateMessage);


        StreamingMessage streamingMessage = StreamingMessage.newBuilder()
                .setType(1)
                .setAttackExecution(gameStateMessage).build();
        verify(streamObserver).onNext(streamingMessage);
    }

    @Test
    void sendMessage_registerStreamObserverTwoTypes_invokeOnNext() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");
        StreamObserver<StreamingMessage> streamObserver = mock(StreamObserver.class);
        connection.registerReturningMessagesStream(streamObserver);
        AttackExecution gameStateMessage = AttackExecution.newBuilder().build();
        PlayerGameEnd playerGameEnd = PlayerGameEnd.newBuilder().build();

        connection.sendMessage(gameStateMessage);
        connection.sendMessage(playerGameEnd);

        StreamingMessage streamingMessage1 = StreamingMessage.newBuilder()
                .setType(1)
                .setAttackExecution(gameStateMessage).build();
        StreamingMessage streamingMessage2 = StreamingMessage.newBuilder()
                .setType(2)
                .setPlayerGameEnd(playerGameEnd).build();
        verify(streamObserver).onNext(streamingMessage1);
        verify(streamObserver).onNext(streamingMessage2);
    }

    @Test
    void registerGameStateUpdateMessagesStreamObserverAndClose_invokeOnCompleted() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");
        StreamObserver<StreamingMessage> streamObserver = mock(StreamObserver.class);
        connection.registerReturningMessagesStream(streamObserver);

        connection.close();

        verify(streamObserver).onCompleted();
    }

    @Test
    void sendMessage_noRegisterStreamObserver_invokeOnNextAfterRegister() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");
        StreamObserver<StreamingMessage> streamObserver = mock(StreamObserver.class);
        AttackExecution gameStateMessage = AttackExecution.newBuilder().build();

        connection.sendMessage(gameStateMessage);
        connection.close();
        connection.registerReturningMessagesStream(streamObserver);

        StreamingMessage streamingMessage = StreamingMessage.newBuilder()
                .setType(1).setAttackExecution(gameStateMessage).build();
        verify(streamObserver).onNext(streamingMessage);
        verify(streamObserver).onCompleted();
    }

    @Test
    void sendMessage_AllTypes_invokeOnNextAfterRegister() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");
        StreamObserver<StreamingMessage> streamObserver = mock(StreamObserver.class);

        connection.registerReturningMessagesStream(streamObserver);

        List<Object> messages = new ArrayList<>();

        messages.add(AttackExecution.newBuilder().build());
        messages.add(PlayerGameEnd.newBuilder().build());
        messages.add(HandChanged.newBuilder().build());
        messages.add(PhaseChanged.newBuilder().build());
        messages.add(CardMoved.newBuilder().build());
        messages.add(CardUsed.newBuilder().build());
        messages.add(PlayerDefenceChanged.newBuilder().build());
        messages.add(CardStatisticsChanged.newBuilder().build());
        messages.add(HeroAbilityUsed.newBuilder().build());
        messages.add(CardCreated.newBuilder().build());
        messages.add(StackActionCompleted.newBuilder().build());
        messages.add(AttackersDefendersSelected.newBuilder().build());
        messages.add(TriggerActivated.newBuilder().build());
        messages.add(CardRevealed.newBuilder().build());
        messages.add(PlayerLinkPointsUpdated.newBuilder().build());

        messages.forEach(connection::sendMessage);

        verify(streamObserver, times(15)).onNext(any());
    }

    @Test
    void sendMessage_incorrectType_throwsIllegalArgumentException() {
        GrpcUserConnection connection = new GrpcUserConnection("Test");
        StreamObserver<StreamingMessage> streamObserver = mock(StreamObserver.class);

        connection.registerReturningMessagesStream(streamObserver);
        assertThrows(IllegalArgumentException.class, () -> connection.sendMessage(0));
    }

    @Test
    void getAllGameState_hasInitialGameState_returnMessageWithInitialState() {
        GameSessionService gameSessionService = getGameSessionServiceMock();
        GrpcUserConnection grpcUserConnection = new GrpcUserConnection("Test");
        grpcUserConnection.setUserId(0);
        grpcUserConnection.registerSessionService(gameSessionService);

        GameStateMessage result = grpcUserConnection.getWholeGameState();

        GameStateMessage expected = getExpectedGameStateMessageForInitialGameState();
        assertEquals(expected, result);
    }

    private GameStateMessage getExpectedGameStateMessageForInitialGameState() {
        return GameStateMessage.newBuilder()
                .setActivePlayer(-1)
                .setCurrentPhase(GamePhase.Waiting)
                .addPlayers(PlayerState.newBuilder()
                        .setId(0)
                        .setHeroId(1)
                        .setDefence(GameConstants.getStartingLife())
                        .setName("Test")
                        .addKnownCards(getPlayerCard(1))
                        .addKnownCards(getPlayerCard(2))
                        .addKnownCards(getPlayerCard(3))
                        .setCardsInLibraryCount(3)
                        .setCardsInHandCount(0)
                        .build())
                .addPlayers(PlayerState.newBuilder()
                        .setId(1)
                        .setHeroId(1)
                        .setDefence(GameConstants.getStartingLife())
                        .setName("Test2")
                        .setCardsInLibraryCount(3)
                        .setCardsInHandCount(0)
                        .addKnownCards(getPlayerCard(4))
                        .build())
                .build();
    }

    private GameSessionService getGameSessionServiceMock() {
        GameState gameState = new GameState();
        gameState.setListOfActiveUsers(Arrays.asList("Test", "Test2"));
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2), getCard(3)));
        gameState.setCardsForUser(1, Arrays.asList(getCardRevealed(4), getCard(5), getCard(6)));
        gameState.setHeroForUser(0, new Player(1, 0, GameConstants.getStartingLife(), new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}));
        gameState.setHeroForUser(1, new Player(1, 1, GameConstants.getStartingLife(), new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}));
        gameState.setPlayersOrder(new int[]{0, 1});
        GameSessionService gameSessionService = mock(GameSessionService.class);
        when(gameSessionService.getAllGameState()).thenReturn(gameState);
        return gameSessionService;
    }

    PlayerCard getPlayerCard(int instanceId) {
        return PlayerCard.newBuilder()
                .setCardCosts(CardCosts.newBuilder().build())
                .setCardId(1)
                .setInstanceId(instanceId)
                .setPositionIndex((instanceId - 1) % 3)
                .setDefence(0)
                .setAttack(0)
                .setPosition(CardPosition.Library)
                .setOverloadedPoints(0)
                .build();
    }

    Card getCard(int instanceId) {
        return new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
    }

    Card getCardRevealed(int instanceId) {
        Card card = new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
        card.setRevealed(true);
        return card;
    }
}