package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CallbackUrl;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.NewSessionData;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.Reply;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.UserData;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GrpcServiceServerServiceTest {

    @Test
    void registerGameSession_emptyRequest_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    private GrpcServiceServerService getServerServiceWithMockedDependencies() {
        return new GrpcServiceServerService(getMockOfTokenContainer(), getMockOfGameSessionManager(), getMockOfAppConfiguration());
    }

    private AppConfiguration getMockOfAppConfiguration() {
        AppConfiguration configuration = mock(AppConfiguration.class);
        when(configuration.getInnerServiceToken()).thenReturn("AccessToken");
        return configuration;
    }

    public UserTokenContainer getMockOfTokenContainer() {
        return mock(UserTokenContainer.class);
    }

    public GameSessionManager getMockOfGameSessionManager() {
        return mock(GameSessionManager.class);
    }

    @Test
    void registerGameSession_oneUserAndCorrectAccessToken_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Collections.singletonList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_TwoUsersWithoutMatchTypeAndCorrectAccessToken_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_TwoUsersAndMatchTypeAndCorrectAccessToken_returnCodeCreated() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Single)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Collections.singletonList(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_ThreeUsersAndMatchTypeTeamAndCorrectAccessToken_returnCodeCreated() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Single)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_ThreeUsersAndTeamMatchTypeTeamAndCorrectAccessToken_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_FourUsersAndSingleMatchTypeTeamAndCorrectAccessToken_returnCodeCreated() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken4").setUsername("Test4").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Single)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_FourUsersAndTeamMatchTypeTeamAndCorrectAccessToken_returnCodeCreated() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken4").setUsername("Test4").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_ThreeUsersAndTeamMatchTypeTeamAndCorrectAccessToken_notRegisterGameSession() {
        GameSessionManager sessionManager = mock(GameSessionManager.class);
        GrpcServiceServerService serverService = new GrpcServiceServerService(getMockOfTokenContainer(), sessionManager, getMockOfAppConfiguration());
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        verify(sessionManager, times(0)).registerNewGameSession(any());
    }

    @Test
    void registerGameSession_ThreeUsersAndTeamMatchTypeTeamAndCorrectAccessToken_notRegisterTokens() {
        UserTokenContainer tokenContainer = mock(UserTokenContainer.class);
        GrpcServiceServerService serverService = new GrpcServiceServerService(tokenContainer, getMockOfGameSessionManager(), getMockOfAppConfiguration());
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        verify(tokenContainer, times(0)).registerNewUserToken(any(), any());
    }

    @Test
    void registerGameSession_CorrectUserDataWithoutCards_notRegisterTokens() {
        UserTokenContainer tokenContainer = mock(UserTokenContainer.class);
        GrpcServiceServerService serverService = new GrpcServiceServerService(tokenContainer, getMockOfGameSessionManager(), getMockOfAppConfiguration());
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        verify(tokenContainer, times(0)).registerNewUserToken(any(), any());
    }

    @Test
    void registerGameSession_CorrectUserDataWithoutCards_returnsBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_CorrectDataWithoutSessionId_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken4").setUsername("Test4").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Team)

                .build();

        serverService.registerGameSession(sessionData, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void registerGameSession_ThreeUsersAndSingleMatchTypeTeamAndCorrectAccessToken_registerGameSession() {
        GameSessionManager sessionManager = mock(GameSessionManager.class);
        GrpcServiceServerService serverService = new GrpcServiceServerService(getMockOfTokenContainer(), sessionManager, getMockOfAppConfiguration());
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).setHeroId(1).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).setHeroId(2).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).setHeroId(3).build())
                .setGameType(GameSessionSettings.MatchType.Single)

                .setSessionId(3)
                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        GameSessionSettings sessionSettings = new GameSessionSettings();
        sessionSettings.setListOfUsers(Arrays.asList("Test", "Test2", "Test3"));
        sessionSettings.setTypeOfMatch(GameSessionSettings.MatchType.Single);
        sessionSettings.setSelectedHerosForUser("Test", 1);
        sessionSettings.setSelectedHerosForUser("Test2", 2);
        sessionSettings.setSelectedHerosForUser("Test3", 3);
        sessionSettings.setCardsForUser("Test", getCardIds());
        sessionSettings.setCardsForUser("Test2", getCardIds());
        sessionSettings.setCardsForUser("Test3", getCardIds());
        sessionSettings.setSessionId(3);

        verify(sessionManager, times(1)).registerNewGameSession(ArgumentMatchers.eq(sessionSettings));
    }

    @Test
    void registerGameSession_ThreeUsersAndSingleMatchTypeTeamAndCorrectAccessToken_registerTokens() {
        List<String> usernames = new ArrayList<>();
        List<String> tokens = new ArrayList<>();
        UserTokenContainer tokenContainer = mock(UserTokenContainer.class);
        doAnswer(answer -> {
            usernames.add(answer.getArgument(0));
            tokens.add(answer.getArgument(1));
            return answer;
        }).when(tokenContainer).registerNewUserToken(any(), any());
        GrpcServiceServerService serverService = new GrpcServiceServerService(tokenContainer, getMockOfGameSessionManager(), getMockOfAppConfiguration());
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        NewSessionData sessionData = NewSessionData.newBuilder()
                .addUsers(UserData.newBuilder().setToken("TestToken").setUsername("Test").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken2").setUsername("Test2").addAllCardIds(getCardIds()).build())
                .addUsers(UserData.newBuilder().setToken("TestToken3").setUsername("Test3").addAllCardIds(getCardIds()).build())
                .setGameType(GameSessionSettings.MatchType.Single)

                .setSessionId(3).build();

        serverService.registerGameSession(sessionData, responseObserver);

        assertEquals(Arrays.asList("Test", "Test2", "Test3"), usernames);
        assertEquals(Arrays.asList("TestToken", "TestToken2", "TestToken3"), tokens);
    }

    public List<Integer> getCardIds() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 45; i++)
            list.add(i);
        return list;
    }

    @Test
    void RegisterCallbackUrl_noCallbackToken_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());


        CallbackUrl callbackUrl = CallbackUrl.newBuilder()
                .setCallbackUrl("http://Test")

                .build();

        serverService.registerCallbackUrl(callbackUrl, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void RegisterCallbackUrl_noCallbackUrl_returnCodeBadRequest() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        CallbackUrl callbackUrl = CallbackUrl.newBuilder()
                .setCallbackSecureToken("CallbackToken")

                .build();

        serverService.registerCallbackUrl(callbackUrl, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
        assertEquals(expected, replyList);
    }

    @Test
    void RegisterCallbackUrl_noCallbackUrl_noUpdateAppConfiguration() {
        AppConfiguration configuration = mock(AppConfiguration.class);
        GrpcServiceServerService serverService = new GrpcServiceServerService(getMockOfTokenContainer(), getMockOfGameSessionManager(), configuration);
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        CallbackUrl callbackUrl = CallbackUrl.newBuilder()
                .setCallbackSecureToken("CallbackToken")

                .build();

        serverService.registerCallbackUrl(callbackUrl, responseObserver);

        verify(configuration, times(0)).setCallbackUrl(anyString());
        verify(configuration, times(0)).setCallbackToken(anyString());
    }

    @Test
    void RegisterCallbackUrl_correctData_updateAppConfiguration() {
        AppConfiguration configuration = mock(AppConfiguration.class);
        GrpcServiceServerService serverService = new GrpcServiceServerService(getMockOfTokenContainer(), getMockOfGameSessionManager(), configuration);
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);

        CallbackUrl callbackUrl = CallbackUrl.newBuilder()
                .setCallbackUrl("http://Test")
                .setCallbackSecureToken("CallbackToken")

                .build();

        serverService.registerCallbackUrl(callbackUrl, responseObserver);

        verify(configuration, times(1)).setCallbackUrl("http://Test");
        verify(configuration, times(1)).setCallbackToken("CallbackToken");
    }

    @Test
    void RegisterCallbackUrl_correctDat_returnCodeCreated() {
        GrpcServiceServerService serverService = getServerServiceWithMockedDependencies();
        List<Reply> replyList = new ArrayList<>();
        StreamObserver<Reply> responseObserver = mock(StreamObserver.class);
        doAnswer(invocationOnMock -> replyList.add(invocationOnMock.getArgument(0))).when(responseObserver).onNext(any());

        CallbackUrl callbackUrl = CallbackUrl.newBuilder()
                .setCallbackUrl("http://Test")
                .setCallbackSecureToken("CallbackToken")

                .build();

        serverService.registerCallbackUrl(callbackUrl, responseObserver);

        List<Reply> expected = Arrays.asList(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
        assertEquals(expected, replyList);
    }
}