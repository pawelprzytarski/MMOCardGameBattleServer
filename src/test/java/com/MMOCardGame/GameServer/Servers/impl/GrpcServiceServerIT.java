package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CallbackUrl;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.InnerServerGrpc;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.Reply;
import io.grpc.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GrpcServiceServerIT {

    private final int serverPort = 1914;

    @Test
    void RegisterCallbackUrl_noToken_breakConnection() throws IOException {
        AppConfiguration appConfiguration = getConfiguration();
        UserTokenContainer tokenContainer = mock(UserTokenContainer.class);
        GrpcServiceServer serviceServer = new GrpcServiceServer(appConfiguration, tokenContainer, getMockOfGameSession());

        serviceServer.start();

        try {
            Channel channel = getChannel();
            InnerServerGrpc.InnerServerBlockingStub serverStub = InnerServerGrpc.newBlockingStub(channel);

            CallbackUrl message = CallbackUrl.newBuilder().setCallbackSecureToken("TestToken").setCallbackUrl("http://Test").build();

            assertThrows(StatusRuntimeException.class, () -> serverStub.registerCallbackUrl(message));
        } finally {
            serviceServer.stop();
            serviceServer.awaitTermination();
        }
    }

    private Channel getChannel() {
        return ManagedChannelBuilder.forAddress("localhost", serverPort).usePlaintext(true).build();
    }

    public GameSessionManager getMockOfGameSession() {
        return mock(GameSessionManager.class);
    }

    private AppConfiguration getConfiguration() {
        AppConfiguration appConfiguration = mock(AppConfiguration.class);
        when(appConfiguration.getServiceServerPort()).thenReturn(serverPort);
        when(appConfiguration.getInnerServiceToken()).thenReturn("TestToken");
        return appConfiguration;
    }

    @Test
    void RegisterCallbackUrl_withToken_returnCoreCreated() throws IOException {
        AppConfiguration appConfiguration = getConfiguration();
        UserTokenContainer tokenContainer = mock(UserTokenContainer.class);
        GrpcServiceServer serviceServer = new GrpcServiceServer(appConfiguration, tokenContainer, getMockOfGameSession());

        serviceServer.start();

        try {
            Channel channel = getChannel();
            InnerServerGrpc.InnerServerBlockingStub serverStub = getServerBlockingStubWithAddingHeader(channel, "TestToken");

            CallbackUrl message = CallbackUrl.newBuilder().setCallbackSecureToken("TestToken").setCallbackUrl("http://Test").build();
            Reply reply = serverStub.registerCallbackUrl(message);

            assertEquals(ErrorCodes.Created, reply.getErrorCode());
        } finally {
            serviceServer.stop();
            serviceServer.awaitTermination();
        }
    }

    private InnerServerGrpc.InnerServerBlockingStub getServerBlockingStubWithAddingHeader(Channel channel, String token) {
        return InnerServerGrpc.newBlockingStub(channel)
                .withInterceptors(new ClientInterceptor() {
                    @Override
                    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, Channel channel1) {
                        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel1.newCall(methodDescriptor, callOptions)) {
                            @Override
                            public void start(Listener<RespT> responseListener, Metadata headers) {
                                headers.put(Metadata.Key.of("Token", Metadata.ASCII_STRING_MARSHALLER), token);
                                super.start(responseListener, headers);
                            }
                        };
                    }
                });
    }

    @Test
    void RegisterCallbackUrl_wrongToken_breakConnection() throws IOException {
        AppConfiguration appConfiguration = getConfiguration();
        UserTokenContainer tokenContainer = mock(UserTokenContainer.class);
        GrpcServiceServer serviceServer = new GrpcServiceServer(appConfiguration, tokenContainer, getMockOfGameSession());

        serviceServer.start();

        try {
            Channel channel = getChannel();
            InnerServerGrpc.InnerServerBlockingStub serverStub = getServerBlockingStubWithAddingHeader(channel, "WrongToken");

            CallbackUrl message = CallbackUrl.newBuilder().setCallbackSecureToken("TestToken").setCallbackUrl("http://Test").build();
            assertThrows(StatusRuntimeException.class, () -> serverStub.registerCallbackUrl(message));
        } finally {
            serviceServer.stop();
            serviceServer.awaitTermination();
        }
    }
}