package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;
import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.exceptions.NotFoundConnectionException;
import com.MMOCardGame.GameServer.Servers.exceptions.UserDuplicationException;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

class GrpcUserServerServiceTest {

    private UserConnectionsManager getUserConnectionManager() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        when(connectionsManager.createConnection("Existing", "Test")).thenThrow(new UserDuplicationException());
        when(connectionsManager.createConnection("Test", "Incorrect")).thenThrow(new AuthenticationException());
        UserConnection mock1 = getMockUserConnection("Test");
        when(connectionsManager.createConnection("Test", "Test")).thenReturn(mock1);
        UserConnection mock2 = getMockUserConnection("Test2");
        when(connectionsManager.createConnection("Test2", "Test")).thenReturn(mock2);
        when(connectionsManager.reconnectUserAndGetConnection("Test", "Test")).thenThrow(new NotFoundConnectionException());
        when(connectionsManager.reconnectUserAndGetConnection("Existing", "Incorrect")).thenThrow(new AuthenticationException());
        UserConnection mockExisting = getMockUserConnection("Existing");
        when(connectionsManager.reconnectUserAndGetConnection("Existing", "Test")).thenReturn(mockExisting);
        return connectionsManager;
    }

    UserTokenContainer getMockUserTokenContainer() {
        return mock(UserTokenContainer.class);
    }

    private UserConnection getMockUserConnection(String test) {
        UUID uuid = UUID.randomUUID();
        GrpcUserConnection connection = mock(GrpcUserConnection.class);
        when(connection.getUserName()).thenReturn(test);
        when(connection.getToken()).thenReturn(uuid);
        when(connection.getWholeGameState()).thenReturn(GameStateMessage.newBuilder().build());
        return connection;
    }

    @Test
    void connect_correctUserConnection_returnsNewConnection() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        Hello message = Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);

        serverService.connect(message, streamObserver);

        verify(streamObserver).onNext(any(ConnectResponse.class));
    }

    @Test
    void connect_correctUserConnection_returns0Code() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        Hello message = Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.connect(message, streamObserver);

        assertEquals(0, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void connect_correctUserTwoConnections_returnsTwoDifferentConnections() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        Hello firstMessage = Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build();
        Hello secondMessage = Hello.newBuilder().setUserName("Test2").setUserPasswordToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.connect(firstMessage, streamObserver);
        serverService.connect(secondMessage, streamObserver);

        verify(streamObserver, times(2)).onNext(any(ConnectResponse.class));

        assertNotEquals(arguments.get(0), arguments.get(1));
    }

    @Test
    void connect_correctUserConnection_returnsTwoDifferentConnections() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        Hello firstMessage = Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build();
        Hello secondMessage = Hello.newBuilder().setUserName("Test2").setUserPasswordToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.connect(firstMessage, streamObserver);
        serverService.connect(secondMessage, streamObserver);

        verify(streamObserver, times(2)).onNext(any(ConnectResponse.class));

        assertNotEquals(arguments.get(0), arguments.get(1));
    }

    @Test
    void connect_ExistingUserConnection_returnsAccessForbiddenCode() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        Hello firstMessage = Hello.newBuilder().setUserName("Existing").setUserPasswordToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.connect(firstMessage, streamObserver);

        assertEquals(com.MMOCardGame.GameServer.Servers.ErrorCodes.AccessForbidden, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void connect_incorrectUserConnection_returnsAccessForbiddenCode() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());

        Hello firstMessage = Hello.newBuilder().setUserName("Test").setUserPasswordToken("Incorrect").build();

        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.connect(firstMessage, streamObserver);

        assertEquals(com.MMOCardGame.GameServer.Servers.ErrorCodes.AccessForbidden, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void connect_innerProblem_returnsInternalProblemCode() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        doThrow(RuntimeException.class).when(connectionsManager).createConnection(anyString(), anyString());
        GrpcUserServerService serverService = new GrpcUserServerService(connectionsManager, getMockUserTokenContainer());

        Hello firstMessage = Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build();

        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.connect(firstMessage, streamObserver);

        assertEquals(ErrorCodes.InternalError, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void reconnect_correctUserConnection_returnsAccessForbiddenCode() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        ReconnectHello firstMessage = ReconnectHello.newBuilder().setUserName("Existing").setInnerToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.reconnect(firstMessage, streamObserver);

        assertEquals(0, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
        assertNotNull(((ConnectResponse) arguments.get(0)).getResponse().getConnectionToken());
    }

    @Test
    void reconnect_nonExistingUserConnection_returnsAccessForbiddenCode() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());
        ReconnectHello firstMessage = ReconnectHello.newBuilder().setUserName("Test").setInnerToken("Test").build();
        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.reconnect(firstMessage, streamObserver);

        assertEquals(com.MMOCardGame.GameServer.Servers.ErrorCodes.AccessForbidden, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void reconnect_incorrectUserConnection_returnsAccessForbiddenCode() {
        GrpcUserServerService serverService = new GrpcUserServerService(getUserConnectionManager(), getMockUserTokenContainer());

        ReconnectHello firstMessage = ReconnectHello.newBuilder().setUserName("Existing").setInnerToken("Incorrect").build();

        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.reconnect(firstMessage, streamObserver);

        assertEquals(ErrorCodes.AccessForbidden, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void reconnect_innerProblem_returnsInternalProblemCode() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        doThrow(RuntimeException.class).when(connectionsManager).reconnectUserAndGetConnection(anyString(), anyString());
        GrpcUserServerService serverService = new GrpcUserServerService(connectionsManager, getMockUserTokenContainer());

        ReconnectHello firstMessage = ReconnectHello.newBuilder().setUserName("Existing").setInnerToken("Test").build();

        StreamObserver<ConnectResponse> streamObserver = mock(StreamObserver.class);
        List<Object> arguments = new ArrayList<>();

        doAnswer(onMock -> arguments.add(onMock.getArgument(0))).when(streamObserver).onNext(any(ConnectResponse.class));

        serverService.reconnect(firstMessage, streamObserver);

        assertEquals(ErrorCodes.InternalError, ((ConnectResponse) arguments.get(0)).getResponse().getReponseCode());
    }

    @Test
    void surrender_hasMessageForExistingCorrectUser_invokesSurrenderOnConnection() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        UUID uuid = UUID.randomUUID();
        GrpcUserConnection connection = mock(GrpcUserConnection.class);
        when(connectionsManager.getConnectionForUser(any())).thenReturn(connection);
        when(connection.getToken()).thenReturn(uuid);
        GrpcUserServerService serverService = new GrpcUserServerService(connectionsManager, getMockUserTokenContainer());

        serverService.surrender(Hello.newBuilder().setUserName("Test").setUserPasswordToken(uuid.toString()).build(), mock(StreamObserver.class));

        verify(connection).surrender();
    }

    @Test
    void surrender_hasMessageForExistingCorrectUser_invokesMethodOnStreamObserver() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        UUID uuid = UUID.randomUUID();
        GrpcUserConnection connection = mock(GrpcUserConnection.class);
        when(connectionsManager.getConnectionForUser(any())).thenReturn(connection);
        when(connection.getToken()).thenReturn(uuid);
        StreamObserver<SimpleResponse> streamObserver = mock(StreamObserver.class);
        GrpcUserServerService serverService = new GrpcUserServerService(connectionsManager, getMockUserTokenContainer());

        serverService.surrender(Hello.newBuilder().setUserName("Test").setUserPasswordToken(uuid.toString()).build(), streamObserver);

        verify(streamObserver).onNext(any());
        verify(streamObserver).onCompleted();
    }

    @Test
    void surrender_hasMessageForExistingCorrectUser_throwsAuthenticationException() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        UUID uuid = UUID.randomUUID();
        GrpcUserConnection connection = mock(GrpcUserConnection.class);
        when(connectionsManager.getConnectionForUser(any())).thenReturn(connection);
        when(connection.getToken()).thenReturn(uuid);
        StreamObserver<SimpleResponse> streamObserver = mock(StreamObserver.class);
        GrpcUserServerService serverService = new GrpcUserServerService(connectionsManager, getMockUserTokenContainer());

        Hello message = Hello.newBuilder().setUserName("Test").setUserPasswordToken(uuid.toString() + "Test").build();

        Assertions.assertThrows(AuthenticationException.class, () -> serverService.surrender(message, streamObserver));
    }

    @Test
    void getAllGameState_hasMessageForExistingCorrectUser_invokeMethodOnStreamObserver() {
        UserConnectionsManager connectionsManager = mock(UserConnectionsManager.class);
        UUID uuid = UUID.randomUUID();
        GameStateMessage gameState = GameStateMessage.newBuilder().build();
        GrpcUserConnection connection = mock(GrpcUserConnection.class);
        when(connectionsManager.getConnectionForUser(any())).thenReturn(connection);
        when(connection.getToken()).thenReturn(uuid);
        when(connection.getWholeGameState()).thenReturn(gameState);
        StreamObserver<GameStateMessage> streamObserver = mock(StreamObserver.class);
        GrpcUserServerService serverService = new GrpcUserServerService(connectionsManager, getMockUserTokenContainer());

        Hello message = Hello.newBuilder().setUserName("Test").setUserPasswordToken(uuid.toString()).build();

        serverService.getAllGameState(message, streamObserver);

        verify(streamObserver).onNext(gameState);
        verify(streamObserver).onCompleted();
    }

}