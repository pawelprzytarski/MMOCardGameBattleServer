package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GrpcUserServerIT {
    private final int testPort;

    public GrpcUserServerIT() {
        testPort = 1410;
    }

    private UserTokenContainer getMockUserTokenContainer() {
        return mock(UserTokenContainer.class);
    }

    private UserConnection getMockUserConnection(String test) {
        UUID uuid = UUID.randomUUID();
        GrpcUserConnection connection = mock(GrpcUserConnection.class);
        when(connection.getUserName()).thenReturn(test);
        when(connection.getToken()).thenReturn(uuid);
        when(connection.getWholeGameState()).thenReturn(GameStateMessage.newBuilder().build());
        return connection;
    }

    @Test
    void testConnect() throws IOException {
        AppConfiguration conf = mock(AppConfiguration.class);
        UserConnectionsManager connectionsManager = mock(com.MMOCardGame.GameServer.Servers.UserConnectionsManager.class);
        UserConnection mock1 = getMockUserConnection("Test");
        when(connectionsManager.createConnection(anyString(), anyString())).thenReturn(mock1);
        when(conf.getUserServerPort()).thenReturn(testPort);
        GRpcUserServer userServer = new GRpcUserServer(conf, connectionsManager, getMockUserTokenContainer());
        userServer.start();

        ConnectResponse response;
        try {
            Channel channel = ManagedChannelBuilder.forAddress("localhost", testPort).usePlaintext(true).build();
            UserServerGrpc.UserServerBlockingStub client = UserServerGrpc.newBlockingStub(channel);
            response = client.connect(Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build());
        } finally {
            userServer.stop();
            userServer.awaitTermination();
        }


        Assert.assertEquals(0, response.getResponse().getReponseCode());
        Assert.assertNotNull(response.getResponse().getConnectionToken());
    }

    @Test
    void testReconnect() throws IOException {
        AppConfiguration conf = mock(AppConfiguration.class);
        UserConnectionsManager connectionsManager = mock(com.MMOCardGame.GameServer.Servers.UserConnectionsManager.class);
        UserConnection mock1 = getMockUserConnection("Test");
        when(connectionsManager.createConnection(anyString(), anyString())).thenReturn(mock1);
        when(connectionsManager.reconnectUserAndGetConnection(anyString(), anyString())).thenReturn(mock1);
        when(conf.getUserServerPort()).thenReturn(testPort);
        GRpcUserServer userServer = new GRpcUserServer(conf, connectionsManager, getMockUserTokenContainer());
        userServer.start();

        ConnectResponse response;
        try {
            Channel channel = ManagedChannelBuilder.forAddress("localhost", testPort).usePlaintext(true).build();
            UserServerGrpc.UserServerBlockingStub client = UserServerGrpc.newBlockingStub(channel);

            client.connect(Hello.newBuilder().setUserName("Test").setUserPasswordToken("Test").build());
            response = client.reconnect(ReconnectHello.newBuilder().setInnerToken("Test").setUserName("Test").build());
        } finally {
            userServer.stop();
            userServer.awaitTermination();
        }

        Assert.assertEquals(0, response.getResponse().getReponseCode());
        Assert.assertNotNull(response.getResponse().getConnectionToken());
    }
}
