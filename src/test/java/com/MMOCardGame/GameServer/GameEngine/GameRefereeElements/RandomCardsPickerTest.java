package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RandomCardsPickerTest {
    List<Card> getCardsList() {
        List<Card> cards = new ArrayList<>();
        for (int i = 0; i < 45; i++)
            cards.add(getCard(i));
        return cards;
    }

    Card getCard(int instanceId) {
        return new Card(instanceId, instanceId, new CardStatistics(), null, instanceId % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", new CardScript());
    }

    @Test
    void getRandomCardsWithMinLinks_hasRandomCollectionAndRandomReturnsZeroAndIncrement_returnCardsCollectionWithTwoLinks() {
        List<Card> cards = getCardsList();
        Random random = mock(Random.class);
        AtomicInteger atomicInteger = new AtomicInteger(0);
        when(random.nextInt(anyInt())).thenAnswer(invocationOnMock -> atomicInteger.getAndIncrement());

        RandomCardsPicker randomCardsPicker = new RandomCardsPicker(random);
        Object result = randomCardsPicker.getRandomCardsWithMinLinks(cards, 7);

        Object expected = new ArrayList<>(new HashSet<>(Arrays.asList(2, 4, 3, 5, 6, 7, 8)));//because set changes order of elements
        assertEquals(expected, result);
    }

    @Test
    void getRandomCardsWithMinLinks_hasRandomCollectionAndRandomReturnsThreeAndIncrement_returnCardsCollectionWithFiveLinks() {
        List<Card> cards = getCardsList();
        Random random = mock(Random.class);
        AtomicInteger atomicInteger = new AtomicInteger(3);
        when(random.nextInt(anyInt())).thenAnswer(invocationOnMock -> atomicInteger.getAndIncrement());

        RandomCardsPicker randomCardsPicker = new RandomCardsPicker(random);
        Object result = randomCardsPicker.getRandomCardsWithMinLinks(cards, 7);

        Object expected = new ArrayList<>(new HashSet<>(Arrays.asList(8, 10, 12, 14, 16, 9, 11)));
        assertEquals(expected, result);
    }

    @Test
    void getRandomCards_hasRandomCollectionAndRandomReturnsZeroAndIncrement_returnCardsCollectionWithTwoLinks() {
        List<Card> cards = getCardsList();
        Random random = mock(Random.class);
        AtomicInteger atomicInteger = new AtomicInteger(0);
        when(random.nextInt(anyInt())).thenAnswer(invocationOnMock -> atomicInteger.getAndIncrement());

        RandomCardsPicker randomCardsPicker = new RandomCardsPicker(random);
        Object result = randomCardsPicker.getRandomCards(cards, 7);

        Object expected = new ArrayList<>(new HashSet<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6)));//because set changes order of elements
        assertEquals(expected, result);
    }

    @Test
    void getRandomCards_hasRandomCollectionAndRandomReturnsThreeAndIncrement_returnCardsCollectionWithFiveLinks() {
        List<Card> cards = getCardsList();
        Random random = mock(Random.class);
        AtomicInteger atomicInteger = new AtomicInteger(3);
        when(random.nextInt(anyInt())).thenAnswer(invocationOnMock -> atomicInteger.getAndIncrement());

        RandomCardsPicker randomCardsPicker = new RandomCardsPicker(random);
        Object result = randomCardsPicker.getRandomCards(cards, 7);

        Object expected = new ArrayList<>(new HashSet<>(Arrays.asList(3, 4, 5, 6, 7, 8, 9)));
        assertEquals(expected, result);
    }

}