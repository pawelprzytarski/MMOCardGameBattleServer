package com.MMOCardGame.GameServer.GameEngine.Triggers;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.CardAttackedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.CardMovedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.TriggerActivatedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardAttackedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerActivatedEvent;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

//Spy and Mock are adding new layer to inheritance chain so they cannot be used for this tests
class TriggersTest {
    @Test
    void registerCardScript_cardNoTypeMatchingClassButMatchingParameterAndNameMethods_noInvokeClassMethods() {
        Triggers triggers = new Triggers(mock(GameState.class));
        NoScriptClass noScriptClass = new NoScriptClass();
        triggers.registerCardScript(noScriptClass);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(0, noScriptClass.doSomethingCounter);
        assertEquals(0, noScriptClass.cardAttackedCardAttackedEventCounter);
        assertEquals(0, noScriptClass.cardAttackedCardMovedEventCounter);
    }

    @Test
    void registerCardScript_cardWithOneMatchingTriggers_invokeScriptOnTrigger() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject);

        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject.cardAttackedCounter);
    }

    @Test
    void registerCardScript_cardWithTwoMatchingTriggers_invokeScriptOnAllTriggers() {
        Triggers triggers = new Triggers(mock(GameState.class));
        TwoMatchingClass scriptObject = new TwoMatchingClass();
        triggers.registerCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject.cardAttackedCounter);
        assertEquals(1, scriptObject.cardMovedCounter);
    }

    @Test
    void registerCardScript_cardWithMatchingTriggerInParentClass_invokeScriptOnTrigger() {
        Triggers triggers = new Triggers(mock(GameState.class));
        ChildClassOfCardAttackedMatchingClass scriptObject = new ChildClassOfCardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject);

        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject.cardAttackedCounter);
    }

    @Test
    void registerCardScript_cardWithMatchingTriggerInParentClassAndInDirectImplementation_invokeScriptOnAllTriggers() {
        Triggers triggers = new Triggers(mock(GameState.class));
        ChildClassOfOneMatchingClassAndCardAttackedMatchClass scriptObject = new ChildClassOfOneMatchingClassAndCardAttackedMatchClass();
        triggers.registerCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject.cardAttackedCounter);
        assertEquals(1, scriptObject.cardMovedCounter);
    }

    @Test
    void registerCardScript_TwoCardScriptWithMatchingDifferentTriggers_invokeScriptsOnAllTriggers() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject1 = new CardAttackedMatchingClass();
        CardMovedMatchingClass scriptObject2 = new CardMovedMatchingClass();
        triggers.registerCardScript(scriptObject1);
        triggers.registerCardScript(scriptObject2);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject1.cardAttackedCounter);
        assertEquals(1, scriptObject2.cardMovedCounter);
    }

    @Test
    void registerCardScript_TwoCardScriptWithMatchingSameTrigger_invokeAllScriptsOnTrigger() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject1 = new CardAttackedMatchingClass();
        CardAttackedMatchingClass scriptObject2 = new CardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject1);
        triggers.registerCardScript(scriptObject2);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject1.cardAttackedCounter);
        assertEquals(1, scriptObject2.cardAttackedCounter);
    }

    @Test
    void registerCardScript_RegisterTwoTimesCardScriptWithMatchingTrigger_invokeScriptOneTime() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject1 = new CardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject1);
        triggers.registerCardScript(scriptObject1);

        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject1.cardAttackedCounter);
    }

    @Test
    void unregisterCardScript_CardScriptWithMatchingTrigger_noInvokeScriptOnTrigger() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject1 = new CardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject1);

        triggers.unregisterCardScript(scriptObject1);

        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject1.cardAttackedCounter);
    }

    @Test
    void unregisterCardScript_TwoCardScriptWithMatchingDifferentTrigger_invokeNoRemovedScriptOnTriggerAndNoInvokeRemovedScript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject1 = new CardAttackedMatchingClass();
        CardMovedMatchingClass scriptObject2 = new CardMovedMatchingClass();
        triggers.registerCardScript(scriptObject1);
        triggers.registerCardScript(scriptObject2);

        triggers.unregisterCardScript(scriptObject1);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject1.cardAttackedCounter);
        assertEquals(1, scriptObject2.cardMovedCounter);
    }

    @Test
    void unregisterCardScript_TwoCardScriptWithMatchingSameTrigger_invokeNoRemovedScriptOnTriggerAndNoInvokeRemovedScript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject1 = new CardAttackedMatchingClass();
        CardAttackedMatchingClass2 scriptObject2 = new CardAttackedMatchingClass2();
        triggers.registerCardScript(scriptObject1);
        triggers.registerCardScript(scriptObject2);

        triggers.unregisterCardScript(scriptObject1);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject1.cardAttackedCounter);
        assertEquals(1, scriptObject2.cardAttackedCounter);
    }

    @Test
    void unregisterCardScript_cardWithOneMatchingTriggers_noInvokeScript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject);

        triggers.unregisterCardScript(scriptObject);

        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject.cardAttackedCounter);
    }

    @Test
    void unregisterCardScript_cardWithTwoMatchingTriggers_noInvokeScript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        TwoMatchingClass scriptObject = new TwoMatchingClass();
        triggers.registerCardScript(scriptObject);

        triggers.unregisterCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject.cardAttackedCounter);
        assertEquals(0, scriptObject.cardMovedCounter);
    }

    @Test
    void unregisterCardScript_cardWithMatchingTriggerInParentClass_noInvokeScript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        ChildClassOfCardAttackedMatchingClass scriptObject = new ChildClassOfCardAttackedMatchingClass();
        triggers.registerCardScript(scriptObject);

        triggers.unregisterCardScript(scriptObject);

        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject.cardAttackedCounter);
    }

    @Test
    void registerCardScript_cardWithMatchingTriggerAndWithSubscriptWithMatchingTrigger_invokeScriptAndSubscript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        CardMovedMatchingClass subscriptObject = new CardMovedMatchingClass();
        scriptObject.addSubscript(subscriptObject);
        triggers.registerCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject.cardAttackedCounter);
        assertEquals(1, subscriptObject.cardMovedCounter);
    }

    @Test
    void registerCardScript_threeLevelSubscriptsChainMatchingTriggers_invokeScriptAndSubscripts() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        CardMovedMatchingClass subscriptObject = new CardMovedMatchingClass();
        CardAttackedMatchingClass subscriptObject2 = new CardAttackedMatchingClass();
        scriptObject.addSubscript(subscriptObject);
        subscriptObject.addSubscript(subscriptObject2);
        triggers.registerCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));

        assertEquals(1, scriptObject.cardAttackedCounter);
        assertEquals(1, subscriptObject.cardMovedCounter);
        assertEquals(1, subscriptObject2.cardAttackedCounter);
    }

    @Test
    void unregisterCardScript_cardWithMatchingTriggerAndWithSubscriptWithMatchingTrigger_noInvokeScriptAndSubscript() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        CardMovedMatchingClass subscriptObject = new CardMovedMatchingClass();
        scriptObject.addSubscript(subscriptObject);
        triggers.registerCardScript(scriptObject);

        triggers.unregisterCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject.cardAttackedCounter);
        assertEquals(0, subscriptObject.cardMovedCounter);
    }

    @Test
    void unregisterCardScript_threeLevelSubscriptsChainMatchingTriggers_noInvokeScriptAndSubscripts() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        CardMovedMatchingClass subscriptObject = new CardMovedMatchingClass();
        CardAttackedMatchingClass subscriptObject2 = new CardAttackedMatchingClass();
        scriptObject.addSubscript(subscriptObject);
        subscriptObject.addSubscript(subscriptObject2);
        triggers.registerCardScript(scriptObject);

        triggers.unregisterCardScript(scriptObject);

        triggers.invokeTrigger(new CardMovedEvent(mock(Card.class), new CardPosition(1, CardPosition.Position.Graveyard),
                new CardPosition(1, CardPosition.Position.Void)));
        triggers.invokeTrigger(new CardAttackedEvent(mock(Card.class), mock(Card.class)));
        assertEquals(0, scriptObject.cardAttackedCounter);
        assertEquals(0, subscriptObject.cardMovedCounter);
        assertEquals(0, subscriptObject2.cardAttackedCounter);
    }

    @Test
    void invokeTrigger_invokeTriggerOnTrigger_observableNotifyAfterEndOfAllTriggers() {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        TriggerInvokedScript scriptObject2 = new TriggerInvokedScript();
        triggers.registerCardScript(scriptObject);
        triggers.registerSimpleScript(scriptObject2);
        AtomicInteger atomicInteger = new AtomicInteger(0);
        triggers.getObservable().addObserver((object -> atomicInteger.incrementAndGet()));

        triggers.invokeTrigger(new CardAttackedEvent(null, null));

        assertEquals(1, atomicInteger.get());
        assertTrue(scriptObject2.activated);
    }

    @Test
    void invokeTrigger_invokeTriggerWithSleep_observableNotNotifyBeforeEndOfAllTriggers() throws InterruptedException {
        Triggers triggers = new Triggers(mock(GameState.class));
        CardAttackedMatchingClass scriptObject = new CardAttackedMatchingClass();
        TriggerInvokedScriptWithSleep scriptObject2 = new TriggerInvokedScriptWithSleep();
        triggers.registerCardScript(scriptObject);
        triggers.registerSimpleScript(scriptObject2);
        AtomicInteger atomicInteger = new AtomicInteger(0);
        triggers.getObservable().addObserver((object -> atomicInteger.incrementAndGet()));

        new Thread(() -> triggers.invokeTrigger(new CardAttackedEvent(null, null))).start();
        Thread.sleep(50);
        assertFalse(scriptObject2.activated);
        assertEquals(0, atomicInteger.get());

        Thread.sleep(150);
        assertTrue(scriptObject2.activated);
        assertEquals(1, atomicInteger.get());
    }

    static class NoScriptClass extends CardScript {
        int doSomethingCounter = 0;
        int cardAttackedCardAttackedEventCounter = 0;
        int cardAttackedCardMovedEventCounter = 0;

        public void doSomething(CardAttackedEvent e) {//parameter matching
            doSomethingCounter++;
        }

        public void cardAttacked(CardAttackedEvent event) {//parameter and name matching
            cardAttackedCardAttackedEventCounter++;
        }

        public void cardAttacked(CardMovedEvent event) {//name matching
            cardAttackedCardMovedEventCounter++;
        }
    }

    class CardAttackedMatchingClass extends CardScript implements CardAttackedScript {
        int cardAttackedCounter = 0;

        @Override
        public void cardAttacked(CardAttackedEvent event) {
            cardAttackedCounter++;
        }
    }

    class TwoMatchingClass extends CardScript implements CardAttackedScript, CardMovedScript {
        int cardAttackedCounter = 0;
        int cardMovedCounter = 0;

        @Override
        public void cardAttacked(CardAttackedEvent event) {
            cardAttackedCounter++;
        }

        @Override
        public TriggerType getTriggerType() {
            return null;
        }

        @Override
        public void cardMoved(CardMovedEvent event) {
            cardMovedCounter++;
        }
    }

    private class ChildClassOfCardAttackedMatchingClass extends CardAttackedMatchingClass {
    }

    private class ChildClassOfOneMatchingClassAndCardAttackedMatchClass extends CardAttackedMatchingClass implements CardMovedScript {
        int cardMovedCounter = 0;

        @Override
        public void cardMoved(CardMovedEvent event) {
            cardMovedCounter++;
        }

        @Override
        public TriggerType getTriggerType() {
            return null;
        }
    }

    private class CardMovedMatchingClass extends CardScript implements CardMovedScript {
        int cardMovedCounter = 0;

        @Override
        public void cardMoved(CardMovedEvent event) {
            cardMovedCounter++;
        }
    }

    class CardAttackedMatchingClass2 extends CardScript implements CardAttackedScript {
        int cardAttackedCounter = 0;

        @Override
        public void cardAttacked(CardAttackedEvent event) {
            cardAttackedCounter++;
        }
    }

    class TriggerInvokedScript implements TriggerActivatedScript {
        boolean activated = false;

        @Override
        public void triggerActivated(TriggerActivatedEvent eventType) {
            activated = true;
        }
    }

    class TriggerInvokedScriptWithSleep implements TriggerActivatedScript {
        boolean activated = false;

        @Override
        public void triggerActivated(TriggerActivatedEvent eventType) {
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
            }
            activated = true;
        }
    }
}