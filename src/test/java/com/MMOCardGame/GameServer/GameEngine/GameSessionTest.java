package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionControllerBuilderForTests;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionDump;
import com.MMOCardGame.GameServer.GameEngine.Sessions.UserSessionEndInfo;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.Instant;
import java.util.*;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GameSessionTest {

    private final String user1name = "Test1";
    private final String user2name = "Test2";

    @Test
    void registerConnection_registerExistingUser_registerSessionService() {
        GameSession gameSession = getGameSessionWithSampleData();

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn(user1name);

        gameSession.registerConnection(connection);

        verify(connection).registerSessionService(any());
    }

    private GameSession getGameSessionWithSampleData() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        return builder.build();
    }

    private GameSession getGameSessionWithSampleDataWithSpecificEndInfoObservable(ObservableNotifier<UserSessionEndInfo> endInfoObservable) {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setEndInfoObservable(endInfoObservable);
        return builder.build();
    }

    @Test
    void registerConnection_registerNonExistingUser_throwNotFoundException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setUserConnection1(null);
        builder.setUserConnection2(null);
        GameSession gameSession = builder.build();

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("UserNonExisting");

        assertThrows(NotFoundException.class, () -> gameSession.registerConnection(connection));
    }

    @Test
    void removeConnection_nonExistingConnection_throwNotFoundException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setUserConnection1(null);
        builder.setUserConnection2(null);
        GameSession gameSession = builder.build();

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn(user1name);

        assertThrows(NotFoundException.class, () -> gameSession.removeConnection(connection));
    }

    @Test
    void removeConnection_ExistingConnection_unregisterSessionService() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setUserConnection1(null);
        builder.setUserConnection2(null);
        GameSession gameSession = builder.build();

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn(user1name);

        gameSession.registerConnection(connection);

        gameSession.removeConnection(connection);

        verify(connection).removeSessionService();
    }

    @Test
    void removeConnection_ExistingConnection_cannotRegisterUserAgain() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setUserConnection1(null);
        builder.setUserConnection2(null);
        GameSession gameSession = builder.build();

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn(user1name);

        gameSession.registerConnection(connection);

        gameSession.removeConnection(connection);

        assertThrows(NotFoundException.class, () -> gameSession.registerConnection(connection));
    }

    @Test
    void removeConnection_ExistingConnection_sendGameEndInfo() {
        List<UserSessionEndInfo> userSessionEndInfo = new ArrayList<>();
        ObservableNotifier<UserSessionEndInfo> endInfoObservable = new ObservableNotifier<>();
        endInfoObservable.addObserver(userSessionEndInfo::add);
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setEndInfoObservable(endInfoObservable);
        builder.setUserConnection1(null);
        builder.setUserConnection2(null);
        GameSession gameSession = builder.build();

        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn(user1name);
        gameSession.registerConnection(connection);

        gameSession.removeConnection(connection);

        UserSessionEndInfo result = userSessionEndInfo.get(0);
        UserSessionEndInfo expected = new UserSessionEndInfo();
        expected.result = UserSessionEndInfo.Result.KICKED;
        expected.additionalInfo = "Undefined reason";
        expected.userLogin = user1name;
        assertEquals(expected, result);
    }

    @Test
    void removeConnection_nonExistingConnection_throwsNotFoundExceptionAndDoNotSendGameEndInfo() {
        List<UserSessionEndInfo> userSessionEndInfo = new ArrayList<>();
        ObservableNotifier<UserSessionEndInfo> endInfoObservable = new ObservableNotifier<>();
        endInfoObservable.addObserver(userSessionEndInfo::add);
        GameSession gameSession = getGameSessionWithSampleDataWithSpecificEndInfoObservable(endInfoObservable);
        UserConnection connection = mock(UserConnection.class);
        when(connection.getUserName()).thenReturn("NonExistingUser");

        assertThrows(NotFoundException.class, () -> gameSession.removeConnection(connection));

        assertTrue(userSessionEndInfo.isEmpty());
    }

    @Test
    void terminate_sendGameEndInfo() {
        List<UserSessionEndInfo> userSessionEndInfo = new ArrayList<>();
        ObservableNotifier<UserSessionEndInfo> endInfoObservable = new ObservableNotifier<>();
        endInfoObservable.addObserver(userSessionEndInfo::add);
        GameSession gameSession = getGameSessionWithSampleDataWithSpecificEndInfoObservable(endInfoObservable);

        gameSession.terminate();

        UserSessionEndInfo expected1 = new UserSessionEndInfo();
        expected1.result=UserSessionEndInfo.Result.KICKED;
        expected1.additionalInfo = "Session terminates";
        expected1.userLogin = user1name;

        UserSessionEndInfo expected2 = new UserSessionEndInfo();
        expected2.result=UserSessionEndInfo.Result.KICKED;
        expected2.additionalInfo = "Session terminates";
        expected2.userLogin = user2name;

        assertThat(userSessionEndInfo, hasItems(expected1, expected2));
    }

    @Test
    void dumpToStream_AllState_correctlyDumpToSteam() throws IOException, ClassNotFoundException {
        List<UserSessionEndInfo> userSessionEndInfo = new ArrayList<>();
        ObservableNotifier<UserSessionEndInfo> endInfoObservable = new ObservableNotifier<>();
        endInfoObservable.addObserver(userSessionEndInfo::add);
        GameSession gameSession = getGameSessionWithSampleDataWithSpecificEndInfoObservable(endInfoObservable);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        gameSession.dumpToStream(new Exception("Test exception"), byteArrayOutputStream);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        GameSessionDump dump = (GameSessionDump) objectInputStream.readObject();

        dump.setDumpDate(Date.from(Instant.ofEpochMilli(0)));
        GameSessionDump expected = new GameSessionDump(Date.from(Instant.ofEpochMilli(0)), gameSession.getAllGameState(),
                new HashSet<>(Arrays.asList(user1name, user2name)), "java.lang.Exception: Test exception");
        assertEquals(expected, dump);
    }
}