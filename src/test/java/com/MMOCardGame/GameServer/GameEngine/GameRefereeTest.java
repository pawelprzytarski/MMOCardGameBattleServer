package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.RandomCardsPicker;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.CardsField;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.PhasesStack;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionControllerBuilderForTests;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardChangeType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.HandChanged;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.Pair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GameRefereeTest {
    private String userName1 = "Test1";
    private String userName2 = "Test2";

    @Test
    void startFirstHandPhase_startFirstHandPhase_sendToAllPlayersDrawCardsMessage() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        phasesStack.endPhase(1);

        verify(userConnection1, times(2)).sendMessage(any(HandChanged.class));
        verify(userConnection2, times(2)).sendMessage(any(HandChanged.class));
    }

    @Test
    void startFirstHandPhase_startFirstHandPhase_sendRandomCards() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        List<Object> arguments = new ArrayList<>();
        doAnswer(invocationOnMock -> arguments.add(invocationOnMock.getArgument(0))).when(userConnection1).sendMessage(any());

        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        phasesStack.endPhase(1);

        HandChanged expectedMessageToPlayer = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(0)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        HandChanged expectedMessageToOthers = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(2)
                .build();
        assertEquals(Arrays.asList(expectedMessageToPlayer, expectedMessageToOthers), arguments);
    }

    @Test
    void startFirstHandPhase_startFirstHandPhase_moveCardsToHand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        List<Object> answers = new ArrayList<>();
        doAnswer(invocationOnMock -> answers.add(invocationOnMock.getArgument(0))).when(gameState).moveCardToHand(anyInt());
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);

        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        phasesStack.endPhase(1);

        assertEquals(Arrays.asList(1, 2, 1, 2), answers);
    }

    private RandomCardsPicker getMockRandomCardsPicker() {
        RandomCardsPicker randomCardPicker = mock(RandomCardsPicker.class);
        when(randomCardPicker.getRandomCardsWithMinLinks(any(), anyInt())).thenReturn(Arrays.asList(1, 2));
        when(randomCardPicker.getRandomCards(any(), anyInt())).thenReturn(Arrays.asList(1, 2));
        return randomCardPicker;
    }

    private GameState getMockGameStateWithPhasesStackAndCardsField(PhasesStack phasesStack) {
        GameState gameState = mock(GameState.class);
        Player player = new Player(0, 1, 20, new int[]{0}, new int[]{0}, new int[]{0});
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1));
        when(gameState.getPlayer(anyInt())).thenReturn(player);
        when(gameState.getPhasesStack()).thenReturn(phasesStack);
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));
        cardsField.moveCardToHand(1);
        cardsField.moveCardToHand(2);
        cardsField.moveCardToHand(3);
        when(gameState.getCardsFieldForPlayer(anyInt())).thenReturn(cardsField);
        return gameState;
    }

    Card getCard(int instanceId) {
        return new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
    }

    @Test
    void muligan_firstMuligan_sendToAllPlayersDrawCardsMessage() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        gameReferee.muligan(1);

        verify(userConnection1, times(2)).sendMessage(any(HandChanged.class));
        verify(userConnection2, times(2)).sendMessage(any(HandChanged.class));
    }

    @Test
    void muligan_firstMuligan_sendRandomCards() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        List<Object> arguments = new ArrayList<>();
        doAnswer(invocationOnMock -> arguments.add(invocationOnMock.getArgument(0))).when(userConnection1).sendMessage(any());
        List<Object> arguments2 = new ArrayList<>();
        doAnswer(invocationOnMock -> arguments2.add(invocationOnMock.getArgument(0))).when(userConnection2).sendMessage(any());

        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        gameReferee.muligan(1);

        HandChanged expectedRemoveMessageToOthers = HandChanged.newBuilder()
                .setType(CardChangeType.Remove)
                .setPlayer(1)
                .setCount(3)//because player have only 3 cards in hand
                .build();
        HandChanged expectedDrawMessageToOthers = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(2)//because mocked card picker returns only 2 cards
                .build();
        assertEquals(Arrays.asList(expectedRemoveMessageToOthers, expectedDrawMessageToOthers), arguments);
        HandChanged expectedMessageToPlayer1 = HandChanged.newBuilder()
                .setType(CardChangeType.Remove)
                .setPlayer(1)
                .addCardInstances(1)
                .addCardInstances(2)
                .addCardInstances(3)
                .setCount(3)
                .build();
        HandChanged expectedMessageToPlayer2 = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        assertEquals(Arrays.asList(expectedMessageToPlayer1,expectedMessageToPlayer2), arguments2);
    }

    @Test
    void muligan_twoTimes_randomSevenAndSixCards() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));

        RandomCardsPicker randomCardsPicker = getMockRandomCardsPicker();
        List<Object> arguments = new ArrayList<>();
        doAnswer(invocationOnMock -> {
            arguments.add(invocationOnMock.getArgument(1));
            return Arrays.asList(1, 2);
        }).when(randomCardsPicker).getRandomCards(any(), anyInt());
        gameReferee.setRandomCardsPicker(randomCardsPicker);

        gameReferee.muligan(1);
        gameReferee.muligan(1);

        assertEquals(Arrays.asList(7, 6), arguments);
    }

    @Test
    void muligan_firstMuligan_moveCardsToHandAndToLibrary() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));
        when(gameState.getCardsFieldForPlayer(anyInt())).thenReturn(cardsField);
        List<Object> answersToHand = new ArrayList<>();
        doAnswer(invocationOnMock -> answersToHand.add(invocationOnMock.getArgument(0))).when(gameState).moveCardToHand(anyInt());
        List<Object> answersToLibrary = new ArrayList<>();
        doAnswer(invocationOnMock -> answersToLibrary.add(invocationOnMock.getArgument(0))).when(gameState).moveCardToLibrary(anyInt());
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);

        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        phasesStack.endPhase(1);

        cardsField.moveCardToHand(1);//because gameState is mocked and don't move cards
        cardsField.moveCardToHand(2);

        gameReferee.muligan(1);

        assertEquals(Arrays.asList(1, 2, 1, 2, 1, 2), answersToHand);
        assertEquals(Arrays.asList(1, 2), answersToLibrary);
    }

    @Test
    void replaceCards_firstMuligan_sendToAllPlayersDrawCardsMessage() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        gameReferee.replaceCards(handChange);

        verify(userConnection1, times(2)).sendMessage(any(HandChanged.class));
        verify(userConnection2, times(2)).sendMessage(any(HandChanged.class));
    }

    @Test
    void replaceCards_canReplace_sendRandomCards() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        List<Object> arguments = new ArrayList<>();
        doAnswer(invocationOnMock -> arguments.add(invocationOnMock.getArgument(0))).when(userConnection1).sendMessage(any());
        List<Object> arguments2= new ArrayList<>();
        doAnswer(invocationOnMock -> arguments2.add(invocationOnMock.getArgument(0))).when(userConnection2).sendMessage(any());

        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        RandomCardsPicker randomCardsPicker = getMockRandomCardsPicker();
        when(randomCardsPicker.getRandomCards(any(), anyInt())).thenReturn(Arrays.asList(4, 5, 6));
        gameReferee.setRandomCardsPicker(randomCardsPicker);

        HandChanged message = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .addCardInstances(1)
                .addCardInstances(2)
                .addCardInstances(3)
                .build();
        gameReferee.replaceCards(message);

        HandChanged expectedRemoveMessageToOthers = HandChanged.newBuilder()
                .setType(CardChangeType.Remove)
                .setPlayer(1)
                .setCount(3)
                .build();
        HandChanged expectedDrawMessageToOthers = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(3)
                .build();
        assertEquals(Arrays.asList(expectedRemoveMessageToOthers, expectedDrawMessageToOthers), arguments);

        HandChanged expectedMessageToPlayer1 = HandChanged.newBuilder()
                .setType(CardChangeType.Remove)
                .setPlayer(1)
                .setCount(3)
                .addCardInstances(1)
                .addCardInstances(2)
                .addCardInstances(3)
                .build();
        HandChanged expectedMessageToPlayer2 = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(3)
                .addCardInstances(4)
                .addCardInstances(5)
                .addCardInstances(6)
                .build();
        assertEquals(Arrays.asList(expectedMessageToPlayer1,expectedMessageToPlayer2), arguments2);
    }

    @Test
    void replaceCards_canReplaceButNoSendCardsToRemove_throwBadRequest() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        List<Object> arguments = new ArrayList<>();
        doAnswer(invocationOnMock -> arguments.add(invocationOnMock.getArgument(0))).when(userConnection1).sendMessage(any());

        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        RandomCardsPicker randomCardsPicker = getMockRandomCardsPicker();
        when(randomCardsPicker.getRandomCards(any(), anyInt())).thenReturn(Arrays.asList(4, 5, 6));
        gameReferee.setRandomCardsPicker(randomCardsPicker);

        HandChanged message = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(3)
                .build();
        assertThrows(BadRequestException.class, () -> gameReferee.replaceCards(message));
    }

    @Test
    void replaceCards_twoTimes_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        gameReferee.replaceCards(handChange);
        assertThrows(InconsistentPlayerCommand.class, () -> gameReferee.replaceCards(handChange));
    }

    @Test
    void replaceCards_cardsNotInHand_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(1, CardPosition.Position.Library));
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(5)
                .build();
        assertThrows(BadRequestException.class, () -> gameReferee.replaceCards(handChange));
    }

    @Test
    void replaceCards_cardsNotUser_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(0, CardPosition.Position.Hand));
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(getMockRandomCardsPicker());

        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(5)
                .build();
        assertThrows(BadRequestException.class, () -> gameReferee.replaceCards(handChange));
    }

    @Test
    void replaceCards_replaceTwoCards_moveCardsToHandAndToLibrary() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Waiting, 1)));
        GameState gameState = getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(phasesStack);
        List<Object> answersToHand = new ArrayList<>();
        doAnswer(invocationOnMock -> answersToHand.add(invocationOnMock.getArgument(0))).when(gameState).moveCardToHand(anyInt());
        List<Object> answersToLibrary = new ArrayList<>();
        doAnswer(invocationOnMock -> answersToLibrary.add(invocationOnMock.getArgument(0))).when(gameState).moveCardToLibrary(anyInt());
        UserConnection userConnection1 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName1, 0);
        UserConnection userConnection2 = GameSessionControllerBuilderForTests.getMockedUserConnection(userName2, 1);

        RandomCardsPicker randomCardsPicker = getMockRandomCardsPicker();
        when(randomCardsPicker.getRandomCardsWithMinLinks(any(), anyInt())).thenReturn(Arrays.asList(1, 2, 3));
        when(randomCardsPicker.getRandomCards(any(), anyInt())).thenReturn(Arrays.asList(4, 5));
        GameReferee gameReferee = new GameReferee(gameState, Arrays.asList(userConnection1, userConnection2));
        gameReferee.setRandomCardsPicker(randomCardsPicker);

        phasesStack.endPhase(1);
        HandChanged message = HandChanged.newBuilder()
                .setType(CardChangeType.Draw)
                .setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        gameReferee.replaceCards(message);

        assertEquals(Arrays.asList(1, 2, 3, 1, 2, 3, 4, 5), answersToHand);
        assertEquals(Arrays.asList(1, 2), answersToLibrary);
    }

    private GameState getGameStateWithPhasesStackAndCardsFieldWithCardsInHand(PhasesStack phasesStack) {
        GameState gameState = getMockGameStateWithPhasesStackAndCardsField(phasesStack);
        when(gameState.getCardPosition(anyInt())).thenReturn(new CardPosition(1, CardPosition.Position.Hand));
        return gameState;
    }
}
