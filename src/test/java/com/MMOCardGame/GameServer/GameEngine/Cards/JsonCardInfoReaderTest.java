package com.MMOCardGame.GameServer.GameEngine.Cards;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class JsonCardInfoReaderTest {
    @Test
    void readFromStream_emptyString_throwArgumentException() {
        JsonCardInfoReader jsonCardReader = new JsonCardInfoReader();
        ByteArrayInputStream inputStream = getMemoryInputStream("");

        assertThrows(IllegalArgumentException.class, () -> jsonCardReader.readFromStream(inputStream));
    }

    private ByteArrayInputStream getMemoryInputStream(String text) {
        return new ByteArrayInputStream(text.getBytes(Charset.forName("UTF-8")));
    }

    @Test
    void readFromStream_BasicLinkSingleCard_returnBasicLinkObject() throws IOException {
        JsonCardInfoReader jsonCardReader = new JsonCardInfoReader();
        ByteArrayInputStream inputStream = getBasicLinkCardJson();

        List<CardInfoReader.CardInfo> result = jsonCardReader.readFromStream(inputStream);
        ArrayList<CardInfoReader.CardInfo> expected = new ArrayList<>();
        expected.add(getBasicLinkCard());
        assertEquals(expected, result);
    }

    private ByteArrayInputStream getBasicLinkCardJson() {
        String text = "[{\n" +
                "\"id\": 1,\n" +
                "\"description\":\"Jakiś opis\",\n" +
                "\"name\":\"Łącze szkoły Raiderów\",\n" +
                "\"type\":\"Link\",\n" +
                "\"subtype\":\"Basic\"\n" +
                "}]";
        return getMemoryInputStream(text);
    }

    private CardInfoReader.CardInfo getBasicLinkCard() {
        CardInfoReader.CardInfo cardInfo = new CardInfoReader.CardInfo();
        cardInfo.id = 1;
        cardInfo.type = "Link";
        cardInfo.subtype = "Basic";
        return cardInfo;
    }

    @Test
    void readFromStream_UnitWithAbility_returnUnitObject() throws IOException {
        JsonCardInfoReader jsonCardReader = new JsonCardInfoReader();
        ByteArrayInputStream inputStream = getUnitWithAbilityCardJson();

        List<CardInfoReader.CardInfo> result = jsonCardReader.readFromStream(inputStream);
        ArrayList<CardInfoReader.CardInfo> expected = new ArrayList<>();
        expected.add(getUnitCard());
        assertEquals(expected, result);
    }

    private ByteArrayInputStream getUnitWithAbilityCardJson() {
        String text = "[{\"" +
                "id\": 5,\n" +
                "\"statistics\":{\n" +
                "\"attack\":2,\n" +
                "\"defence\":1\n" +
                "},\n" +
                "\"abilities\":[\n" +
                "1\n" +
                "],\n" +
                "\"cost\":{\n" +
                "\"neutral\":1,\n" +
                "\"raiders\":1,\n" +
                "\"scanners\":0,\n" +
                "\"whiteHats\":0,\n" +
                "\"psychotronnics\":0\n" +
                "},\n" +
                "\"description\":\"Zawsze czujny\",\n" +
                "\"name\":\"Husarz\",\n" +
                "\"type\":\"Unit\",\n" +
                "\"class\": \"SI\",\n" +
                "\"subclass\": \"Szarza\"" +
                "}]";
        return getMemoryInputStream(text);
    }

    private CardInfoReader.CardInfo getUnitCard() {
        CardInfoReader.CardInfo cardInfo = new CardInfoReader.CardInfo();
        cardInfo.id = 5;
        cardInfo.statistics = new CardInfoReader.CardInfo.Statistics();
        cardInfo.statistics.attack = 2;
        cardInfo.statistics.defence = 1;
        cardInfo.abilities = Collections.singletonList(1);
        cardInfo.cost = new CardInfoReader.CardInfo.Costs();
        cardInfo.cost.neutral = 1;
        cardInfo.cost.raiders = 1;
        cardInfo.type = "Unit";
        cardInfo.cardClass = "SI";
        cardInfo.subclass = "Szarza";
        return cardInfo;
    }

    @Test
    void readFromStream_UnitWithAbilityAndBasicLink_returnArrayWithUnitObjectAndLinkObject() throws IOException {
        JsonCardInfoReader jsonCardReader = new JsonCardInfoReader();
        ByteArrayInputStream inputStream = getUnitAndLinkCardsJson();

        List<CardInfoReader.CardInfo> result = jsonCardReader.readFromStream(inputStream);
        ArrayList<CardInfoReader.CardInfo> expected = new ArrayList<>();
        expected.add(getUnitCard());
        expected.add(getBasicLinkCard());
        assertEquals(expected, result);
    }

    private ByteArrayInputStream getUnitAndLinkCardsJson() {
        String text = "[{" +
                "\"id\": 5,\n" +
                "\"statistics\":{\n" +
                "\"attack\":2,\n" +
                "\"defence\":1\n" +
                "},\n" +
                "\"abilities\":[\n" +
                "1\n" +
                "],\n" +
                "\"cost\":{\n" +
                "\"neutral\":1,\n" +
                "\"raiders\":1,\n" +
                "\"scanners\":0,\n" +
                "\"whiteHats\":0,\n" +
                "\"psychotronnics\":0\n" +
                "},\n" +
                "\"description\":\"Zawsze czujny\",\n" +
                "\"name\":\"Husarz\",\n" +
                "\"type\":\"Unit\",\n" +
                "\"class\": \"SI\",\n" +
                "\"subclass\": \"Szarza\"" +
                "},{" +
                "\"id\": 1,\n" +
                "\"description\":\"Jakiś opis\",\n" +
                "\"name\":\"Łącze szkoły Raiderów\",\n" +
                "\"type\":\"Link\",\n" +
                "\"subtype\":\"Basic\"\n" +
                "}]";
        return getMemoryInputStream(text);
    }
}