package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.CustomOptionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.CardsInHandStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.OptionsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class TransactionTest {

    private GameState gameStateMock;
    private TargetsStepDefinition targetsStepDefinitionMock;
    private Transaction transactionMock;

    @BeforeEach
    void setup(){
        gameStateMock = Mockito.mock(GameState.class);
        targetsStepDefinitionMock = Mockito.mock(TargetsStepDefinition.class);
        transactionMock = Mockito.mock(Transaction.class);
    }

    @Test
    void selectFirstOption_withFirstOptionAddingStepAfterCurrent_shouldCallAddStepOnTransaction(){
        var optionsTargets = new ArrayList<CustomOptionDefinition>();
        var option1 = new CustomOptionDefinition("option1",
                stepEnvironment -> stepEnvironment
                        .getTransaction()
                        .addStepAfterCurrent(new RunningTransactionStep(targetsStepDefinitionMock)));
        optionsTargets.add(option1);
        var optionsStep = new OptionsStepDefinition(optionsTargets, 1);


        var optionsInfo = optionsStep.getPossibleOptionsInfo(gameStateMock, -1);
        ArrayList<TargetSelectingOption> selectedOption = new ArrayList<>();
        selectedOption.add(optionsInfo.getOptions().get(0));
        optionsStep.getSelectedOptionsHandler().accept(selectedOption, new StepEnvironment(transactionMock, gameStateMock));


        verify(transactionMock, times(1)).addStepAfterCurrent(any());
    }

    @Test
    void createTransaction_withThreeSteps_shouldHaveThreeSteps(){
        var optionsTargets = new ArrayList<CustomOptionDefinition>();
        var option1 = new CustomOptionDefinition("option1",
                stepEnvironment -> stepEnvironment
                        .getTransaction()
                        .addStepAfterCurrent(new RunningTransactionStep(targetsStepDefinitionMock)));
        optionsTargets.add(option1);
        var optionsStep = new OptionsStepDefinition(optionsTargets, 1);
        var targetStep = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, 2);
        var cardInHandStep = new CardsInHandStepDefinition(card -> true, 1);

        TransactionDefinition definition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);
        definition.addStep(optionsStep);
        definition.addStep(targetStep);
        definition.addStep(cardInHandStep);

        Transaction transaction = new Transaction(definition);

        assertEquals(3, transaction.getDefinition().getStepsDefinitions().size());
    }

}