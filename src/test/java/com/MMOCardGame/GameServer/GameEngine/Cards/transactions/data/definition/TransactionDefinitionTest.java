package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.StepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransactionDefinitionTest {

    @Test
    void getFirstStepDefinition_withProperTransactionDefinition_shouldReturnFirstStepDefinition(){
        TransactionDefinition transactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);
        TargetsStepDefinition step1 = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, 2);
        transactionDefinition.addStep(step1);
        TargetsStepDefinition step2 = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, (optionsList, gameState) -> 2);
        transactionDefinition.addStep(step2);

        StepDefinition result = transactionDefinition.getFirstStepDefinition();

        assertEquals(step1, result);
    }

    @Test
    void getFirstStepDefinition_withEmptyTransactionDefinition_shouldReturnNull(){
        TransactionDefinition transactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);

        StepDefinition result = transactionDefinition.getFirstStepDefinition();

        assertNull(result);
    }

    @Test
    void hasAnySteps_withTransactionDefinitionWithTwoSteps_shouldReturnTrue(){
        TransactionDefinition transactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);
        TargetsStepDefinition step1 = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, 2);
        transactionDefinition.addStep(step1);
        TargetsStepDefinition step2 = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, (optionsList, gameState) -> 2);
        transactionDefinition.addStep(step2);

        boolean result = transactionDefinition.hasAnySteps();

        assertTrue(result);
    }

    @Test
    void hasAnySteps_withEmptyTransactionDefinition_shouldReturnFalse(){
        TransactionDefinition transactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Attack);

        boolean result = transactionDefinition.hasAnySteps();

        assertFalse(result);
    }

}