package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScript.CardScriptCard1;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScript.NamedAbilityScript1;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptForMultipleCards.CardScriptCardMultiple;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptForMultipleCards.NamedAbilityScriptMultiple;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptWithTwoLevelInheritance.CardScriptCard2;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptWithTwoLevelInheritance.NamedAbilityScript2;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts.CardScriptCard3;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts.CardScriptCard4;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts.CardScriptNamedAbility3;
import com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts.CardScriptNamedAbility4;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CardScriptFinderTest {
    @Test
    void findNamedAbilitiesScripts_NonExistingPackage_returnEmptyCollection() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("NonExistingPackage"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        assertTrue(result.isEmpty());
    }

    @Test
    void findNamedAbilitiesScripts_PackageWithoutCardScript_returnEmptyCollection() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithoutCardScript"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        assertTrue(result.isEmpty());
    }

    @Test
    void findNamedAbilitiesScripts_PackageWithOneCardScript_returnOneClass() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScript"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        Map<Integer, Class<? extends CardScript>> expected = Collections.singletonMap(1, NamedAbilityScript1.class);
        assertEquals(expected, result);
    }

    @Test
    void findNamedAbilitiesScripts_PackageWithOneCardScriptWithTwoLevelInheritance_returnOneClass() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptWithTwoLevelInheritance"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        Map<Integer, Class<? extends CardScript>> expected = Collections.singletonMap(2, NamedAbilityScript2.class);
        assertEquals(expected, result);
    }

    @Test
    void findNamedAbilitiesScripts_PackageWithTwoCardScript_returnTwoClasses() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        Map<Integer, Class<? extends CardScript>> expected = new HashMap<>();
        expected.put(3, CardScriptNamedAbility3.class);
        expected.put(4, CardScriptNamedAbility4.class);
        assertEquals(expected, result);
    }

    @Test
    void findNamedAbilitiesScripts_ThreePackages_returnAllClassesFromPackages() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Arrays.asList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts",
                "com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScript",
                "com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptWithTwoLevelInheritance"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        Map<Integer, Class<? extends CardScript>> expected = new HashMap<>();
        expected.put(1, NamedAbilityScript1.class);
        expected.put(2, NamedAbilityScript2.class);
        expected.put(3, CardScriptNamedAbility3.class);
        expected.put(4, CardScriptNamedAbility4.class);
        assertEquals(expected, result);
    }


    @Test
    void findCardScripts_NonExistingPackage_returnEmptyCollection() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("NonExistingPackage"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        assertTrue(result.isEmpty());
    }

    @Test
    void findCardScripts_PackageWithoutCardScript_returnEmptyCollection() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithoutCardScript"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        assertTrue(result.isEmpty());
    }

    @Test
    void findCardScripts_PackageWithOneCardScript_returnOneClass() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScript"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        Map<Integer, Class<? extends CardScript>> expected = Collections.singletonMap(1, CardScriptCard1.class);
        assertEquals(expected, result);
    }

    @Test
    void findCardScripts_PackageWithOneCardScriptWithTwoLevelInheritance_returnOneClass() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptWithTwoLevelInheritance"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        Map<Integer, Class<? extends CardScript>> expected = Collections.singletonMap(2, CardScriptCard2.class);
        assertEquals(expected, result);
    }

    @Test
    void findCardScripts_PackageWithTwoCardScript_returnTwoClasses() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        Map<Integer, Class<? extends CardScript>> expected = new HashMap<>();
        expected.put(3, CardScriptCard3.class);
        expected.put(4, CardScriptCard4.class);
        assertEquals(expected, result);
    }

    @Test
    void findCardScripts_ThreePackages_returnAllClassesFromPackages() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Arrays.asList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithTwoScripts",
                "com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScript",
                "com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptWithTwoLevelInheritance"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        Map<Integer, Class<? extends CardScript>> expected = new HashMap<>();
        expected.put(1, CardScriptCard1.class);
        expected.put(2, CardScriptCard2.class);
        expected.put(3, CardScriptCard3.class);
        expected.put(4, CardScriptCard4.class);
        assertEquals(expected, result);
    }

    @Test
    void findCardScripts_scriptsForMultipleCards_returnScriptForAllIds() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptForMultipleCards"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findCardScripts();

        Map<Integer, Class<? extends CardScript>> expected = new HashMap<>();
        expected.put(3, CardScriptCardMultiple.class);
        expected.put(4, CardScriptCardMultiple.class);
        expected.put(5, CardScriptCardMultiple.class);
        assertEquals(expected, result);
    }

    @Test
    void findNamedAbilitiesScripts_scriptsForMultipleCards_returnScriptForAllIds() throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(Collections.singletonList("com.MMOCardGame.GameServer.GameEngine.Cards.PackageWithOneScriptForMultipleCards"));

        Map<Integer, Class<? extends CardScript>> result = cardScriptFinder.findNamedAbilitiesScripts();

        Map<Integer, Class<? extends CardScript>> expected = new HashMap<>();
        expected.put(1, NamedAbilityScriptMultiple.class);
        expected.put(2, NamedAbilityScriptMultiple.class);
        expected.put(3, NamedAbilityScriptMultiple.class);
        assertEquals(expected, result);
    }
}

