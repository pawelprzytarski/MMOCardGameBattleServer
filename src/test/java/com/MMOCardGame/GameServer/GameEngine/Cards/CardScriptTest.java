package com.MMOCardGame.GameServer.GameEngine.Cards;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CardScriptTest {
    @Test
    void canBeAttackedBy_ClearBasicScript_returnsTrue() {
        CardScript cardScript = new CardScript();

        boolean result = cardScript.canBeAttackedBy(mock(Card.class));

        assertTrue(result);
    }

    @Test
    void canBeBlockedBy_ClearBasicScript_returnsTrue() {
        CardScript cardScript = new CardScript();

        boolean result = cardScript.canBeBlockedBy(mock(Card.class));

        assertTrue(result);
    }

    @Test
    void canBeAffectedByAbility_ClearBasicScript_returnsTrue() {
        CardScript cardScript = new CardScript();

        boolean result = cardScript.canBeAffectedByAbility(mock(Card.class), 1);

        assertTrue(result);
    }

    @Test
    void canBeAffectedByUserAbility_ClearBasicScript_returnsTrue() {
        CardScript cardScript = new CardScript();

        boolean result = cardScript.canBeAffectedByUserAbility(1);

        assertTrue(result);
    }

    @Test
    void canBeAttackedBy_WithSubscriptReturnsFalse_returnsFalse() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAttackedBy(any())).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);

        boolean result = cardScript.canBeAttackedBy(mock(Card.class));

        assertFalse(result);
    }

    @Test
    void canBeAttackedBy_WithSubscriptReturnsTrue_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAttackedBy(any())).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);

        boolean result = cardScript.canBeAttackedBy(mock(Card.class));

        assertTrue(result);
    }

    @Test
    void canBeAttackedBy_WithTwoSubscriptReturnsTrue_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAttackedBy(any())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeAttackedBy(any())).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeAttackedBy(mock(Card.class));

        assertTrue(result);
    }

    @Test
    void canBeAttackedBy_WithTwoSubscriptAndOneReturnsFalse_returnsFalse() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAttackedBy(any())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeAttackedBy(any())).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeAttackedBy(mock(Card.class));

        assertFalse(result);
    }

    @Test
    void canBeBlockedBy_WithTwoSubscriptReturnsTrue_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeBlockedBy(any())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeBlockedBy(any())).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeBlockedBy(mock(Card.class));

        assertTrue(result);
    }

    @Test
    void canBeBlockedBy_WithTwoSubscriptAndOneReturnsFalse_returnsFalse() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeBlockedBy(any())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeBlockedBy(any())).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeBlockedBy(mock(Card.class));

        assertFalse(result);
    }

    @Test
    void canBeAffectedByAbility_WithTwoSubscriptAndOneReturnsFalse_returnsFalse() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAttackedBy(any())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeAttackedBy(any())).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeAttackedBy(mock(Card.class));

        assertFalse(result);
    }

    @Test
    void canBeAffectedByAbility_WithTwoSubscriptReturnsTrue_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAffectedByAbility(any(), anyInt())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeAffectedByAbility(any(), anyInt())).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeAffectedByAbility(mock(Card.class), 1);

        assertTrue(result);
    }

    @Test
    void canBeAffectedByUserAbility_WithTwoSubscriptReturnsTrue_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAffectedByUserAbility(anyInt())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeAffectedByUserAbility(anyInt())).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeAffectedByUserAbility(1);

        assertTrue(result);
    }

    @Test
    void canBeAffectedByUserAbility_WithTwoSubscriptAndOneReturnsFalse_returnsFalse() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAffectedByUserAbility(anyInt())).thenReturn(true);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canBeAffectedByUserAbility(anyInt())).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canBeAffectedByUserAbility(1);

        assertFalse(result);
    }

    @Test
    void canBeAffectedByUserAbility_unregisterSubscriptReturnFalse_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAffectedByUserAbility(anyInt())).thenReturn(false);
        CardScript cardScript = new CardScript();

        cardScript.addSubscript(subscript);
        cardScript.removeSubscript(subscript.getClass());

        boolean result = cardScript.canBeAffectedByUserAbility(1);

        assertTrue(result);
    }

    @Test
    void canBeAffectedByUserAbility_RegisterTwoSubscriptAndUnregisterOne_returnsFalse() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAffectedByUserAbility(anyInt())).thenReturn(false);
        CardScript cardScript = new CardScript();

        cardScript.addSubscript(subscript);
        cardScript.addSubscript(new SpecialScript());
        cardScript.removeSubscript(subscript.getClass());

        boolean result = cardScript.canBeAffectedByUserAbility(1);

        assertFalse(result);
    }

    @Test
    void canBeAffectedByUserAbility_RegisterTwoSubscriptAndUnregisterTwo_returnsTrue() {
        CardScript subscript = mock(CardScript.class);
        when(subscript.canBeAffectedByUserAbility(anyInt())).thenReturn(false);
        CardScript cardScript = new CardScript();

        cardScript.addSubscript(subscript);
        cardScript.addSubscript(new SpecialScript());
        cardScript.removeSubscript(subscript.getClass());
        cardScript.removeSubscript(SpecialScript.class);

        boolean result = cardScript.canBeAffectedByUserAbility(1);

        assertTrue(result);
    }

    @Test
    void canAttack_hasAliveCard_returnsTrue() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);

        boolean result = cardScript.canAttack();

        assertTrue(result);
    }

    @Test
    void canAttack_hasDeadCard_returnsFalse() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);

        boolean result = cardScript.canAttack();

        assertFalse(result);
    }

    @Test
    void canAttack_hasAliveCardWithScriptReturnsFalse_returnsFalse() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(true);
        CardScript subscript = mock(CardScript.class);
        when(subscript.canAttack()).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);
        cardScript.addSubscript(subscript);

        boolean result = cardScript.canAttack();

        assertFalse(result);
    }

    @Test
    void canAttack_hasDeadCardWithScriptReturnsFalse_returnsFalse() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(false);
        CardScript subscript = mock(CardScript.class);
        when(subscript.canAttack()).thenReturn(false);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);
        cardScript.addSubscript(subscript);

        boolean result = cardScript.canAttack();

        assertFalse(result);
    }

    @Test
    void canAttack_hasAliveCardWithScriptReturnsTrue_returnsTrue() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(true);
        CardScript subscript = mock(CardScript.class);
        when(subscript.canAttack()).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);
        cardScript.addSubscript(subscript);

        boolean result = cardScript.canAttack();

        assertTrue(result);
    }

    @Test
    void canAttack_hasDeadCardWithScriptReturnsTrue_returnsFalse() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(false);
        CardScript subscript = mock(CardScript.class);
        when(subscript.canAttack()).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);
        cardScript.addSubscript(subscript);

        boolean result = cardScript.canAttack();

        assertFalse(result);
    }

    @Test
    void canAttack_hasAliveCardWithTwoScriptReturnsFalseAndTrue_returnsFalse() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(true);
        CardScript subscript = mock(CardScript.class);
        when(subscript.canAttack()).thenReturn(false);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canAttack()).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canAttack();

        assertFalse(result);
    }

    @Test
    void canAttack_hasDeadCardWithTwoScriptReturnsFalseAndTrue_returnsFalse() {
        Card card = mock(Card.class);
        when(card.isAlive()).thenReturn(false);
        CardScript subscript = mock(CardScript.class);
        when(subscript.canAttack()).thenReturn(false);
        CardScript subscript2 = mock(CardScript.class);
        when(subscript2.canAttack()).thenReturn(true);
        CardScript cardScript = new CardScript();
        cardScript.setParentCard(card);
        cardScript.addSubscript(subscript);
        cardScript.addSubscript(subscript2);

        boolean result = cardScript.canAttack();

        assertFalse(result);
    }

    public static class SpecialScript extends CardScript {
        @Override
        public boolean canBeAffectedByUserAbility(int abilityId) {
            return false;
        }
    }
}