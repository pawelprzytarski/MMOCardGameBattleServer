package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.StepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

class TransactionsCollectionTest {
    private Transaction transactionMock = Mockito.mock(Transaction.class);
    private TransactionDefinition definitionMock = Mockito.mock(TransactionDefinition.class);
    private StepDefinition stepMock = Mockito.mock(StepDefinition.class);

    @BeforeEach
    void setup(){
        Mockito.when(transactionMock.getId()).thenReturn(1);
        Mockito.when(transactionMock.getDefinition())
                .thenReturn(definitionMock);
        Mockito.when(definitionMock.getStartedFor())
                .thenReturn(TargetSelectingTransaction.StartTransactionFor.Play);
        Mockito.when(definitionMock.getStepsDefinitions())
                .thenReturn(new ArrayList<>(){{add(stepMock);}});
        Mockito.when(definitionMock.hasAnySteps())
                .thenReturn(true);
    }

    @Test
    void finishingTransaction_shouldMakeItNotRunning(){
        var transactionsCollection = new TransactionsCollection();
        var transaction = transactionsCollection.tryStartAndGetNewTransaction(definitionMock);

        transactionsCollection.finishTransaction(transaction.getId());

        assertFalse(transactionsCollection.isRunning(transaction.getId()));
    }

    @Test
    void finishingTransaction_shouldMakeItRemembered(){
        var transactionsCollection= new TransactionsCollection();
        var transaction = transactionsCollection.tryStartAndGetNewTransaction(definitionMock);

        transactionsCollection.finishTransaction(transaction.getId());

        assertTrue(transactionsCollection.isRemembered(transaction.getId()));
    }
}