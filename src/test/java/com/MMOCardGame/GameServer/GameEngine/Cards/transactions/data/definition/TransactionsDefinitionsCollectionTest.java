package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransactionsDefinitionsCollectionTest {

    @Test
    void getTransactionsDefinition_startedForTrigger_shouldReturnTriggerTypeDefinition(){
        TransactionsDefinitionsCollection definitions = new TransactionsDefinitionsCollection();
        definitions.addDefinition(new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Trigger));
        TransactionDefinition triggerDefinition = definitions.getDefinition(TargetSelectingTransaction.StartTransactionFor.Trigger);

        TargetSelectingTransaction.StartTransactionFor result = triggerDefinition.getStartedFor();

        assertEquals(TargetSelectingTransaction.StartTransactionFor.Trigger, result);
    }

    @Test
    void createdTransactionsDefinitionsCollection_shouldNotContainDefinitionOfAttackType(){
        TransactionsDefinitionsCollection definitions = new TransactionsDefinitionsCollection();

        boolean result = definitions.contains(TargetSelectingTransaction.StartTransactionFor.Attack);

        assertFalse(result);
    }

    @Test
    void createdTransactionsDefinitionsCollection_shouldNotContainDefinitionOfPlayType(){
        TransactionsDefinitionsCollection definitions = new TransactionsDefinitionsCollection();

        boolean result = definitions.contains(TargetSelectingTransaction.StartTransactionFor.Play);

        assertFalse(result);
    }

}