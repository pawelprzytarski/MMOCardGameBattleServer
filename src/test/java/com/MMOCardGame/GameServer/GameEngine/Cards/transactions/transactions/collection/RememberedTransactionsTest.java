package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

class RememberedTransactionsTest {

    private Transaction transactionMock = Mockito.mock(Transaction.class);
    private TransactionDefinition definitionMock = Mockito.mock(TransactionDefinition.class);

    @BeforeEach
    void setup(){
        Mockito.when(transactionMock.getId()).thenReturn(1);
        Mockito.when(transactionMock.getDefinition())
                .thenReturn(definitionMock);
        Mockito.when(definitionMock.getStartedFor())
                .thenReturn(TargetSelectingTransaction.StartTransactionFor.Play);
    }

    @Test
    void rememberTransaction_shouldContainItByIdAndType(){
        var rememberedTransactions = new RememberedTransactions();

        rememberedTransactions.rememberTransaction(transactionMock);

        assertTrue(rememberedTransactions.contains(transactionMock.getId()));
        assertTrue(rememberedTransactions.contains(transactionMock.getDefinition().getStartedFor()));
    }

    @Test
    void removingTransaction_byId_shouldRemoveItByIdAndType(){
        var rememberedTransactions = new RememberedTransactions();
        rememberedTransactions.rememberTransaction(transactionMock);

        rememberedTransactions.remove(transactionMock.getId());

        assertFalse(rememberedTransactions.contains(transactionMock.getId()));
        assertFalse(rememberedTransactions.contains(transactionMock.getDefinition().getStartedFor()));
    }

    @Test
    void removingTransaction_byType_shouldRemoveItByIdAndType(){
        var rememberedTransactions = new RememberedTransactions();
        rememberedTransactions.rememberTransaction(transactionMock);

        rememberedTransactions.remove(transactionMock.getDefinition().getStartedFor());

        assertFalse(rememberedTransactions.contains(transactionMock.getId()));
        assertFalse(rememberedTransactions.contains(transactionMock.getDefinition().getStartedFor()));
    }

}