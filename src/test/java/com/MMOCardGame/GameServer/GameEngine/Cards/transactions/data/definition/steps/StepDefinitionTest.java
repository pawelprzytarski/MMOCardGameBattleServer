package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.OptionsInfo;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StepDefinitionTest {

    private GameState gameState;

    @BeforeEach
    void setup(){
        gameState = Mockito.mock(GameState.class);
    }

    @Test
    void createdTargetsStep_indicatingToSelectOneOption_shouldReturnOptionsInfoWithSelectOneOption(){
        var step = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, 1);

        OptionsInfo options = step.getPossibleOptionsInfo(gameState, -1);

        assertEquals(1, options.getCountOfTargets());
    }

    @Test
    void createdTargetsStep_withFunctionIndicatingToSelectOneOption_shouldReturnOptionsInfoWithSelectOneOption(){
        var step = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> true, (list, gameState) -> 1);

        OptionsInfo options = step.getPossibleOptionsInfo(gameState, -1);

        assertEquals(1, options.getCountOfTargets());
    }

    @Test
    void createdTargetsStep_passingAllCardsWithOneCardInGamesState_shouldReturnOneCardInOptions(){
        Card mockedCard = Mockito.mock(Card.class);
        Set<Card> cards = new HashSet<>();
        cards.add(mockedCard);
        Mockito.when(gameState.getAllCards()).thenReturn(cards);
        var step = new TargetsStepDefinition((gameState, player) -> false, (gameState, card) -> true, 1);

        OptionsInfo options = step.getPossibleOptionsInfo(gameState, -1);

        assertEquals(1, options.getOptions().size());
        assertEquals(TargetType.Card, options.getOptions().get(0).getTargetType());
    }

    @Test
    void createdTargetsStep_passingNoCardsWithOneCardInGamesState_shouldReturnEmptyOptions(){
        Card mockedCard = Mockito.mock(Card.class);
        Set<Card> cards = new HashSet<>();
        cards.add(mockedCard);
        Mockito.when(gameState.getAllCards()).thenReturn(cards);
        var step = new TargetsStepDefinition((gameState, player) -> true, (gameState, card) -> false, 1);

        OptionsInfo options = step.getPossibleOptionsInfo(gameState, -1);

        assertTrue(options.getOptions().isEmpty());
    }

}