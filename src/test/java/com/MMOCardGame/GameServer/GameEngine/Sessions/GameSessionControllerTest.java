package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameReferee;
import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.RandomCardsPicker;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.LinksPoints;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.PhasesStack;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.DrawCardScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.MainPhaseEndedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.PhaseEndedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.*;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import com.MMOCardGame.GameServer.GameEngine.factories.GameRefereeBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.GameStateBuilder;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.Pair;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GameSessionControllerTest {
    private static Stream<Arguments> doTargetSelecting_IncorrectTargetType_params() {
        return Stream.of(
                Arguments.of(TargetType.Player, TargetSelectingTransactionType.StartTransaction),
                Arguments.of(TargetType.UndefinedType, TargetSelectingTransactionType.StartTransaction),
                Arguments.of(TargetType.Player, TargetSelectingTransactionType.SelectingOption),
                Arguments.of(TargetType.UndefinedType, TargetSelectingTransactionType.SelectingOption)
        );
    }

    @Test
    void endPhase_correctUser_changedPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Main.ordinal()).build();
        controller.endPhase(1, message);

        assertEquals(GamePhase.Attack, controller.getAllGameState().getCurrentPhase());
        assertEquals(1, controller.getAllGameState().getCurrentPlayer());
    }

    private GameSessionControllerBuilderForTests getGameSessionWithPhasesStack(PhasesStack phasesStack) {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(phasesStack);
        GameStateBuilder gameStateBuilder = builder.getGameStateBuilder();
        GameState gameState = gameStateBuilder.build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        Map<Integer, Player> playerMap = new HashMap<>();
        playerMap.put(0, new Player(1, 0, 20, null, null, null));
        playerMap.put(1, new Player(1, 1, 20, null, null, null));
        gameState.setPlayers(playerMap);
        return builder;
    }

    @Test
    void endPhase_correctUserWaitingPhase_changedPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1));
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();
        phasesStack.setStopNotifying(true);

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Waiting.ordinal()).build();
        controller.endPhase(1, message);
        controller.endPhase(0, message);

        assertEquals(GamePhase.FirstHandDraw, controller.getAllGameState().getCurrentPhase());
        assertEquals(-1, controller.getAllGameState().getCurrentPlayer());
    }

    @Test
    void endPhase_correctUserWaitingPhaseEndPhaseTwoTimes_changedPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1));
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();
        phasesStack.setStopNotifying(true);

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Waiting.ordinal()).build();
        controller.endPhase(1, message);
        controller.endPhase(0, message);
        message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.FirstHandDraw.ordinal()).build();
        controller.endPhase(1, message);
        controller.endPhase(0, message);

        assertEquals(GamePhase.Untap, controller.getAllGameState().getCurrentPhase());
        assertEquals(0, controller.getAllGameState().getCurrentPlayer());
    }

    @Test
    void endPhase_correctUser_sendMessageToAllUsers() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        GameSessionController controller = builder.build();
        UserConnection userConnection1 = builder.getUserConnection1();
        UserConnection userConnection2 = builder.getUserConnection2();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Main.ordinal()).build();
        controller.endPhase(1, message);

        verify(userConnection1).sendMessage(any(PhaseChanged.class));
        verify(userConnection2).sendMessage(any(PhaseChanged.class));
    }

    @Test
    void endPhase_incorrectUser_doNotChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Main.ordinal()).build();
        try {
            controller.endPhase(0, message);
        } catch (InconsistentPlayerCommand ignore) {
        }

        assertEquals(GamePhase.Main, controller.getAllGameState().getCurrentPhase());
        assertEquals(1, controller.getAllGameState().getCurrentPlayer());
    }

    @Test
    void endPhase_incorrectUser_throwsInconsistentException() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Main.ordinal()).build();

        assertThrows(InconsistentPlayerCommand.class, () -> controller.endPhase(0, message));
    }

    @Test
    void endPhase_incorrectUserChanged_doNotSendMessageToAllUsers() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        GameSessionController controller = builder.build();
        UserConnection userConnection1 = builder.getUserConnection1();
        UserConnection userConnection2 = builder.getUserConnection2();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Main.ordinal()).build();
        try {
            controller.endPhase(0, message);
        } catch (InconsistentPlayerCommand ignore) {
        }

        verify(userConnection1, times(0)).sendMessage(any(PhaseChanged.class));
        verify(userConnection2, times(0)).sendMessage(any(PhaseChanged.class));
    }

    @Test
    void endPhase_correctUserInconsistentPreviousPhase_doNotChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Attack.ordinal()).build();
        try {
            controller.endPhase(1, message);
        } catch (InconsistentPlayerCommand ignore) {
        }

        assertEquals(GamePhase.Main, controller.getAllGameState().getCurrentPhase());
        assertEquals(1, controller.getAllGameState().getCurrentPlayer());
    }

    @Test
    void endPhase_correctUserInconsistentPreviousPhase_throwsInconsistentException() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Attack.ordinal()).build();

        assertThrows(InconsistentPlayerCommand.class, () -> controller.endPhase(1, message));
    }

    @Test
    void endPhase_correctUserInconsistentPreviousPhase_doNotSendMessageToAllUsers() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        GameSessionController controller = builder.build();
        UserConnection userConnection1 = builder.getUserConnection1();
        UserConnection userConnection2 = builder.getUserConnection2();

        PhaseChanged message = PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.Untap.ordinal()).build();
        try {
            controller.endPhase(1, message);
        } catch (InconsistentPlayerCommand ignore) {
        }

        verify(userConnection1, times(0)).sendMessage(any(PhaseChanged.class));
        verify(userConnection2, times(0)).sendMessage(any(PhaseChanged.class));
    }

    @Test
    void keepStartingHand_correctUser_changePhaseToUntap() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        controller.keepStartingHand(1);

        assertEquals(GamePhase.Untap, controller.gameState.getCurrentPhase());
    }

    @Test
    void keepStartingHand_correctUserButIncorrectPhase_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        assertThrows(InconsistentPlayerCommand.class, () -> controller.keepStartingHand(1));
    }

    @Test
    void keepStartingHand_correctUserButKeptHand_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        controller.keepStartingHand(1);//first keep
        assertThrows(InconsistentPlayerCommand.class, () -> controller.keepStartingHand(1));
    }

    private GameSessionController getGameSessionWithGameRefereeReturningEmptyMuligan(PhasesStack phasesStack) {
        GameReferee gameReferee = mock(GameReferee.class);
        GameRefereeBuilder gameRefereeBuilder = mock(GameRefereeBuilder.class);
        when(gameRefereeBuilder.setGameSettings(any())).thenCallRealMethod();
        when(gameRefereeBuilder.setGameSession(any())).thenCallRealMethod();
        when(gameRefereeBuilder.build()).thenReturn(gameReferee);
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        builder.setGameRefereeBuilder(gameRefereeBuilder);
        return builder.build();
    }

    @Test
    void muligan_correctUserButIncorrectPhase_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        assertThrows(InconsistentPlayerCommand.class, () -> controller.muligan(1));
    }

    @Test
    void muligan_correctUserButKeptHand_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        controller.keepStartingHand(1);
        assertThrows(InconsistentPlayerCommand.class, () -> controller.muligan(1));
    }

    @Test
    void replaceCards_correctUserButIncorrectPhase_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        assertThrows(InconsistentPlayerCommand.class, () -> controller.replaceCards(handChange));
    }

    @Test
    void replaceCards_correctUserButKeptHand_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        controller.keepStartingHand(1);
        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .addCardInstances(1)
                .addCardInstances(2)
                .build();
        assertThrows(InconsistentPlayerCommand.class, () -> controller.replaceCards(handChange));
    }

    @Test
    void replaceCards_correctUserButIncompleteMessage_throwsInconsistentPlayerCommand() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        HandChanged handChange = HandChanged.newBuilder().setPlayer(1)
                .setCount(2)
                .build();
        assertThrows(InconsistentPlayerCommand.class, () -> controller.replaceCards(handChange));
    }

    @Test
    void endPhase_endFirstDraw_startFirstPlayerUntap() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionController controller = getGameSessionWithPlayer(phasesStack);

        controller.keepStartingHand(1);

        assertEquals(GamePhase.Untap, phasesStack.getCurrentPhase());
        assertEquals(0, phasesStack.getCurrentPlayer());
    }

    private GameSessionController getGameSessionWithPlayer(PhasesStack phasesStack) {
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();
        controller.getAllGameState().setPlayers(Collections.singletonMap(1, new Player(0, 1, 20, new int[]{1}, new int[]{1}, new int[]{1})));
        return controller;
    }

    @Test
    void endPhase_endFirstDraw_sendChangePhaseMessageToAll() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.FirstHandDraw, 1);
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        UserConnection connection1 = builder.getUserConnection1();
        UserConnection connection2 = builder.getUserConnection2();
        GameSessionController controller = builder.build();
        controller.getAllGameState().setPlayers(Collections.singletonMap(1, new Player(0, 1, 20, new int[]{1}, new int[]{1}, new int[]{1})));

        controller.keepStartingHand(1);

        verify(connection1).sendMessage(any(PhaseChanged.class));
        verify(connection2).sendMessage(any(PhaseChanged.class));
    }

    @Test
    void endPhase_endEndRound_changeToNextPlayerUntap() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.EndRound, 1);
        GameSessionController controller = getGameSessionWithPhasesStack(phasesStack).build();

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        assertEquals(GamePhase.Untap, phasesStack.getCurrentPhase());
        assertEquals(0, phasesStack.getCurrentPlayer());
    }

    @Ignore
    void endPhase_endEndRound_removeOverloadPointFromAllPlayerCardsOnBattleField() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.EndRound, 1);
        List<Card> cards = Arrays.asList(getCardWithOverloadPoint(1), getCardWithTwoOverloadPoint(2), getCardWithOverloadPoint(3), getCard(4));
        GameSessionController controller = getGameSessionWithOverloadedCards(cards, phasesStack).build();

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        List<Card> expected = Arrays.asList(getCard(1), getCardWithOverloadPoint(2), getCardWithOverloadPoint(3), getCard(4));
        expected.get(0).setRevealed(true);
        expected.get(1).setRevealed(true);
        assertEquals(expected, cards);
    }

    private GameSessionControllerBuilderForTests getGameSessionWithOverloadedCards(List<Card> cards, PhasesStack phasesStack) {
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        GameState gameState = builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, cards);
        gameState.moveCardToBattlefield(1);
        gameState.moveCardToBattlefield(2);
        return builder;
    }

    private GameSessionControllerBuilderForTests getGameSessionWithCards(List<Card> cards, PhasesStack phasesStack) {
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        GameState gameState = builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, cards);
        return builder;
    }

    private Card getCardWithOverloadPoint(int id) {
        Card card = getCard(id);
        card.incrementOverloadPoints();
        return card;
    }

    private Card getCardWithTwoOverloadPoint(int id) {
        Card card = getCard(id);
        card.incrementOverloadPoints();
        card.incrementOverloadPoints();
        return card;
    }

    private Card getCard(int id) {
        return new Card(id, id, new CardStatistics(1, 1, 2, 2, 0, 0, 0, 0), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", new CardScript() {
            @Override
            public CardAbilityTask abilityAction(@Nonnull GameState gameState, int decisionId) {
                return gameStateParam -> {
                };
            }

            @Override
            public Collection<TargetSelectingTransaction> getSelectedOptionsDuringTransaction(int transactionId) {
                return new ArrayList<>();
            }
        });
    }

    @Ignore
    void endPhase_endEndRound_sendMessageAboutChangeOverload() {
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderWithEndRoundPhase();
        GameSessionController controller = builder.build();
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForUserConnections(builder, answers1, answers2);

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        PhaseChanged phaseChanged = PhaseChanged.newBuilder()
                .setOldPhaseValue(GamePhase.EndRound.ordinal())
                .setOldPlayer(1)
                .setNewPhaseValue(GamePhase.Untap.ordinal())
                .setNewPlayer(0)
                .build();

        Card card = getCard(1);
        CardStatisticsChanged cardStatisticsChange1 = CardStatisticsChanged.newBuilder()
                .setCardInstanceId(1)
                .setOverloadPoints(0)
                .setAttackValue(card.getCurrentStatistics().getAttack())
                .setDefenceValue(card.getCurrentStatistics().getDefence())
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .build())
                .setOwner(0)
                .setPositionValue(CardPosition.Position.Battlefield.ordinal())
                .build();
        CardStatisticsChanged cardStatisticsChange2 = CardStatisticsChanged.newBuilder()
                .setCardInstanceId(2)
                .setOverloadPoints(1)
                .setAttackValue(card.getCurrentStatistics().getAttack())
                .setDefenceValue(card.getCurrentStatistics().getDefence())
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .build())
                .setOwner(0)
                .setPositionValue(CardPosition.Position.Battlefield.ordinal())
                .build();
        List expected = Arrays.asList(phaseChanged, cardStatisticsChange1, cardStatisticsChange2);
        assertEquals(expected, answers1);
        assertEquals(expected, answers2);
    }

    private GameSessionControllerBuilderForTests getGameSessionBuilderWithEndRoundPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.EndRound, 1);
        return getGameSessionBuilderWithDefaultCards(phasesStack);
    }

    private void setReadersForUserConnections(GameSessionControllerBuilderForTests builder, List<Object> answers1, List<Object> answers2) {
        UserConnection connection1 = builder.getUserConnection1();
        UserConnection connection2 = builder.getUserConnection2();
        doAnswer(invocationOnMock -> answers1.add(invocationOnMock.getArgument(0))).when(connection1).sendMessage(any(CardStatisticsChanged.class));
        doAnswer(invocationOnMock -> answers2.add(invocationOnMock.getArgument(0))).when(connection2).sendMessage(any(CardStatisticsChanged.class));
        doAnswer(invocationOnMock -> answers1.add(invocationOnMock.getArgument(0))).when(connection1).sendMessage(any(PhaseChanged.class));
        doAnswer(invocationOnMock -> answers2.add(invocationOnMock.getArgument(0))).when(connection2).sendMessage(any(PhaseChanged.class));
    }

    private GameSessionControllerBuilderForTests getGameSessionBuilderWithDefaultCards(PhasesStack phasesStack) {
        List<Card> cards = Arrays.asList(getCardWithOverloadPoint(1), getCardWithTwoOverloadPoint(2), getCardWithOverloadPoint(3), getCard(4));
        return getGameSessionWithOverloadedCards(cards, phasesStack);
    }

    @Ignore
    void endPhase_endEndRound_invokeTriggerBeforeUntap() {
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderWithEndRoundPhase();
        UserConnection connection1 = builder.getUserConnection1();
        GameSessionController controller = builder.build();
        List<Object> answers1 = new ArrayList<>();
        doAnswer(invocationOnMock -> answers1.add("Invoke untap message")).when(connection1).sendMessage(any(CardStatisticsChanged.class));
        TestPhaseEndedScript testPhaseEndedScript = new TestPhaseEndedScript(answers1);
        controller.getAllGameState().getTriggers().registerCardScript(testPhaseEndedScript);

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        assertEquals(Arrays.asList("Invoke trigger", "Invoke untap message", "Invoke untap message"), answers1);
    }

    @Test
    void endPhase_startUntapPhase_drawNewCard() {
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderWithEndRoundPhase();
        setRandomCardsPickerReturingThreeMock(builder);
        GameSessionController controller = builder.build();

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        CardPosition.Position position = controller.gameState.getCardsFieldForPlayer(0).getCardPosition(3);
        assertEquals(CardPosition.Position.Hand, position);
    }

    private void setRandomCardsPickerReturingThreeMock(GameSessionControllerBuilderForTests builder) {
        RandomCardsPicker randomCardsPicker = mock(RandomCardsPicker.class);
        when(randomCardsPicker.getRandomCards(any(), anyInt())).thenReturn(Collections.singletonList(3));
        when(randomCardsPicker.getRandomCard(any())).thenReturn(3);
        builder.getGameRefereeBuilder().setRandomCardPicker(randomCardsPicker);
    }

    private void setReadersForUserConnectionsOnCardDraw(GameSessionControllerBuilderForTests builder, List<Object> answers1, List<Object> answers2) {
        Class<?> messageType = HandChanged.class;
        setReadersForSendingMessage(builder, answers1, answers2, messageType);
    }

    private void setReadersForSendingMessage(GameSessionControllerBuilderForTests builder, List<Object> answers1, List<Object> answers2, Class<?> messageType) {
        setReadersForSendingMessages(answers1, messageType, builder.getUserConnection1());
        setReadersForSendingMessages(answers2, messageType, builder.getUserConnection2());
    }

    private void setReadersForSendingMessages(List<Object> answers2, Class<?> messageType, UserConnection userConnection2) {
        doAnswer(invocationOnMock -> answers2.add(invocationOnMock.getArgument(0))).when(userConnection2).sendMessage(any(messageType));
    }

    @Ignore
    void endPhase_startUntapPhase_drawAfterUntapCards() {
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderWithEndRoundPhase();
        setRandomCardsPickerReturingThreeMock(builder);
        GameSessionController controller = builder.build();
        UserConnection connection1 = builder.getUserConnection1();
        List<Object> answers1 = new ArrayList<>();
        doAnswer(invocationOnMock -> answers1.add("Invoke untap message")).when(connection1).sendMessage(any(CardStatisticsChanged.class));
        TestDrawCardScript testDrawCardScript = new TestDrawCardScript(answers1);
        controller.getAllGameState().getTriggers().registerCardScript(testDrawCardScript);

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        assertEquals(Arrays.asList("Invoke untap message", "Invoke untap message", "Invoke trigger"), answers1);
    }

    @Test
    void endPhase_startUntapPhaseAndNoCardsInLibrary_defeatPlayer() {
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderWithEndRoundPhase();
        setRandomCardsPickerReturingThreeMock(builder);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        controller.gameState.moveCardToHand(2);
        controller.gameState.moveCardToHand(3);
        controller.gameState.moveCardToHand(4);
        Triggers triggers = mock(Triggers.class);
        controller.gameState.getPlayer(0).setTriggers(triggers);

        controller.endPhase(1, PhaseChanged.newBuilder().setOldPhaseValue(GamePhase.EndRound.ordinal()).build());

        verify(triggers).invokeTrigger(new PlayerDefeatedEvent(0));
    }

    @Test
    void moveCard_moveCardToHand_sendCardMovedMessage() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        GameSessionController controller = builder.build();
        UserConnection connection1 = builder.getUserConnection1();
        UserConnection connection2 = builder.getUserConnection2();

        controller.gameState.moveCardToHand(1);

        verify(connection1).sendMessage(any(CardMoved.class));
        verify(connection2).sendMessage(any(CardMoved.class));
    }

    @Test
    void moveCard_moveCardToSameField_noSendMessage() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        GameSessionController controller = builder.build();
        UserConnection connection1 = builder.getUserConnection1();
        UserConnection connection2 = builder.getUserConnection2();

        controller.gameState.moveCardToLibrary(1);

        verify(connection1, times(0)).sendMessage(any());
        verify(connection2, times(0)).sendMessage(any());
    }

    @Test
    void moveCard_moveCardToOtherField_sendCardMovedMessage() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        GameSessionController controller = builder.build();
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardMoved.class);

        controller.gameState.moveCardToHand(1);
        controller.gameState.moveCardToBattlefield(1);

        CardMoved cardMove1 = CardMoved.newBuilder()
                .setNewOwner(0)
                .setNewPositionValue(CardPosition.Position.Hand.ordinal())
                .setOldOwner(0)
                .setOldPositionValue(CardPosition.Position.Library.ordinal())
                .setCardInstanceId(1)
                .build();

        CardMoved cardMove2 = CardMoved.newBuilder()
                .setNewOwner(0)
                .setNewPositionValue(CardPosition.Position.Battlefield.ordinal())
                .setOldOwner(0)
                .setOldPositionValue(CardPosition.Position.Hand.ordinal())
                .setCardInstanceId(1)
                .build();
        assertEquals(Arrays.asList(cardMove1,cardMove2), answers1);
        assertEquals(Arrays.asList(cardMove1,cardMove2), answers2);
    }

    @Test
    void endPhase_endReactionPhases_doStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Arrays.asList(new Pair<>(GamePhase.Main, 1), new Pair<>(GamePhase.ReactionPhase, 1)));
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        GameSessionController controller = builder.build();
        controller.gameState.getReactionStack().pushAction(() -> {
        }, () -> {
        }, 0);
        List<Object> answers1 = new ArrayList<>();
        setReadersForSendingMessages(answers1, StackActionCompleted.class, builder.getUserConnection1());

        controller.gameState.endPhase(1);

        StackActionCompleted stackAction = StackActionCompleted.newBuilder().setStackId(0).build();
        assertEquals(Collections.singletonList(stackAction), answers1);
    }

    @Test
    void endPhase_endReactionPhasesAndTwoActionsOnStack_startNextReactionPhases() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Arrays.asList(new Pair<>(GamePhase.Main, 1), new Pair<>(GamePhase.ReactionPhase, 1)));
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        GameSessionController controller = builder.build();
        controller.gameState.getReactionStack().pushAction(() -> {
        }, () -> {
        }, 1);
        controller.gameState.getReactionStack().pushAction(() -> {
        }, () -> {
        }, 0);

        controller.gameState.endPhase(1);

        assertEquals(GamePhase.ReactionPhase, controller.gameState.getCurrentPhase());
        assertEquals(1, controller.gameState.getCurrentPlayer());
    }

    @Test
    void endPhase_endReactionPhasesAndTwoActionsOnStack_info() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Arrays.asList(new Pair<>(GamePhase.Main, 1), new Pair<>(GamePhase.ReactionPhase, 1)));
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        GameSessionController controller = builder.build();
        controller.gameState.getReactionStack().pushAction(() -> {
        }, () -> {
        }, 0);
        controller.gameState.getReactionStack().pushAction(() -> {
        }, () -> {
        }, 0);
        List<Object> answers1 = new ArrayList<>();
        setReadersForSendingMessages(answers1, PhaseChanged.class, builder.getUserConnection1());

        controller.gameState.endPhase(1);

        PhaseChanged stackAction = PhaseChanged.newBuilder()
                .setOldPlayer(1)
                .setOldPhaseValue(GamePhase.ReactionPhase.ordinal())
                .setNewPlayer(0)
                .setNewPhaseValue(GamePhase.ReactionPhase.ordinal())
                .build();
        assertEquals(Collections.singletonList(stackAction), answers1);
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndCorrectLinksPoints_sendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        var result = controller.useCard(cardUsed, 0);

        assertEquals(cardUsed, result);
        assertTrue(answers1.isEmpty());
        assertEquals(Collections.singletonList(cardUsed), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndCorrectLinksPoints_checkCardReaveled() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);
        int usedCardId = 1;

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(usedCardId)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        CardUsed result = controller.useCard(cardUsed, 0);

        assertEquals(cardUsed, result);
        assertTrue(answers1.isEmpty());
        assertEquals(Collections.singletonList(cardUsed), answers2);

        Card card = builder.getGameStateBuilder().build().getCard(usedCardId);

        assertTrue(card.isRevealed());
    }

    private GameSessionControllerBuilderForTests getGameSessionBuilderForLinksPoints(PhasesStack phasesStack, LinksPoints linksPoints) {
        List<Card> cards = Arrays.asList(getCard(1), getCard(2));
        GameSessionControllerBuilderForTests builder = getGameSessionWithCards(cards, phasesStack);
        builder.getGameStateBuilder().build().setPlayers(Collections.singletonMap(0, new Player(0, 0, 20, new int[]{0}, new int[]{0}, new int[]{0})));
        builder.getGameStateBuilder().build().getPlayer(0).getLinksPoints().addPoints(linksPoints);
        return builder;
    }

    @Test
    void useCard_incorrectPhaseAndCardInHandAndCorrectLinksPoints_noSendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Untap, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        assertThrows(BadRequestException.class, () -> controller.useCard(cardUsed, 0));

        assertEquals(new ArrayList<>(), answers1);
        assertEquals(new ArrayList<>(), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndCorrectLinksPointsButCardHaveOverloadpoints_noSendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        controller.gameState.getCard(1).setOverloadPoints(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        assertThrows(BadRequestException.class, () -> controller.useCard(cardUsed, 0));

        assertEquals(new ArrayList<>(), answers1);
        assertEquals(new ArrayList<>(), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndIncorrectLinksPoints_noSendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 0, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();

        assertThrows(BadRequestException.class, () -> controller.useCard(cardUsed, 0));

        assertEquals(new ArrayList<>(), answers1);
        assertEquals(new ArrayList<>(), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndCorrectLinksPointsWithoutNeutral_sendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsedRequest = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();

        CardUsed result = controller.useCard(cardUsedRequest, 0);

        var expectedCardUsed = CardUsed.newBuilder(cardUsedRequest)
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(2)
                        .setScanners(1)
                        .setPsychos(1)
                        .build())
                .build();
        assertEquals(expectedCardUsed, result);
        assertEquals(new ArrayList<>(), answers1);
        assertEquals(Arrays.asList(expectedCardUsed), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndCorrectLinksPointsWithoutNeutral_checkPlayerUsedLinksPoints() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed CardUsedRequest = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        controller.useCard(CardUsedRequest, 0);

        assertEquals(new LinksPoints(0, 0, 0, 0, 0), controller.gameState.getPlayer(0).getLinksPoints());
    }

    @Test
    void useCard_correctPhaseAndCardInHandAndCorrectLinksPointsWithoutNeutral_checkCardRevealed() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);
        int usedCardId = 1;

        CardUsed CardUsedRequest = CardUsed.newBuilder()
                .setCardInstanceId(usedCardId)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        try {
            controller.useCard(CardUsedRequest, 0);
        } catch (Exception ignored) {
        }

        Card card = builder.getGameStateBuilder().build().getCard(usedCardId);

        assertTrue(card.isRevealed());
    }

    @Test
    void useCard_correctPhaseAndCardOnBattlefieldAndCorrectLinksPoints_sendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        CardUsed result = controller.useCard(cardUsed, 0);

        assertEquals(cardUsed, result);
        assertTrue(answers1.isEmpty());
        assertEquals(Collections.singletonList(cardUsed), answers2);
    }

    @Test
    void useCard_incorrectPhaseAndCardOnBattlefieldAndCorrectLinksPoints_noSendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Untap, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();

        assertThrows(BadRequestException.class, () -> controller.useCard(cardUsed, 0));

        assertEquals(new ArrayList<>(), answers1);
        assertEquals(new ArrayList<>(), answers2);
    }

    @Test
    void useCard_incorrectPhaseAndCardOnBattlefieldAndCorrectLinksPoints_checkIsLinksPointsIsSame() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Untap, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        try {
            controller.useCard(cardUsed, 0);
        } catch (Exception ignored) {
        }

        assertEquals(new LinksPoints(2, 2, 0, 0, 0), controller.gameState.getPlayer(0).getLinksPoints());
    }

    @Test
    void useCard_incorrectPhaseAndCardInHandAndCorrectLinksPoints_checkIsLinksPointsIsSame() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Untap, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        try {
            controller.useCard(cardUsed, 0);
        } catch (Exception ignored) {
        }

        assertEquals(new LinksPoints(2, 2, 0, 0, 0), controller.gameState.getPlayer(0).getLinksPoints());
    }

    @Test
    void useCard_incorrectPhaseAndCardInHandAndCorrectLinksPoints_checkCardIsNotRevealed() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Untap, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 2, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToHand(1);
        int usedCardId = 1;

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(usedCardId)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();

        try {
            controller.useCard(cardUsed, 0);
        } catch (Exception ignored) {
        }

        Card card = builder.getGameStateBuilder().build().getCard(usedCardId);

        assertFalse(card.isRevealed());
    }

    @Test
    void useCard_correctPhaseAndCardOnBattlefieldAndIncorrectLinksPoints_noSendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(2, 0, 0, 0, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsed = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        assertThrows(BadRequestException.class, () -> controller.useCard(cardUsed, 0));

        assertEquals(new ArrayList<>(), answers1);
        assertEquals(new ArrayList<>(), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardOnBattlefieldAndCorrectLinksPointsWithoutNeutral_sendMessageAboutNewStackAction() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed cardUsedRequest = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        CardUsed result = controller.useCard(cardUsedRequest, 0);

        var expectedCardUsed = CardUsed.newBuilder(cardUsedRequest)
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(2)
                        .setScanners(1)
                        .setPsychos(1)
                        .build())
                .build();

        assertEquals(expectedCardUsed, result);
        assertTrue(answers1.isEmpty());
        assertEquals(Collections.singletonList(expectedCardUsed), answers2);
    }

    @Test
    void useCard_correctPhaseAndCardOnBattlefieldAndCorrectLinksPointsWithoutNeutral_checkPlayerUsedLinksPoints() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);

        CardUsed CardUsedRequest = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        controller.useCard(CardUsedRequest, 0);

        assertEquals(new LinksPoints(0, 0, 0, 0, 0), controller.gameState.getPlayer(0).getLinksPoints());
    }

    @Test
    void useCard_correctPhaseAndCardOnBattlefieldAndCorrectLinksPoints_checkCardGetOverloadPoints() {
        final int abilityOverloadCost = 4;
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        LinksPoints linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        GameSessionControllerBuilderForTests builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();
        controller.gameState.moveCardToBattlefield(1);
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, CardUsed.class);
        controller.gameState.getCard(1).getCardScript().setAbilityOverloadCost(abilityOverloadCost);

        CardUsed CardUsedRequest = CardUsed.newBuilder()
                .setCardInstanceId(1)
                .setCardCosts(CardCosts.newBuilder()
                        .setNeutral(2)
                        .setRaiders(2)
                        .build())
                .setStackId(0)
                .setCardAbilityId(-1)
                .build();
        controller.useCard(CardUsedRequest, 0);

        assertEquals(abilityOverloadCost, controller.gameState.getCard(1).getOverloadPoints());
    }

    @ParameterizedTest
    @ValueSource(ints = {TargetSelectingTransactionType.Result_VALUE, TargetSelectingTransactionType.AvailableOptions_VALUE})
    void doTargetSelectingTransaction_incorrectTransactionType_throwBadRequestException(int type) {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        var linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        var builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();

        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setTypeValue(type)
                .build();

        assertThrows(BadRequestException.class, () -> controller.doTargetSelectingTransaction(targetSelectingTransaction));
    }

    @ParameterizedTest
    @MethodSource(value = "doTargetSelecting_IncorrectTargetType_params")
    void doTargetSelectingTransaction_incorrectTransactionTargetType_throwBadRequestException(TargetType targetType, TargetSelectingTransactionType transactionType) {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        var linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        var builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();

        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setType(transactionType)
                .setDestinationTargetType(targetType)
                .setDestinationTargetId(2)
                .addOptions(TargetSelectingOption.newBuilder().setId(1).build())
                .build();

        assertThrows(BadRequestException.class, () -> controller.doTargetSelectingTransaction(targetSelectingTransaction));
    }

    @Test
    void doTargetSelectingTransaction_withoutTargetId_throwBadRequestException() {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        var linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        var builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();

        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setType(TargetSelectingTransactionType.StartTransaction)
                .setDestinationTargetTypeValue(TargetType.Card_VALUE)
                .build();

        assertThrows(BadRequestException.class, () -> controller.doTargetSelectingTransaction(targetSelectingTransaction));
    }

    @Test
    void doTargetSelectingTransaction_withoutOptionInOptionSelecting_throwBadRequestException() {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        var linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        var builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();

        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setType(TargetSelectingTransactionType.SelectingOption)
                .setDestinationTargetType(TargetType.Card)
                .addOptions(TargetSelectingOption.newBuilder().setId(1).build())
                .build();

        assertThrows(BadRequestException.class, () -> controller.doTargetSelectingTransaction(targetSelectingTransaction));
    }

    @Test
    void doTargetSelectingTransaction_withoutTargetType_throwBadRequestException() {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        var linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        var builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();

        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setType(TargetSelectingTransactionType.StartTransaction)
                .setDestinationTargetId(2)
                .build();

        assertThrows(BadRequestException.class, () -> controller.doTargetSelectingTransaction(targetSelectingTransaction));
    }

    @Test
    void doTargetSelectingTransaction_notExistingUnit_throwNotFoundException() {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        var linksPoints = new LinksPoints(0, 2, 1, 1, 0);
        var builder = getGameSessionBuilderForLinksPoints(phasesStack, linksPoints);
        GameSessionController controller = builder.build();

        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setType(TargetSelectingTransactionType.StartTransaction)
                .setDestinationTargetType(TargetType.Card)
                .setDestinationTargetId(2000)
                .build();

        assertThrows(NotFoundException.class, () -> controller.doTargetSelectingTransaction(targetSelectingTransaction));
    }

    @Test
    void endPhase_haveMultipleCardsWithTriggers_addReactionStacksElementsAndGamePhases() {
        List<Card> cards = Arrays.asList(getCardWithScript(1, new TriggerCardScript_NewReactionStackElement()), getCardWithScript(2, new TriggerCardScript_NewReactionStackElement()),
                getCardWithScript(3, new TriggerCardScript_TransactionTrigger()), getCardWithScript(4, new TriggerCardScript_TransactionTrigger()),
                getCardWithScript(5, new TriggerCardScript_NewReactionStackElement()), getCardWithScript(6, new TriggerCardScript_TransactionTrigger()));
        GameSessionControllerBuilderForTests builder = getGameControllerBuilderWithCards(cards);
        GameSessionController controller = builder.build();
        for (int i = 1; i <= 6; i++)
            controller.gameState.moveCardToBattlefield(i);

        controller.gameState.endPhase(0);

        var expectedStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Attack, 0)));
        for (int i = 6; i >= 1; i--)
            expectedStack.startTriggerPhase(TriggerType.MainPhaseEnded, i);
        expectedStack.startReactionPhasesInsteadTriggerPhase(TriggerType.MainPhaseEnded, 1);
        assertEquals(expectedStack, controller.gameState.getPhasesStack());
        assertEquals(1, controller.gameState.getReactionStack().getElementsCount());
    }

    private Card getCardWithScript(int id, CardScript cardScript) {
        return new Card(id, id, new CardStatistics(1, 1, 2, 2, 0, 0, 0, 0), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "",
                cardScript);
    }

    private GameSessionControllerBuilderForTests getGameControllerBuilderWithCards(List<Card> cards) {
        var phasesStack = new PhasesStack(Arrays.asList(0, 1), Collections.singletonList(new Pair<>(GamePhase.Main, 0)));
        GameSessionControllerBuilderForTests builder = getGameSessionWithPhasesStack(phasesStack);
        GameState gameState = builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, cards);
        builder.getGameStateBuilder().build().setPlayers(Collections.singletonMap(0, new Player(0, 0, 20, new int[]{0}, new int[]{0}, new int[]{0})));
        builder.getGameStateBuilder().build().setPlayers(Collections.singletonMap(1, new Player(0, 0, 20, new int[]{0}, new int[]{0}, new int[]{0})));
        return builder;
    }

    @Test
    void endPhase_haveMultipleCardsWithTriggers_sendPhaseChangedOnlyOnesForAllTriggerPhase() {
        List<Card> cards = Arrays.asList(getCardWithScript(1, new TriggerCardScript_NewReactionStackElement()), getCardWithScript(2, new TriggerCardScript_NewReactionStackElement()),
                getCardWithScript(3, new TriggerCardScript_TransactionTrigger()), getCardWithScript(4, new TriggerCardScript_TransactionTrigger()),
                getCardWithScript(5, new TriggerCardScript_NewReactionStackElement()), getCardWithScript(6, new TriggerCardScript_TransactionTrigger()));
        GameSessionControllerBuilderForTests builder = getGameControllerBuilderWithCards(cards);
        GameSessionController controller = builder.build();
        for (int i = 1; i <= 6; i++)
            controller.gameState.moveCardToBattlefield(i);

        controller.gameState.endPhase(0);

        PhaseChanged phaseChanged = PhaseChanged.newBuilder()
                .setOldPhase(com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase.Attack)
                .setOldPlayer(0)
                .setNewPhase(com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase.TriggerPhase)
                .setNewPlayer(-1)
                .build();

        verify(builder.getUserConnection1()).sendMessage(phaseChanged);
        verify(builder.getUserConnection2()).sendMessage(phaseChanged);
        verify(builder.getUserConnection1(), times(3)).sendMessage(any(PhaseChanged.class));
        verify(builder.getUserConnection1(), times(3)).sendMessage(any(PhaseChanged.class));
    }

    @Test
    void endPhase_haveMultipleCardsWithTriggersEndPhases_sendPhaseChangedOnlyOnesForTriggerPhase() {
        List<Card> cards = Arrays.asList(getCardWithScript(1, new TriggerCardScript_NewReactionStackElement()), getCardWithScript(2, new TriggerCardScript_NewReactionStackElement()),
                getCardWithScript(3, new TriggerCardScript_TransactionTrigger()), getCardWithScript(4, new TriggerCardScript_TransactionTrigger()),
                getCardWithScript(5, new TriggerCardScript_NewReactionStackElement()), getCardWithScript(6, new TriggerCardScript_TransactionTrigger()));
        GameSessionControllerBuilderForTests builder = getGameControllerBuilderWithCards(cards);
        GameSessionController controller = builder.build();
        for (int i = 1; i <= 6; i++)
            controller.gameState.moveCardToBattlefield(i);

        controller.gameState.endPhase(0);
        for (int i = 1; i <= 6; i++) {
            controller.gameState.endPhase(1);
            controller.gameState.endPhase(0);
            controller.gameState.getPhasesStack().endTriggerPhase(TriggerType.MainPhaseEnded, i);
        }

        PhaseChanged phaseChanged = PhaseChanged.newBuilder()
                .setNewPhase(com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase.Attack)
                .setNewPlayer(0)
                .setOldPhase(com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase.TriggerPhase)
                .setOldPlayer(-1)
                .build();

        verify(builder.getUserConnection1()).sendMessage(phaseChanged);
        verify(builder.getUserConnection2()).sendMessage(phaseChanged);
    }

    @Test
    void triggerAction_hasRegisteredTrigger_runActions() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(cardScript.isTransactionNeeded(any())).thenReturn(true);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);

        controller.doTriggerReaction(TriggerActivated.newBuilder()
                .setTransactionId(4)
                .setCardInstanceId(10)
                .setTriggerType(com.MMOCardGame.GameServer.Servers.ProtoBuffers.TriggerType.TriggerActivated_)
                .build());

        verify(cardScript).startTriggerActions(TriggerType.TriggerActivated, 4);
    }

    @Test
    void triggerAction_hasNotRegisteredTrigger_notRunActionsAndThrowsBadRequestException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(cardScript.isTransactionNeeded(any())).thenReturn(true);
        when(cardScript.getTriggerTransactionPlayer(any())).thenReturn(1);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        reset(cardScript);//needed because mock invoked in multiple places

        assertThrows(InconsistentPlayerCommand.class, () -> controller.doTriggerReaction(TriggerActivated.newBuilder().setTransactionId(4).setCardInstanceId(10).build()));

        verifyZeroInteractions(cardScript);
    }

    @Test
    void triggerAction_hasRegisteredTriggerButMessageTryRunForIncorrectCard_notRunActionsAndThrowsBadRequestException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(cardScript.isTransactionNeeded(any())).thenReturn(true);
        when(cardScript.getTriggerTransactionPlayer(any())).thenReturn(1);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);
        reset(cardScript);//needed because mock invoked in multiple places

        assertThrows(BadRequestException.class, () -> controller.doTriggerReaction(TriggerActivated.newBuilder().setTransactionId(4).setCardInstanceId(1000).build()));

        verifyZeroInteractions(cardScript);
    }

    @Test
    void triggerAction_hasRegisteredTriggerButMessageNoContainsCardId_notRunActionsAndThrowsBadRequestException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(cardScript.isTransactionNeeded(any())).thenReturn(true);
        when(cardScript.getTriggerTransactionPlayer(any())).thenReturn(1);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);
        reset(cardScript);//needed because mock invoked in multiple places

        assertThrows(BadRequestException.class, () -> controller.doTriggerReaction(TriggerActivated.newBuilder().setTransactionId(4).build()));

        verifyZeroInteractions(cardScript);
    }

    @Test
    void triggerAction_hasRegisteredTriggerButMessageNoContainsTransactionId_notRunActionsAndThrowsBadRequestException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(cardScript.isTransactionNeeded(any())).thenReturn(true);
        when(cardScript.getTriggerTransactionPlayer(any())).thenReturn(1);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);
        reset(cardScript);//needed because mock invoked in multiple places

        assertThrows(BadRequestException.class, () -> controller.doTriggerReaction(TriggerActivated.newBuilder().setCardInstanceId(10).setTransactionId(-1).build()));

        verifyZeroInteractions(cardScript);
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsIncorrectPhase_throwsInconsistentStateException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Main,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        assertThrows(InconsistentPlayerCommand.class, () -> controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .build()));
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndListWithNonExistingCards_throwsBadRequestException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        assertThrows(BadRequestException.class, () -> controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .build())
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(100)
                        .setTransactionId(0)
                        .build())
                .build()));

        assertTrue(controller.gameState.getAttackersForPlayer(0).isEmpty());
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndTryUseCardNotBelongToPlayer_throwsBadRequestException() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        assertThrows(BadRequestException.class, () -> controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(3)
                        .setTransactionId(1)
                        .build())
                .build()));

        assertTrue(controller.gameState.getAttackersForPlayer(0).isEmpty());
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndListWithExistingCards_saveAttackers() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .build())
                .build());

        assertEquals(Collections.singletonMap(1,1),controller.gameState.getAttackersForPlayer(0));
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndTryUseCardWithOverloadPoints_saveAttackerWithoutOverloadPoints() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        var cardWithOverloadPoints=getCard(1);
        cardWithOverloadPoints.setOverloadPoints(1);
        gameState.setCardsForUser(0, Arrays.asList(cardWithOverloadPoints, getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .build())
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(2)
                        .setTransactionId(1)
                        .build())
                .build());

        assertEquals(Collections.singletonMap(2,1),controller.gameState.getAttackersForPlayer(0));
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndListWithExistingCards_endPhase() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .build())
                .build());

        assertEquals(GamePhase.Defence,controller.gameState.getCurrentPhase());
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndListWithExistingCards_sendMessagesToAll() {
        var returnedTransaction=Arrays.asList(TargetSelectingTransaction.newBuilder().setTransactionId(100).build());
        CardScript cardScript=mock(CardScript.class);
        when(cardScript.getSelectedOptionsDuringTransaction(anyInt())).thenReturn(returnedTransaction);
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCardWithScript(1,cardScript), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .build())
                .build());

        var expectedMessage=AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .addAllTransaction(returnedTransaction)
                        .build())
                .build();

        verify(builder.getUserConnection1()).sendMessage(expectedMessage);
        verify(builder.getUserConnection2()).sendMessage(expectedMessage);
    }

    @Test
    void selectAttackersDefenders_hasTwoCardsAndRunAttackAndDefendPhases_sendMessagesForBoth() {
        var returnedTransaction1=Arrays.asList(TargetSelectingTransaction.newBuilder().setTransactionId(100).build());
        var returnedTransaction2=Arrays.asList(TargetSelectingTransaction.newBuilder().setTransactionId(120).build());
        CardScript cardScript1=mock(CardScript.class);
        when(cardScript1.getSelectedOptionsDuringTransaction(anyInt())).thenReturn(returnedTransaction1);
        CardScript cardScript2=mock(CardScript.class);
        when(cardScript2.getSelectedOptionsDuringTransaction(anyInt())).thenReturn(returnedTransaction2);

        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1),GamePhase.Attack,0));
        GameState gameState=builder.getGameStateBuilder().build();
        gameState.setCardsForUser(0, Arrays.asList(getCardWithScript(1,cardScript1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCardWithScript(3, cardScript2), getCard(4)));
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);

        controller.selectAttackersDefenders(0, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .build())
                .build());
        controller.selectAttackersDefenders(1, AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(3)
                        .setTransactionId(1)
                        .build())
                .build());

        var expectedMessage1=AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(1)
                        .setTransactionId(1)
                        .addAllTransaction(returnedTransaction1)
                        .build())
                .build();
        var expectedMessage2=AttackersDefendersSelected.newBuilder()
                .addTargets(AttackersDefendersSelected.AttackDefenceTarget.newBuilder()
                        .setAttackerId(3)
                        .setTransactionId(1)
                        .addAllTransaction(returnedTransaction2)
                        .build())
                .build();

        verify(builder.getUserConnection1()).sendMessage(expectedMessage1);
        verify(builder.getUserConnection2()).sendMessage(expectedMessage1);
        verify(builder.getUserConnection1()).sendMessage(expectedMessage2);
        verify(builder.getUserConnection2()).sendMessage(expectedMessage2);

    }



    static class TestPhaseEndedScript extends CardScript implements PhaseEndedScript {
        List<Object> answers;

        TestPhaseEndedScript(List<Object> answers1) {
            this.answers = answers1;
        }

        @Override
        public void phaseEnded(PhaseEndedEvent event) {
            answers.add("Invoke trigger");
        }
    }

    static class TestDrawCardScript extends CardScript implements DrawCardScript {
        List<Object> answers;

        TestDrawCardScript(List<Object> answers1) {
            this.answers = answers1;
        }

        @Override
        public void drawCard(DrawCardEvent event) {
            answers.add("Invoke trigger");
        }
    }

    class TriggerCardScript_NewReactionStackElement extends CardScript implements MainPhaseEndedScript {

        @Override
        public void phaseEnded(MainPhaseEndedEvent event) {
            this.addTriggerPhaseForMe(MainPhaseEndedScript.super.getTriggerType(), event);
        }

        @Override
        protected Pair<CardAbilityTask, Collection<TargetSelectingTransaction>> getTriggerTask(TriggerType type, TriggerEvent event, int transactionId) {
            return new Pair<>((g) -> {
            }, new ArrayList<>());
        }
    }

    class TriggerCardScript_TransactionTrigger extends CardScript implements MainPhaseEndedScript {

        @Override
        public void phaseEnded(MainPhaseEndedEvent event) {
            this.addTriggerPhaseForMe(MainPhaseEndedScript.super.getTriggerType(), event);
        }

        @Override
        public boolean isTransactionNeeded(TriggerType triggerType) {
            return true;
        }

        @Override
        protected Pair<CardAbilityTask, Collection<TargetSelectingTransaction>> getTriggerTask(TriggerType type, TriggerEvent event, int transactionId) {
            return new Pair<>((g) -> {
            }, new ArrayList<>());
        }

        @Override
        public TargetSelectingTransaction runTransactionAction(TargetSelectingTransaction message, GameState gameState) {
            return super.runTransactionAction(message, gameState);
        }
    }
}
