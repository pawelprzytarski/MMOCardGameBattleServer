package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.PhasesStack;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.ProtoMessagesHelper;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.CardMovedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.PhaseEndedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardRevealedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardStatisticsChangeEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.TriggerActivatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import com.MMOCardGame.GameServer.GameEngine.factories.GameStateBuilder;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.CallbackTask;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.Pair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EventsControllerTest {
    @Test
    void defeatPlayer_activePlayerWithCards_removePlayerFromList() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();

        controller.eventsController.removeDefeatedPlayerFromSession(0);

        assertEquals(Arrays.asList(1), new ArrayList<>(controller.gameState.getListOfActiveUsersIds()));
    }

    @Test
    void defeatPlayer_activePlayerWithCards_removeAllPlayerCards() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();

        controller.eventsController.removeDefeatedPlayerFromSession(0);

        assertNull(controller.gameState.getCardsFieldForPlayer(0));
    }

    @Test
    void defeatPlayer_activePlayerWithCards_removeCardsTriggers() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(phasesStack);
        GameStateBuilder gameStateBuilder = builder.getGameStateBuilder();
        GameState gameState = gameStateBuilder.build();
        TriggerCardScript triggerCardScript1 = new TriggerCardScript();
        TriggerCardScript triggerCardScript2 = new TriggerCardScript();
        gameState.setCardsForUser(0, Arrays.asList(getCardWithScript(1, triggerCardScript1), getCardWithScript(2, triggerCardScript2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        GameSessionController controller = builder.build();

        controller.eventsController.removeDefeatedPlayerFromSession(0);
        CardMovedEvent event = new CardMovedEvent(getCard(6),
                new CardPosition(0, CardPosition.Position.Library),
                new CardPosition(0, CardPosition.Position.Battlefield));
        controller.gameState.getTriggers().invokeTrigger(event);

        assertFalse(triggerCardScript1.invoked);
        assertFalse(triggerCardScript2.invoked);
    }

    private Card getCardWithScript(int id, CardScript cardScript) {
        return new Card(id, id, new CardStatistics(), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", cardScript);
    }

    @Test
    void defeatPlayer_activePlayerWithCards_disconnectPlayer() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionManager mockedSessionManager = builder.getSessionManager();
        List<Object> removedConnections = new ArrayList<>();
        doAnswer(invocationOnMock -> removedConnections.add(invocationOnMock.getArgument(0))).when(mockedSessionManager).removeConnectionOnSessionDemand(any());
        GameSessionController controller = builder.build();
        controller.gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2", "Test3"));
        UserConnection connection1 = builder.getUserConnection1();

        controller.eventsController.removeDefeatedPlayerFromSession(0);

        assertEquals(Arrays.asList(connection1), removedConnections);
    }

    @Test
    void defeatPlayer_activePlayerWithCards_sendToAllPlayersMessageAboutDefeat() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, PlayerGameEnd.class);

        controller.eventsController.removeDefeatedPlayerFromSession(0);

        PlayerGameEnd playerGameEnd = PlayerGameEnd.newBuilder()
                .setPlayer(0)
                .setResult(GameResult.Defeated)
                .build();
        assertEquals(Arrays.asList(playerGameEnd), answers1);
        assertEquals(Arrays.asList(playerGameEnd), answers2);
    }

    private GameSessionControllerBuilderForTests getGameSessionInMainPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(phasesStack);
        GameStateBuilder gameStateBuilder = builder.getGameStateBuilder();
        GameState gameState = gameStateBuilder.build();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        gameState.setCardsForUser(1, Arrays.asList(getCard(3), getCard(4)));
        gameState.setHeroForUser(0, new Player(1, 0, 20, new int[]{1}, new int[]{1}, new int[]{1}));
        return builder;
    }

    private Card getCard(int id) {
        return new Card(id, id, new CardStatistics(), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", new CardScript());
    }

    private void setReadersForSendingMessage(GameSessionControllerBuilderForTests builder, List<Object> answers1, List<Object> answers2, Class<?> messageType) {
        UserConnection connection1 = builder.getUserConnection1();
        UserConnection connection2 = builder.getUserConnection2();
        if (messageType == null) {
            doAnswer(invocationOnMock -> answers1.add(invocationOnMock.getArgument(0))).when(connection1).sendMessage(any());
            doAnswer(invocationOnMock -> answers2.add(invocationOnMock.getArgument(0))).when(connection2).sendMessage(any());
        } else {
            doAnswer(invocationOnMock -> answers1.add(invocationOnMock.getArgument(0))).when(connection1).sendMessage(any(messageType));
            doAnswer(invocationOnMock -> answers2.add(invocationOnMock.getArgument(0))).when(connection2).sendMessage(any(messageType));
        }
    }

    @Test
    void defeatPlayer_playerLoseAllLife_sendToAllPlayersMessageAboutDefeat() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();
        controller.gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2", "Test3"));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, PlayerGameEnd.class);

        controller.getAllGameState().getPlayers().get(0).changeLife(-20);

        PlayerGameEnd playerGameEnd = PlayerGameEnd.newBuilder()
                .setPlayer(0)
                .setResult(GameResult.Defeated)
                .build();
        assertEquals(Arrays.asList(playerGameEnd), answers1);
        assertEquals(Arrays.asList(playerGameEnd), answers2);
    }

    @Test
    void playerLoseLife_playerLoseAllLife_sendToAllPlayersMessageAboutDefeatAfterMessageAboutLifeLose() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();
        controller.gameState.setListOfActiveUsers(Arrays.asList("Test1", "Test2", "Test3"));
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, null);

        controller.getAllGameState().getPlayers().get(0).changeLife(-20);

        PlayerDefenceChanged playerDefenceChanged = PlayerDefenceChanged.newBuilder()
                .setPlayer(0)
                .setNewDefenceValue(0)
                .build();
        PlayerGameEnd playerGameEnd = PlayerGameEnd.newBuilder()
                .setPlayer(0)
                .setResult(GameResult.Defeated)
                .build();
        assertEquals(Arrays.asList(playerDefenceChanged, playerGameEnd), answers1);
        assertEquals(Arrays.asList(playerDefenceChanged, playerGameEnd), answers2);
    }

    @Test
    void defeatPlayer_playerLoseAllLifeAndRemainOnePlayer_sendToRemainPlayerWinMessage() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();
        List<Object> answers1 = new ArrayList<>();
        List<Object> answers2 = new ArrayList<>();
        setReadersForSendingMessage(builder, answers1, answers2, PlayerGameEnd.class);

        controller.getAllGameState().getPlayers().get(0).changeLife(-20);

        PlayerGameEnd playerGameEndLose = PlayerGameEnd.newBuilder()
                .setPlayer(0)
                .setResult(GameResult.Defeated)
                .build();
        PlayerGameEnd playerGameEndWon = PlayerGameEnd.newBuilder()
                .setPlayer(1)
                .setResult(GameResult.Won)
                .build();
        assertEquals(Arrays.asList(playerGameEndLose, playerGameEndWon), answers2);
    }

    @Test
    void defeatPlayer_playerLoseAllLifeAndRemainOnePlayer_removeSession() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();
        GameSessionManager mockedSessionManager = builder.getSessionManager();
        List<Object> removedConnections = new ArrayList<>();
        doAnswer(invocationOnMock -> removedConnections.add(invocationOnMock.getArgument(0))).when(mockedSessionManager).removeConnectionOnSessionDemand(any());
        UserConnection connection1 = builder.getUserConnection1();
        UserConnection connection2 = builder.getUserConnection2();

        controller.getAllGameState().getPlayers().get(0).changeLife(-20);

        assertEquals(Arrays.asList(connection1, connection2), removedConnections);
    }

    @Test
    void defeatPlayer_playerLoseAllLifeAndRemainOnePlayer_invokeGameEndObservers() {
        GameSessionControllerBuilderForTests builder = getGameSessionInMainPhase();
        GameSessionController controller = builder.build();
        ObservableNotifier<UserSessionEndInfo> observable = builder.getEndInfoObservable();
        List<Object> removedConnections = new ArrayList<>();
        observable.addObserver(removedConnections::add);

        controller.getAllGameState().getPlayers().get(0).changeLife(-20);

        UserSessionEndInfo winInfo = new UserSessionEndInfo(UserSessionEndInfo.Result.WIN, "Won", "Test2", 0);
        UserSessionEndInfo loseInfo = new UserSessionEndInfo(UserSessionEndInfo.Result.LOST, "Defeated", "Test1", 0);
        assertEquals(Arrays.asList(loseInfo, winInfo), removedConnections);
    }

    @Test
    void invokeCardRevealTrigger_sendMessageToAllPlayers() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(phasesStack);
        Triggers triggers = builder.getGameStateBuilder().build().getTriggers();
        GameSessionController controller = builder.build();

        CardRevealedEvent event = new CardRevealedEvent(getCard(1), new CardPosition(0, CardPosition.Position.Battlefield, 1));
        triggers.invokeTrigger(event);

        CardRevealed expected = ProtoMessagesHelper.getCardRevealedMessageFromEvent(event);

        verify(builder.getUserConnection1()).sendMessage(expected);
        verify(builder.getUserConnection2()).sendMessage(expected);
    }

    @Test
    void cardStatisticsChange_changeCardStatisticsTrigger_sendMessageToAllPlayers() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1), GamePhase.Main, 1);
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(phasesStack);
        GameState gameState = builder.getGameStateBuilder().build();
        Triggers triggers = gameState.getTriggers();
        gameState.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2)));
        GameSessionController controller = builder.build();

        Card card = getCard(2);

        var event = new CardStatisticsChangeEvent(new StatisticsChange(card), card);
        triggers.invokeTrigger(event);

        var expected = CardStatisticsChanged.newBuilder()
                .setCardInstanceId(card.getInstanceId())
                .setOverloadPoints(card.getOverloadPoints())
                .setAttackValue(card.getAttack())
                .setDefenceValue(card.getDefence())
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .build())
                .setOwner(0)
                .setPosition(com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardPosition.Library)
                .build();

        verify(builder.getUserConnection1()).sendMessage(expected);
        verify(builder.getUserConnection2()).sendMessage(expected);
    }

    @Test
    void cardStatisticsChange_changeRealCardStatistics_sendMessageToAllPlayers() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        GameSessionController controller = builder.build();

        Card card = controller.gameState.getCard(10);
        card.changeDefence(-1);

        var expected = CardStatisticsChanged.newBuilder()
                .setCardInstanceId(card.getInstanceId())
                .setOverloadPoints(card.getOverloadPoints())
                .setAttackValue(card.getAttack())
                .setDefenceValue(card.getDefence())
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .build())
                .setPosition(com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardPosition.Library)
                .setOwner(0)
                .build();

        verify(builder.getUserConnection1()).sendMessage(expected);
        verify(builder.getUserConnection2()).sendMessage(expected);
    }

    @Test
    void cardStatisticsChange_changeRealCardDefendToZero_sendMessageToAllPlayersAndInvokeDefaultCardScriptAction() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        GameSessionController controller = builder.build();

        Card card = controller.gameState.getCard(10);
        CardPosition oldPosition = controller.gameState.getCardPosition(10);
        card.changeDefence(-2);

        var expected = CardStatisticsChanged.newBuilder()
                .setCardInstanceId(card.getInstanceId())
                .setOverloadPoints(card.getOverloadPoints())
                .setAttackValue(card.getAttack())
                .setDefenceValue(card.getDefence())
                .setCardCosts(CardCosts.newBuilder()
                        .setRaiders(card.getCurrentStatistics().getRaidersCost())
                        .setNeutral(card.getCurrentStatistics().getNeutralCost())
                        .setPsychos(card.getCurrentStatistics().getPsychoCost())
                        .setScanners(card.getCurrentStatistics().getScannersCost())
                        .setWhiteHats(card.getCurrentStatistics().getWhiteHatCost())
                        .build())
                .setPosition(com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardPosition.Library)
                .setOwner(0)
                .build();

        var moveExpected = CardMoved.newBuilder()
                .setCardInstanceId(10)
                .setNewOwner(oldPosition.getOwner())
                .setOldOwner(oldPosition.getOwner())
                .setOldPositionValue(oldPosition.getPosition().ordinal())
                .setNewPosition(com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardPosition.Graveyard)
                .build();

        verify(builder.getUserConnection1()).sendMessage(expected);
        verify(builder.getUserConnection2()).sendMessage(expected);
        verify(builder.getUserConnection1()).sendMessage(moveExpected);
        verify(builder.getUserConnection2()).sendMessage(moveExpected);
    }

    @Test
    void cardStatisticsChange_changeRealCardDefendToZero_invokeCustomCardScriptAction() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        var customCardScript = new CardScriptWithCustomDeathAction();
        builder.getOverrideCardScripts().put(10, customCardScript);
        GameSessionController controller = builder.build();

        Card card = controller.gameState.getCard(10);
        card.changeDefence(-2);

        assertTrue(customCardScript.invokedDeathAction);
    }

    @Test
    void endTriggerInvokes_noHasRegisteredTrigger_doNothing() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        reset(cardScript);//needed because gameState invokes package-private methods of this mock

        controller.gameState.getTriggers().invokeTrigger(new TriggerActivatedEvent(null));

        verify(controller.gameState).getTriggers();
        verifyNoMoreInteractions(controller.gameState, cardScript);
    }

    @Test
    void endTriggerInvokes_hasRegisteredTrigger_runCardTrigger() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);

        controller.gameState.getTriggers().invokeTrigger(new TriggerActivatedEvent(null));

        verify(cardScript).isTransactionNeeded(TriggerType.TriggerActivated);
        verify(cardScript).startTriggerActions(TriggerType.TriggerActivated, -1);
    }

    @Test
    void endTriggerInvokes_hasRegisteredTrigger_endPhase() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        boolean[] phaseEnded = new boolean[]{false};
        controller.gameState.getTriggers().registerSimpleScript((PhaseEndedScript) (event) -> phaseEnded[0] = true);//because we cannot spy inside class of controller
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);

        controller.gameState.getTriggers().invokeTrigger(new TriggerActivatedEvent(null));

        assertTrue(phaseEnded[0]);
    }

    @Test
    void endTriggerInvokes_hasRegisteredTriggerAndNeededTransaction_runTransactionAndSendTransactionMessageToPlayer() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(cardScript.isTransactionNeeded(any())).thenReturn(true);
        when(cardScript.getTriggerTransactionPlayer(any())).thenReturn(1);
        when(cardScript.runTransactionAction(any(), any())).thenReturn(TargetSelectingTransaction.newBuilder().setTransactionId(4).build());
        when(builder.getUserConnection1().getUserId()).thenReturn(1);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);//it's also invoking trigger

        verify(cardScript).runTransactionAction(any(), any());
        var triggerActivatedMessage = TriggerActivated.newBuilder().setStackId(-1).setCardInstanceId(10).setTriggerType(com.MMOCardGame.GameServer.Servers.ProtoBuffers.TriggerType.TriggerActivated_).build();
        verify(builder.getUserConnection1()).sendMessage(triggerActivatedMessage);
        verify(builder.getUserConnection2()).sendMessage(triggerActivatedMessage);
        verify(builder.getUserConnection1()).sendMessage(TargetSelectingTransaction.newBuilder().setTransactionId(4).build());
    }

    @Test
    void endTriggerInvokes_hasRegisteredTrigger_notifyPlayersAboutTrigger() {
        GameSessionControllerBuilderForTests builder = new GameSessionControllerBuilderForTests();
        CardScript cardScript = mock(CardScript.class);
        when(builder.getUserConnection1().getUserId()).thenReturn(1);
        builder.getOverrideCardScripts().put(10, cardScript);
        GameSessionController controller = builder.build();
        controller.gameState = spy(controller.gameState);
        controller.gameState.getPhasesStack().startTriggerPhase(TriggerType.TriggerActivated, 10);//it's also invoking trigger

        var triggerActivatedMessage = TriggerActivated.newBuilder().setStackId(-1).setCardInstanceId(10).setTriggerType(com.MMOCardGame.GameServer.Servers.ProtoBuffers.TriggerType.TriggerActivated_).build();
        verify(builder.getUserConnection1()).sendMessage(triggerActivatedMessage);
        verify(builder.getUserConnection2()).sendMessage(triggerActivatedMessage);
    }

    @Test
    void startAttacksExecutor_endDefeatPhase_runPrepareAndExecute(){
        var builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1), Arrays.asList(new Pair<>(GamePhase.AttackExecution, 1),new Pair<>(GamePhase.Defence, 2))));
        var gameState = builder.getGameStateBuilder().build();
        var attacksExecutor=mock(AttacksExecutor.class);
        EventsController eventsController=new EventsController(null, gameState,new HashSet<>(),null,attacksExecutor);
        eventsController.setObserversOnGameSessionObjects();

        gameState.endPhase(2);

        verify(attacksExecutor).prepareAttacks(1);
        verify(attacksExecutor).executeAttacksWhileInPhase();
        verify(attacksExecutor, times(0)).cleanGameState();
    }

    @Test
    void startAttacksExecutor_endTriggerPhase_runPrepareAndExecute(){
        var builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1), Arrays.asList(new Pair<>(GamePhase.AttackExecution, 1),new Pair<>(GamePhase.TriggerPhase, 2))));
        var gameState = builder.getGameStateBuilder().build();
        var attacksExecutor=mock(AttacksExecutor.class);
        EventsController eventsController=new EventsController(null, gameState,new HashSet<>(),null,attacksExecutor);
        eventsController.setObserversOnGameSessionObjects();

        gameState.endPhase(2);

        verify(attacksExecutor).prepareAttacks(1);
        verify(attacksExecutor).executeAttacksWhileInPhase();
        verify(attacksExecutor, times(0)).cleanGameState();
    }

    @Test
    void startAttacksExecutor_changeAttackExecutionPhaseToEndRoundPhase_cleanGameState(){
        var builder = new GameSessionControllerBuilderForTests();
        builder.setPhasesStack(new PhasesStack(Arrays.asList(0,1), Arrays.asList(new Pair<>(GamePhase.EndRound, 2), new Pair<>(GamePhase.AttackExecution, 1))));
        var gameState = builder.getGameStateBuilder().build();
        var attacksExecutor=mock(AttacksExecutor.class);
        EventsController eventsController=new EventsController(null, gameState,new HashSet<>(),null,attacksExecutor);
        eventsController.setObserversOnGameSessionObjects();

        gameState.getPhasesStack().endTechnicalPhase();

        verify(attacksExecutor, times(0)).prepareAttacks(1);
        verify(attacksExecutor, times(0)).executeAttacksWhileInPhase();
        verify(attacksExecutor).cleanGameState();
    }

    static class TriggerCardScript extends CardScript implements CardMovedScript {
        boolean invoked = false;

        @Override
        public void cardMoved(CardMovedEvent event) {
            invoked = true;
        }
    }

    class CardScriptWithCustomDeathAction extends CardScript {
        boolean invokedDeathAction = false;

        @Override
        public CallbackTask<GameState> getDeathAction() {
            return gameState -> invokedDeathAction = true;
        }
    }


}