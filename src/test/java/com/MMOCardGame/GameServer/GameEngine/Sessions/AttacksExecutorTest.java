package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.AttackExecution;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class AttacksExecutorTest {
    @Test
    void cleanGameState_mockedGameState_invokeCleanOnAllRememberedAttackers() {
        var gameState = mock(GameState.class);
        var mockedHashMap = mock(HashMap.class);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(mockedHashMap);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, new HashSet<>());

        attacksExecutor.cleanGameState();

        verify(mockedHashMap, times(4)).clear();
        verify(gameState).getAttackersForPlayer(0);
        verify(gameState).getAttackersForPlayer(1);
        verify(gameState).getAttackersForPlayer(2);
        verify(gameState).getAttackersForPlayer(3);
    }

    @Test
    void prepareAttacks_mockedGameState_getAttackersForAllPlayers() {
        var gameState = mock(GameState.class);
        var mockedHashMap = mock(HashMap.class);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(mockedHashMap);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, new HashSet<>());

        attacksExecutor.prepareAttacks(0);

        verify(gameState).getAttackersForPlayer(0);
        verify(gameState).getAttackersForPlayer(1);
        verify(gameState).getAttackersForPlayer(2);
        verify(gameState).getAttackersForPlayer(3);
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithoutAttackers_doNothingElseOnGameState() {
        var gameState = mock(GameState.class);
        var mockedHashMap = mock(HashMap.class);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(mockedHashMap);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, new HashSet<>());

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(gameState).getAttackersForPlayer(0);
        verify(gameState).getAttackersForPlayer(1);
        verify(gameState).getAttackersForPlayer(2);
        verify(gameState).getAttackersForPlayer(3);
        verify(gameState).getListOfActiveUsersIds();
        verifyNoMoreInteractions(gameState);
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackers_doAttacksForAllAttackers() {
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.AttackExecution);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, new HashSet<>());

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(gameState.getCard(1).getCardScript()).getAttackMultiplier(3, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Card);
        verify(gameState.getCard(1).getCardScript()).getAttackMultiplier(4, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Card);
        verify(gameState.getCard(1).getCardScript()).isAlwaysAttackingTarget();
        verify(gameState.getCard(2).getCardScript()).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(2).getCardScript()).isAlwaysAttackingTarget();
        verify(gameState.getCard(3).getCardScript()).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(4).getCardScript()).getAttackMultiplier(1, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Card);
        verify(gameState.getCard(4).getCardScript()).getAttackMultiplier(2, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Card);
        verify(gameState.getCard(5).getCardScript()).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(5).getCardScript(), times(0)).isAlwaysAttackingTarget();// because not blocked

        //results of attacks
        verify(gameState.getCard(1), times(2)).changeDefence(anyInt());
        verify(gameState.getCard(2)).changeDefence(anyInt());
        verify(gameState.getCard(3)).changeDefence(anyInt());
        verify(gameState.getCard(4), times(2)).changeDefence(anyInt());
        verify(gameState.getCard(5), times(0)).changeDefence(anyInt());//because attacking player
        verify(gameState.getPlayer(1)).changeLife(anyInt());
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackersIncorrectPhase_doNotAttacksForAllAttackers() {
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.TriggerPhase);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, new HashSet<>());

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(gameState.getCard(1).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(1).getCardScript()).isAlwaysAttackingTarget();
        verify(gameState.getCard(2).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(2).getCardScript()).isAlwaysAttackingTarget();
        verify(gameState.getCard(3).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(4).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(5).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(5).getCardScript(), times(0)).isAlwaysAttackingTarget();//because not blocked

        //results of attacks
        verify(gameState.getCard(1), times(0)).changeDefence(anyInt());
        verify(gameState.getCard(2), times(0)).changeDefence(anyInt());
        verify(gameState.getCard(3), times(0)).changeDefence(anyInt());
        verify(gameState.getCard(4), times(0)).changeDefence(anyInt());
        verify(gameState.getCard(5), times(0)).changeDefence(anyInt());//because attacking player
        verify(gameState.getPlayer(1), times(0)).changeLife(anyInt());
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackersIncorrectPhaseAfterFirstAttack_attackForOnlyFirstCard() {
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        var mockerCard = gameState.getCard(1);
        doAnswer(o -> {
            when(gameState.getCurrentPhase()).thenReturn(GamePhase.TriggerPhase);
            return 0;
        }).when(mockerCard).changeDefence(anyInt());

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.AttackExecution);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, new HashSet<>());

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(gameState.getCard(1).getCardScript()).getAttackMultiplier(3, com.MMOCardGame.GameServer.GameEngine.Enums.TargetType.Card);
        verify(gameState.getCard(1).getCardScript()).isAlwaysAttackingTarget();
        verify(gameState.getCard(2).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(2).getCardScript()).isAlwaysAttackingTarget();
        verify(gameState.getCard(3).getCardScript()).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(4).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(5).getCardScript(), times(0)).getAttackMultiplier(anyInt(), any());
        verify(gameState.getCard(5).getCardScript(), times(0)).isAlwaysAttackingTarget();//because not blocked

        //results of attacks
        verify(gameState.getCard(1)).changeDefence(anyInt());
        verify(gameState.getCard(2), times(0)).changeDefence(anyInt());
        verify(gameState.getCard(3)).changeDefence(anyInt());
        verify(gameState.getCard(4), times(0)).changeDefence(anyInt());
        verify(gameState.getCard(5), times(0)).changeDefence(anyInt());//because attacking player
        verify(gameState.getPlayer(1), times(0)).changeLife(anyInt());
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackers_sendMessagesAboutAttacks() {
        var connectionsList = Arrays.asList(mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class));
        var connections = new HashSet<>(connectionsList);
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.AttackExecution);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, connections);

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(connectionsList.get(0), times(4)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(1), times(4)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(2), times(4)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(3), times(4)).sendMessage(any(AttackExecution.class));

        verify(connectionsList.get(0)).sendMessage(AttackExecution.newBuilder().setAttackerId(1).setDefenderId(3).setDefenderType(TargetType.Card).build());
        verify(connectionsList.get(0)).sendMessage(AttackExecution.newBuilder().setAttackerId(1).setDefenderId(4).setDefenderType(TargetType.Card).build());
        verify(connectionsList.get(0)).sendMessage(AttackExecution.newBuilder().setAttackerId(2).setDefenderId(4).setDefenderType(TargetType.Card).build());
        verify(connectionsList.get(0)).sendMessage(AttackExecution.newBuilder().setAttackerId(5).setDefenderId(1).setDefenderType(TargetType.Player).build());
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackersIncorrectPhase_doNotSendMessages() {
        var connectionsList = Arrays.asList(mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class));
        var connections = new HashSet<>(connectionsList);
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.TriggerPhase);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, connections);

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(connectionsList.get(0), times(0)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(1), times(0)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(2), times(0)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(3), times(0)).sendMessage(any(AttackExecution.class));
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackersIncorrectPhaseAfterFirstAttack_sendMessageForOnlyOneAttack() {
        var connectionsList = Arrays.asList(mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class));
        var connections = new HashSet<>(connectionsList);
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        var mockerCard = gameState.getCard(1);
        doAnswer(o -> {
            when(gameState.getCurrentPhase()).thenReturn(GamePhase.TriggerPhase);
            return 0;
        }).when(mockerCard).changeDefence(anyInt());

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.AttackExecution);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, connections);

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(connectionsList.get(0), times(1)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(1), times(1)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(2), times(1)).sendMessage(any(AttackExecution.class));
        verify(connectionsList.get(3), times(1)).sendMessage(any(AttackExecution.class));

        verify(connectionsList.get(0)).sendMessage(AttackExecution.newBuilder().setAttackerId(1).setDefenderId(3).setDefenderType(TargetType.Card).build());
    }

    @Test
    void executeAttacksWhileInPhase_mockedGameStateWithAttackers_addOverloadPointsToCards() {
        var connectionsList = Arrays.asList(mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class),
                mock(UserConnection.class));
        var connections = new HashSet<>(connectionsList);
        var gameState = mock(GameState.class, Mockito.RETURNS_DEEP_STUBS);
        when(gameState.getAttackersForPlayer(anyInt())).thenReturn(new HashMap<>());
        Map<Integer, Integer> attackers = new HashMap<>();
        attackers.put(1, 2);
        attackers.put(2, 1);
        attackers.put(5, 1);
        Map<Integer, Integer> defenders = new HashMap<>();
        defenders.put(3, 2);
        defenders.put(4, 1);
        when(gameState.getAttackersForPlayer(0)).thenReturn(attackers);
        when(gameState.getAttackersForPlayer(1)).thenReturn(defenders);
        when(gameState.getListOfActiveUsersIds()).thenReturn(Arrays.asList(0, 1, 2, 3));
        List<TargetSelectingTransaction> transactions1 = getTargetSelectingTransactionsWithTwoTargets(1, TargetType.Player, 1, TargetType.Player);
        List<TargetSelectingTransaction> transactions2 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        List<TargetSelectingTransaction> transactions3 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Card);
        List<TargetSelectingTransaction> transactions4 = getTargetSelectingTransactionsWithTwoTargets(2, TargetType.Card, 1, TargetType.Card);
        List<TargetSelectingTransaction> transactions5 = getTargetSelectingTransactionsWithOneTarget(1, TargetType.Player);
        when(gameState.getCard(1).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions1, 2)));
        when(gameState.getCard(2).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions2)));
        when(gameState.getCard(3).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(2, transactions3)));
        when(gameState.getCard(4).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions4, 3)));
        when(gameState.getCard(5).getCardScript()).thenReturn(spy(new CardScriptReturningTransaction(1, transactions5)));

        when(gameState.getCurrentPhase()).thenReturn(GamePhase.AttackExecution);

        AttacksExecutor attacksExecutor = new AttacksExecutor(gameState, connections);

        attacksExecutor.prepareAttacks(0);
        attacksExecutor.executeAttacksWhileInPhase();

        verify(gameState.getCard(1)).changeOverloadPoints(2);
        verify(gameState.getCard(2)).changeOverloadPoints(1);
        verify(gameState.getCard(3)).changeOverloadPoints(1);
        verify(gameState.getCard(4)).changeOverloadPoints(3);
        verify(gameState.getCard(5)).changeOverloadPoints(1);
    }

    private List<TargetSelectingTransaction> getTargetSelectingTransactionsWithOneTarget(int id, TargetType type) {
        return Arrays.asList(TargetSelectingTransaction.newBuilder().addOptions(TargetSelectingOption.newBuilder()
                .setId(id)
                .setTargetType(type)
                .build()).build());
    }

    private List<TargetSelectingTransaction> getTargetSelectingTransactionsWithTwoTargets(int id, TargetType type, int id2, TargetType type2) {
        return Arrays.asList(TargetSelectingTransaction.newBuilder()
                .addOptions(TargetSelectingOption.newBuilder()
                        .setId(id)
                        .setTargetType(type)
                        .build())
                .addOptions(TargetSelectingOption.newBuilder()
                        .setId(id2)
                        .setTargetType(type2)
                        .build())
                .build());
    }

    static class CardScriptReturningTransaction extends CardScript {
        int transactionId;
        Collection<TargetSelectingTransaction> transactions;

        public CardScriptReturningTransaction(int transactionId, Collection<TargetSelectingTransaction> transactions) {
            this.transactionId = transactionId;
            this.transactions = transactions;
        }

        public CardScriptReturningTransaction(int transactionId, Collection<TargetSelectingTransaction> transactions, int overloadCost) {
            this(transactionId,transactions);
            setAttackOverloadCost(overloadCost);
            setDefendOverloadCost(overloadCost);
        }

        @Override
        public Collection<TargetSelectingTransaction> getSelectedOptionsDuringTransaction(int transactionId) {
            if (transactionId != this.transactionId)
                throw new IllegalArgumentException();
            return transactions;
        }
    }
}
