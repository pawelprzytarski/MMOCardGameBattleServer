package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.PhasesStack;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.factories.GameRefereeBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.GameStateBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.PlayerFactory;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.ObservableNotifier;
import lombok.Data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Data
public class GameSessionControllerBuilderForTests {
    AtomicInteger instanceIdsSequence = new AtomicInteger(10);
    private String user1name = "Test1";
    private String user2name = "Test2";
    private UserConnection userConnection1;
    private UserConnection userConnection2;
    private ObservableNotifier<UserSessionEndInfo> endInfoObservable = new ObservableNotifier<>();
    private GameSessionSettings gameSessionSettings;
    private GameSessionManager sessionManager = mock(GameSessionManager.class);
    private GameStateBuilder gameStateBuilder;
    private GameRefereeBuilder gameRefereeBuilder = new GameRefereeBuilder();
    private Map<Integer, CardScript> overrideCardScripts = new HashMap<>();

    public GameSessionControllerBuilderForTests() {
        userConnection1 = getMockedUserConnection(user1name, 0);
        userConnection2 = getMockedUserConnection(user2name, 1);
        gameSessionSettings = getDefaultGameSessionSettings();
        gameStateBuilder = getDefaultGameStateBuilder();
    }

    public static UserConnection getMockedUserConnection(String user, int i) {
        UserConnection userConnection1 = mock(UserConnection.class);
        when(userConnection1.getUserName()).thenReturn(user);
        when(userConnection1.getUserId()).thenReturn(i);
        return userConnection1;
    }

    public GameSessionController build() {
        GameSessionController controller = new GameSessionController(gameSessionSettings, endInfoObservable, sessionManager, gameStateBuilder, gameRefereeBuilder);
        if (userConnection1 != null)
            controller.registerConnection(userConnection1);
        if (userConnection2 != null)
            controller.registerConnection(userConnection2);
        return controller;
    }

    private GameSessionSettings getDefaultGameSessionSettings() {
        GameSessionSettings gameSessionSettings = new GameSessionSettings();
        gameSessionSettings.setListOfUsers(Arrays.asList(user1name, user2name));
        gameSessionSettings.setCardsForUser(user1name, Arrays.asList(1, 2));
        gameSessionSettings.setCardsForUser(user2name, Arrays.asList(1, 2));
        gameSessionSettings.setSelectedHerosForUser(user1name, 1);
        gameSessionSettings.setSelectedHerosForUser(user2name, 1);
        return gameSessionSettings;
    }

    private GameStateBuilder getDefaultGameStateBuilder() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        CardFactory cardFactory = mock(CardFactory.class);
        when(cardFactory.createCard(anyInt())).thenAnswer(invocationOnMock -> getCard(invocationOnMock.getArgument(0)));
        when(cardFactory.createListOfCards(any())).thenAnswer(obj -> Arrays.asList(getCard(1), getCard(2)));
        gameStateBuilder.setCardFactory(cardFactory);
        gameStateBuilder.setPlayerFactory(mock(PlayerFactory.class));
        PlayerFactory playerFactory = mock(PlayerFactory.class);
        when(playerFactory.createHero(anyInt(), anyInt())).thenReturn(new Player(0, 0, 20, new int[]{1}, new int[]{1}, new int[]{1}));
        gameStateBuilder.setPlayerFactory(playerFactory);
        gameStateBuilder.setRandomGenerator(new Random());
        return gameStateBuilder;
    }

    private Card getCard(int id) {
        int instanceId = instanceIdsSequence.getAndIncrement();
        CardScript cardScript = new CardScript();
        if (overrideCardScripts.containsKey(instanceId))
            cardScript = overrideCardScripts.get(instanceId);
        return new Card(id, instanceId, new CardStatistics(2, 2, 0, 1, 1, 1, 1, 2), null, id % 2 == 0 ? CardType.Link : CardType.Unit, CardSubtype.Basic, "", "", cardScript);
    }

    public UserConnection getMockedUserConnection(String user) {
        UserConnection userConnection1 = mock(UserConnection.class);
        when(userConnection1.getUserName()).thenReturn(user);
        return userConnection1;
    }

    public void setPhasesStack(PhasesStack phasesStack) {
        GameStateBuilder gameStateBuilder = getDefaultGameStateFromPhasesStack(phasesStack);
        setGameStateBuilder(gameStateBuilder);
    }

    public GameStateBuilder getDefaultGameStateFromPhasesStack(PhasesStack phasesStack) {
        GameState gameState = new GameState(phasesStack);
        gameState.setListOfActiveUsers(getGameSessionSettings().getListOfUsers());
        GameStateBuilder gameStateBuilder = mock(GameStateBuilder.class);
        when(gameStateBuilder.build()).thenReturn(gameState);
        when(gameStateBuilder.setGameSessionSettings(any())).thenReturn(gameStateBuilder);
        return gameStateBuilder;
    }

    public GameSession buildMocked() {
        GameSession gameSession = mock(GameSession.class);
        when(gameSession.getAllGameState()).thenReturn(gameStateBuilder.build());
        return gameSession;
    }
}
