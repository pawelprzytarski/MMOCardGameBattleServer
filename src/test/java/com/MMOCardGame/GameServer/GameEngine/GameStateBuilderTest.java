package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.Player;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.GameEngine.factories.GameStateBuilder;
import com.MMOCardGame.GameServer.GameEngine.factories.PlayerFactory;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GameStateBuilderTest {
    private int instanceId = 0;

    private com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings getGameSessionSettings() {
        com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings gameSessionSettings = new com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings();
        gameSessionSettings.setListOfUsers(Arrays.asList("Test1", "Test2"));
        gameSessionSettings.setSelectedHerosForUser("Test1", 1);
        gameSessionSettings.setSelectedHerosForUser("Test2", 2);
        gameSessionSettings.setSessionId(123);
        gameSessionSettings.setTypeOfMatch(GameSessionSettings.MatchType.Single);
        gameSessionSettings.setCardsForUser("Test1", Arrays.asList(1, 2, 3, 4));
        gameSessionSettings.setCardsForUser("Test2", Arrays.asList(5, 6, 7, 8));
        return gameSessionSettings;
    }

    private CardFactory getCardFactory() {
        CardFactory cardFactory = mock(CardFactory.class);
        when(cardFactory.createCard(anyInt())).then(invocationOnMock -> getCard(invocationOnMock.getArgument(0)));
        when(cardFactory.createListOfCards(anyList())).then(invocationOnMock -> ((List<Integer>) invocationOnMock.getArgument(0)).stream().map(this::getCard).collect(Collectors.toList()));
        return cardFactory;
    }

    private Card getCard(int id) {
        return new Card(id, instanceId++, new CardStatistics(), new ArrayList<>(), CardType.Link, CardSubtype.Basic, "", "", new CardScript());
    }

    private PlayerFactory getPlayerFactory() {
        PlayerFactory heroFactory = mock(PlayerFactory.class);
        when(heroFactory.createHero(anyInt(), anyInt())).then(invocationOnMock -> new Player(invocationOnMock.getArgument(0), invocationOnMock.getArgument(1),
                20, new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}));
        return heroFactory;
    }

    private Random getRandomGenerator() {
        Random random = mock(Random.class);
        when(random.nextInt()).thenReturn(1);
        when(random.nextInt(anyInt())).thenReturn(1);
        return random;
    }

    @Test
    void build_withoutDependencies_throwsNullPointerException() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();

        assertThrows(NullPointerException.class, gameStateBuilder::build);
    }

    @Test
    void build_withoutCardFactory_throwsNullPointerException() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        gameStateBuilder.setGameSessionSettings(getGameSessionSettings());
        gameStateBuilder.setPlayerFactory(getPlayerFactory());
        gameStateBuilder.setRandomGenerator(getRandomGenerator());

        assertThrows(NullPointerException.class, gameStateBuilder::build);
    }

    @Test
    void build_withoutGameSessionSettings_throwsNullPointerException() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        gameStateBuilder.setCardFactory(mock(CardFactory.class));
        gameStateBuilder.setPlayerFactory(getPlayerFactory());
        gameStateBuilder.setRandomGenerator(getRandomGenerator());

        assertThrows(NullPointerException.class, gameStateBuilder::build);
    }

    @Test
    void build_withoutPlayerFactory_throwsNullPointerException() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        gameStateBuilder.setGameSessionSettings(getGameSessionSettings());
        gameStateBuilder.setCardFactory(mock(CardFactory.class));
        gameStateBuilder.setRandomGenerator(getRandomGenerator());

        assertThrows(NullPointerException.class, gameStateBuilder::build);
    }

    @Test
    void build_withoutRandomGenerator_throwsNullPointerException() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        gameStateBuilder.setCardFactory(mock(CardFactory.class));
        gameStateBuilder.setGameSessionSettings(getGameSessionSettings());
        gameStateBuilder.setPlayerFactory(getPlayerFactory());

        assertThrows(NullPointerException.class, gameStateBuilder::build);
    }

    @Test
    void build_withAllDependencies_returnsGameStateObject() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        gameStateBuilder.setCardFactory(mock(CardFactory.class));
        gameStateBuilder.setGameSessionSettings(getGameSessionSettings());
        gameStateBuilder.setPlayerFactory(getPlayerFactory());
        gameStateBuilder.setRandomGenerator(getRandomGenerator());

        Object result = gameStateBuilder.build();

        assertNotNull(result);
        assertEquals(GameState.class, result.getClass());
    }

    @Test
    void build_withAllDependencies_returnsGameStateWithAllData() {
        GameStateBuilder gameStateBuilder = new GameStateBuilder();
        gameStateBuilder.setCardFactory(getCardFactory());
        gameStateBuilder.setGameSessionSettings(getGameSessionSettings());
        gameStateBuilder.setPlayerFactory(getPlayerFactory());
        gameStateBuilder.setRandomGenerator(getRandomGenerator());

        instanceId = 1;

        GameState result = gameStateBuilder.build();

        GameState expected = getExpectedGameState();
        assertEquals(expected, result);
    }

    private GameState getExpectedGameState() {
        instanceId = 1;
        GameState expected = new GameState();
        expected.setListOfActiveUsers(Arrays.asList("Test1", "Test2"));
        expected.setCardsForUser(0, Arrays.asList(getCard(1), getCard(2), getCard(3), getCard(4)));
        expected.setCardsForUser(1, Arrays.asList(getCard(5), getCard(6), getCard(7), getCard(8)));
        Map<Integer, Player> heroMap = new HashMap<>();
        heroMap.put(0, getExpectedHero(1, 0));
        heroMap.put(1, getExpectedHero(2, 1));
        expected.setPlayers(heroMap);
        expected.setPlayersOrder(new int[]{0, 1});
        return expected;
    }

    private Player getExpectedHero(int heroId, int userId) {
        return new Player(heroId, userId, 20, new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0});
    }
}