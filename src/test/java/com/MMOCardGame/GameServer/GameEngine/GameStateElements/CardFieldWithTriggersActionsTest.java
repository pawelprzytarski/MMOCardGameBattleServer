package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardRevealedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CardFieldWithTriggersActionsTest {
    private Collection<Card> getInitCards() {
        return Arrays.asList(getCard(1), getCard(2), getCard(3));
    }

    private Card getCard(int instanceId) {
        return new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
    }

    @Test
    void moveCardToBattlefield_moveFromLibrary_registerCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        verify(triggers, times(1)).registerCardScript(any());
    }

    @Test
    void moveCardToBattlefield_moveFromHand_registerCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToHand(1);

        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        verify(triggers, times(1)).registerCardScript(any());
    }

    @Test
    void moveCardToBattlefield_moveFromGraveyard_registerCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToGraveyard(1);

        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        verify(triggers, times(1)).registerCardScript(any());
    }

    @Test
    void moveCardToHand_moveFromLibrary_noRegisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToHand(1);

        verify(triggers, times(0)).registerCardScript(any());
    }

    @Test
    void moveCardToGraveyard_moveFromLibrary_noRegisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToGraveyard(1);

        verify(triggers, times(0)).registerCardScript(any());
    }

    @Test
    void moveCardToGraveyard_moveFromBattlefield_unregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        cardFieldWithTriggersActions.moveCardToGraveyard(1);

        verify(triggers, times(1)).unregisterCardScript(any());
    }

    @Test
    void moveCardToHand_moveFromBattlefield_unregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        cardFieldWithTriggersActions.moveCardToHand(1);

        verify(triggers, times(1)).unregisterCardScript(any());
    }

    @Test
    void moveCardToLibrary_moveFromBattlefield_unregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        cardFieldWithTriggersActions.moveCardToLibrary(1);

        verify(triggers, times(1)).unregisterCardScript(any());
    }

    @Test
    void removeCard_removeFromBattlefield_unregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        cardFieldWithTriggersActions.removeCard(1);

        verify(triggers, times(1)).unregisterCardScript(any());
    }

    @Test
    void removeCard_removeFromLibrary_noUnregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToLibrary(1);

        cardFieldWithTriggersActions.removeCard(1);

        verify(triggers, times(0)).unregisterCardScript(any());
    }

    @Test
    void removeCard_removeFromHand_noUnregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToHand(1);

        cardFieldWithTriggersActions.removeCard(1);

        verify(triggers, times(0)).unregisterCardScript(any());
    }

    @Test
    void removeCard_removeFromGraveyard_noUnregisterCardScript() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);
        cardFieldWithTriggersActions.moveCardToGraveyard(1);

        cardFieldWithTriggersActions.removeCard(1);

        verify(triggers, times(0)).unregisterCardScript(any());
    }

    @Test
    void moveCardToBattlefield_moveFromLibrary_invokeCardRevealedEvent() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        verify(triggers, times(1)).invokeTrigger(any(CardRevealedEvent.class));
    }

    @Test
    void moveCardToGraveyard_moveFromLibrary_invokeCardRevealedEvent() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToGraveyard(1);

        verify(triggers, times(1)).invokeTrigger(any(CardRevealedEvent.class));
    }

    @Test
    void moveCardToHand_moveFromLibrary_noInvokeCardRevealedEvent() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToHand(1);

        verify(triggers, times(0)).invokeTrigger(any(CardRevealedEvent.class));
    }

    @Test
    void moveCardToVoid_moveFromLibrary_invokeCardRevealedEvent() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToVoid(1);

        verify(triggers, times(1)).invokeTrigger(any(CardRevealedEvent.class));
    }

    @Test
    void moveCardToHandTwoTimes_moveFromHand_noInvokeCardRevealedEvent() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToHand(1);
        cardFieldWithTriggersActions.moveCardToLibrary(1);
        cardFieldWithTriggersActions.moveCardToHand(1);

        verify(triggers, times(0)).invokeTrigger(any(CardRevealedEvent.class));
    }

    @Test
    void moveCardToHandTwoTimes_moveFromBattlefield_invokeCardRevealedEventExactlyOnes() {
        Triggers triggers = mock(Triggers.class);
        CardFieldWithTriggersActions cardFieldWithTriggersActions = new CardFieldWithTriggersActions(getInitCards(), triggers, 0);

        cardFieldWithTriggersActions.moveCardToBattlefield(1);
        cardFieldWithTriggersActions.moveCardToLibrary(1);
        cardFieldWithTriggersActions.moveCardToBattlefield(1);

        verify(triggers, times(1)).invokeTrigger(any(CardRevealedEvent.class));
    }
}