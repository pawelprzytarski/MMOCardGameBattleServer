package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.common.Observer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ReactionStackTest {
    @Test
    void doAction_noActionOnStack_doNothing() {
        ReactionStack reactionStack = new ReactionStack();
        reactionStack.doAction();
    }

    @Test
    void doAction_actionOnStack_runTask() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable = mock(Runnable.class);
        Runnable canceledTask = mock(Runnable.class);
        reactionStack.pushAction(runnable, canceledTask, 0);

        reactionStack.doAction();

        verify(runnable).run();
        verifyZeroInteractions(canceledTask);
    }

    @Test
    void doAction_canceledActionOnStack_runCanceledTask() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable = mock(Runnable.class);
        Runnable canceledTask = mock(Runnable.class);
        reactionStack.pushAction(runnable, canceledTask, 0).setCanceled(true);

        reactionStack.doAction();

        verify(canceledTask).run();
        verifyZeroInteractions(runnable);
    }

    @Test
    void pushAction_simpleTask_returnsElementWithTaskAnd0Id() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable = mock(Runnable.class);
        Runnable canceledTask = mock(Runnable.class);

        ReactionStackElement element = reactionStack.pushAction(runnable, canceledTask, 0);

        ReactionStackElement expected = new ReactionStackElement(runnable, canceledTask, 0, 0);
        assertEquals(expected, element);
    }

    @Test
    void pushAction_twoSimpleTasks_returnsElementWithTaskAndNextId() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable = mock(Runnable.class);
        Runnable canceledTask = mock(Runnable.class);

        ReactionStackElement element1 = reactionStack.pushAction(runnable, canceledTask, 0);
        ReactionStackElement element2 = reactionStack.pushAction(runnable, canceledTask, 2);

        ReactionStackElement expected1 = new ReactionStackElement(runnable, canceledTask, 0, 0);
        assertEquals(expected1, element1);
        ReactionStackElement expected2 = new ReactionStackElement(runnable, canceledTask, 1, 2);
        assertEquals(expected2, element2);
    }

    @Test
    void doAction_twoSimpleTasksDoActionTwice_runTwoTasks() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable runnable2 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Runnable canceledTask2 = mock(Runnable.class);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.pushAction(runnable2, canceledTask2, 2);

        reactionStack.doAction();
        reactionStack.doAction();

        verify(runnable1).run();
        verify(runnable2).run();
        verifyZeroInteractions(canceledTask1);
        verifyZeroInteractions(canceledTask2);
    }

    @Test
    void doAction_twoSimpleCanceledTasksDoActionTwice_runTwoCancelTasks() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable runnable2 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Runnable canceledTask2 = mock(Runnable.class);
        reactionStack.pushAction(runnable1, canceledTask1, 0).setCanceled(true);
        reactionStack.pushAction(runnable2, canceledTask2, 2).setCanceled(true);

        reactionStack.doAction();
        reactionStack.doAction();

        verify(canceledTask1).run();
        verify(canceledTask2).run();
        verifyZeroInteractions(runnable1);
        verifyZeroInteractions(runnable2);
    }

    @Test
    void doAction_twoSimpleTasksDoActionOnce_runLastTask() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable runnable2 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Runnable canceledTask2 = mock(Runnable.class);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.pushAction(runnable2, canceledTask2, 2);

        reactionStack.doAction();

        verifyZeroInteractions(runnable1);
        verify(runnable2).run();
    }

    @Test
    void doAction_twoSimpleCanceledTasksDoActionOnce_runLastTask() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable runnable2 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Runnable canceledTask2 = mock(Runnable.class);
        reactionStack.pushAction(runnable1, canceledTask1, 0).setCanceled(true);
        reactionStack.pushAction(runnable2, canceledTask2, 2).setCanceled(true);

        reactionStack.doAction();

        verifyZeroInteractions(canceledTask1);
        verify(canceledTask2).run();
    }

    @Test
    void getElementById_twoSimpleTasks_getTwoTasks() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable runnable2 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Runnable canceledTask2 = mock(Runnable.class);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.pushAction(runnable2, canceledTask2, 2);

        ReactionStackElement element1 = reactionStack.getElementById(0);
        ReactionStackElement element2 = reactionStack.getElementById(1);

        ReactionStackElement expected1 = new ReactionStackElement(runnable1, canceledTask1, 0, 0);
        assertEquals(expected1, element1);
        ReactionStackElement expected2 = new ReactionStackElement(runnable2, canceledTask2, 1, 2);
        assertEquals(expected2, element2);
    }

    @Test
    void getElementById_twoSimpleTasksAndOneDone_getRemainingTask() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable runnable2 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Runnable canceledTask2 = mock(Runnable.class);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.pushAction(runnable2, canceledTask2, 2);

        reactionStack.doAction();
        ReactionStackElement element1 = reactionStack.getElementById(0);
        ReactionStackElement element2 = reactionStack.getElementById(1);

        ReactionStackElement expected1 = new ReactionStackElement(runnable1, canceledTask1, 0, 0);
        assertEquals(expected1, element1);
        assertNull(element2);
    }

    @Test
    void addCreateElementObserver_twoSimpleObserver_invokeObservers() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<ReactionStackElement> observer1 = mock(Observer.class);
        Observer<ReactionStackElement> observer2 = mock(Observer.class);

        reactionStack.addCreateElementObserver(observer1);
        reactionStack.addCreateElementObserver(observer2);
        reactionStack.pushAction(runnable1, canceledTask1, 0);

        verify(observer1).update(any());
        verify(observer2).update(any());
    }

    @Test
    void removeCreateElementObserver_twoSimpleObserver_invokeRemainingObserver() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<ReactionStackElement> observer1 = mock(Observer.class);
        Observer<ReactionStackElement> observer2 = mock(Observer.class);

        reactionStack.addCreateElementObserver(observer1);
        reactionStack.addCreateElementObserver(observer2);
        reactionStack.removeCreateElementObserver(observer1);
        reactionStack.pushAction(runnable1, canceledTask1, 0);

        verify(observer1, times(0)).update(any());
        verify(observer2).update(any());
    }

    @Test
    void addBeforeDoActionObserver_twoSimpleObserver_invokeObservers() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<BeforeReactionStackActionEvent> observer1 = mock(Observer.class);
        Observer<BeforeReactionStackActionEvent> observer2 = mock(Observer.class);

        reactionStack.addBeforeDoActionObserver(observer1);
        reactionStack.addBeforeDoActionObserver(observer2);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.doAction();

        verify(observer1).update(any());
        verify(observer2).update(any());
    }

    @Test
    void doAction_observerWithCancelAction_noRunAction() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<BeforeReactionStackActionEvent> observer1 = mock(Observer.class);
        doAnswer(invocationOnMock -> (((BeforeReactionStackActionEvent) invocationOnMock.getArgument(0)).canceled = true)).
                when(observer1).update(any());

        reactionStack.addBeforeDoActionObserver(observer1);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.doAction();

        verify(runnable1, times(0)).run();
    }

    @Test
    void doAction_observerWithCancelAction_remainElementOnStack() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<BeforeReactionStackActionEvent> observer1 = mock(Observer.class);
        doAnswer(invocationOnMock -> (((BeforeReactionStackActionEvent) invocationOnMock.getArgument(0)).canceled = true)).
                when(observer1).update(any());

        reactionStack.addBeforeDoActionObserver(observer1);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.doAction();

        ReactionStackElement element = reactionStack.getElementById(0);
        ReactionStackElement expected = new ReactionStackElement(runnable1, canceledTask1, 0, 0);
        assertEquals(expected, element);
    }

    @Test
    void removeBeforeDoActionObserver_twoSimpleObserver_invokeRemainingObserver() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<BeforeReactionStackActionEvent> observer1 = mock(Observer.class);
        Observer<BeforeReactionStackActionEvent> observer2 = mock(Observer.class);

        reactionStack.addBeforeDoActionObserver(observer1);
        reactionStack.addBeforeDoActionObserver(observer2);
        reactionStack.removeBeforeDoActionObserver(observer1);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.doAction();

        verify(observer1, times(0)).update(any());
        verify(observer2).update(any());
    }

    @Test
    void addAfterDoActionObserver_twoSimpleObserver_invokeObservers() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<ReactionStackElement> observer1 = mock(Observer.class);
        Observer<ReactionStackElement> observer2 = mock(Observer.class);

        reactionStack.addAfterDoActionObserver(observer1);
        reactionStack.addAfterDoActionObserver(observer2);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.doAction();

        verify(observer1).update(any());
        verify(observer2).update(any());
    }

    @Test
    void removeAfterDoActionObserver_twoSimpleObserver_invokeRemainingObserver() {
        ReactionStack reactionStack = new ReactionStack();
        Runnable runnable1 = mock(Runnable.class);
        Runnable canceledTask1 = mock(Runnable.class);
        Observer<ReactionStackElement> observer1 = mock(Observer.class);
        Observer<ReactionStackElement> observer2 = mock(Observer.class);

        reactionStack.addAfterDoActionObserver(observer1);
        reactionStack.addAfterDoActionObserver(observer2);
        reactionStack.removeAfterDoActionObserver(observer1);
        reactionStack.pushAction(runnable1, canceledTask1, 0);
        reactionStack.doAction();

        verify(observer1, times(0)).update(any());
        verify(observer2).update(any());
    }
}