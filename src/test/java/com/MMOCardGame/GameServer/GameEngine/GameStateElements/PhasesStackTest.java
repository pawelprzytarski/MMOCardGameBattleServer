package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GamePhaseChanged;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.common.Observer;
import com.MMOCardGame.GameServer.common.Pair;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PhasesStackTest {
    private static Stream<Arguments> getParamsForPlayerRoundPhasesChangesTestGetPhase() {
        return Stream.of(
                Arguments.of(GamePhase.Untap, GamePhase.Main, true),
                Arguments.of(GamePhase.Untap, GamePhase.Untap, false),
                Arguments.of(GamePhase.Main, GamePhase.Attack, true),
                Arguments.of(GamePhase.Main, GamePhase.Main, false),
                Arguments.of(GamePhase.Attack, GamePhase.Defence, true),
                Arguments.of(GamePhase.Attack, GamePhase.Attack, false),
                Arguments.of(GamePhase.Defence, GamePhase.Defence, false),
                Arguments.of(GamePhase.ReactionPhase, GamePhase.ReactionPhase, false)
        );
    }

    private static Stream<Arguments> getParamsForPlayerRoundPhasesChangesTestGetPlayer() {
        return Stream.of(
                Arguments.of(GamePhase.Untap, false, true),
                Arguments.of(GamePhase.Untap, false, false),
                Arguments.of(GamePhase.Main, false, true),
                Arguments.of(GamePhase.Main, false, false),
                Arguments.of(GamePhase.Attack, true, true),
                Arguments.of(GamePhase.Attack, false, false),
                Arguments.of(GamePhase.Defence, false, false),
                Arguments.of(GamePhase.EndRound, false, false),
                Arguments.of(GamePhase.EndRound, true, true),
                Arguments.of(GamePhase.ReactionPhase, false, false)
        );
    }

    @Test
    void getCurrentPhase_initedStack_returnsWaiting() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3));
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.Waiting, result);
    }

    @Test
    void getCurrentPhase_endedWaiting_returnsFirstHandPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3));
        IntStream.range(0, 4).forEach(phasesStack::endPhase);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.FirstHandDraw, result);
    }

    @Test
    void getCurrentPlayer_waitingPhase_returnsNonePlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3));
        int result = phasesStack.getCurrentPlayer();
        assertEquals(-1, result);
    }

    @Test
    void getCurrentPlayer_firstHandPhase_returnsNonePlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3));
        IntStream.range(0, 4).forEach(phasesStack::endPhase);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(-1, result);
    }

    @Test
    void getCurrentPhase_allPlayersEndedFirstHandPhase_returnsFirstPlayerUntapPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), getStackForFirstDraw());
        IntStream.range(0, 4).forEach(phasesStack::endPhase);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.Untap, result);
    }

    private List<Pair<GamePhase, Integer>> getStackForFirstDraw() {
        return getAllPlayerStackForState(GamePhase.FirstHandDraw);
    }

    private List<Pair<GamePhase, Integer>> getAllPlayerStackForState(GamePhase defence) {
        return Arrays.asList(new Pair<>(defence, 0),
                new Pair<>(defence, 1),
                new Pair<>(defence, 2),
                new Pair<>(defence, 3));
    }

    @Test
    void getCurrentPlayer_allPlayersEndedFirstHandPhase_returnsFirstPlayerInOrder() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), getStackForFirstDraw());
        IntStream.range(0, 4).forEach(phasesStack::endPhase);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(0, result);
    }

    @Test
    void getCurrentPhase_onePlayersEndedFirstHandPhase_returnsFirstHandPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), getStackForFirstDraw());
        phasesStack.endPhase(2);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.FirstHandDraw, result);
    }

    @Test
    void getCurrentPhase_allButOnePlayersEndedFirstHandPhase_returnsFirstHandPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), getStackForFirstDraw());
        IntStream.range(0, 3).forEach(phasesStack::endPhase);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.FirstHandDraw, result);
    }

    @ParameterizedTest(name = "getCurrentPhase_isCorrectPlayer={2}StartPhase={0}_returns={1}")
    @MethodSource("getParamsForPlayerRoundPhasesChangesTestGetPhase")
    void getCurrentPhase_parametrizedStartGamePhaseAndParametrizedPlayer_returnsParametrizedGamePhase(GamePhase startPhase, GamePhase expectedPhase, Boolean isCorrectPlayer) {
        int playerId = isCorrectPlayer ? 1 : 0;
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), startPhase, 1);
        phasesStack.endPhase(playerId);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(expectedPhase, result);
    }

    @Test
    void getCurrentPhase_DefencePhaseEndPhaseToNextPlayer_returnsDefencePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.Defence, 1), new Pair<>(GamePhase.Defence, 2)));
        phasesStack.endPhase(1);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.Defence, result);
    }

    @Test
    void getCurrentPhase_ReactionPhaseEndPhaseToNextPlayer_returnsDefencePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.ReactionPhase, 1), new Pair<>(GamePhase.ReactionPhase, 2)));
        phasesStack.endPhase(1);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.ReactionPhase, result);
    }

    @ParameterizedTest(name = "getCurrentPlayer_isCorrectPlayer={2}StartPhase={0}_returnsNextPlayer={2}")
    @MethodSource("getParamsForPlayerRoundPhasesChangesTestGetPlayer")
    void getCurrentPlayer_parametrizedStartGamePhaseAndParametrizedPlayer_returnsParametrizedGamePhase(GamePhase startPhase, Boolean isNextPlayer, Boolean isCorrectPlayer) {
        int playerId = isCorrectPlayer ? 1 : 0;
        int expectedPlayer = isNextPlayer ? 2 : 1;
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), startPhase, 1);
        phasesStack.endPhase(playerId);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(expectedPlayer, result);
    }

    @Test
    void getCurrentPlayer_DefencePhaseEndPhaseToNextPlayer_returnsNextPlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.Defence, 1), new Pair<>(GamePhase.Defence, 2)));
        phasesStack.endPhase(1);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(2, result);
    }

    @Test
    void getCurrentPlayer_ReactionPhaseEndPhaseToNextPlayer_returnsNextPlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.ReactionPhase, 1), new Pair<>(GamePhase.ReactionPhase, 2)));
        phasesStack.endPhase(1);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(2, result);
    }

    @Test
    void getCurrentPhase_lastPlayerEndsDefence_returnsEndRoundPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Attack, 0);
        phasesStack.endPhase(0);//Now it's defence
        IntStream.range(1, 4).forEach(phasesStack::endPhase);
        IntStream.range(0, 4).forEach(phasesStack::endPhase);
        phasesStack.endTechnicalPhase();
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.EndRound, result);
    }

    @Test
    void getCurrentPlayer_lastPlayerEndsDefence_returnsPreviousPlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Attack, 0);
        phasesStack.endPhase(0);//Now it's defence
        IntStream.range(1, 4).forEach(phasesStack::endPhase);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(0, result);
    }

    @Test
    void getCurrentPlayer_lastPlayerEndsEndRound_returnsNextPlayerInOrder() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Attack, 0);
        phasesStack.endPhase(0);//Now it's defence
        IntStream.range(1, 4).forEach(phasesStack::endPhase);
        IntStream.range(0, 4).forEach(phasesStack::endPhase);//reaction phases
        phasesStack.endTechnicalPhase();
        IntStream.range(0, 4).forEach(phasesStack::endPhase);//end round
        int result = phasesStack.getCurrentPlayer();
        assertEquals(1, result);
    }

    @Test
    void getCurrentPhase_lastPlayerEndsEndRound_returnsUntapPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Attack, 0);
        phasesStack.endPhase(0);//Now it's defence
        IntStream.range(1, 4).forEach(phasesStack::endPhase);
        IntStream.range(0, 4).forEach(phasesStack::endPhase);//reaction phases
        phasesStack.endTechnicalPhase();
        IntStream.range(0, 4).forEach(phasesStack::endPhase);//end round
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.Untap, result);
    }

    @Test
    void getCurrentPhase_startReactionStack_returnReactionPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhasesWithStartingPlayer(2);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.ReactionPhase, result);
    }

    @Test
    void getCurrentPlayer_startReactionStackWithoutStartingPlayer_returnsNextPlayerInOrder() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhases();
        int result = phasesStack.getCurrentPlayer();
        assertEquals(2, result);
    }

    @Test
    void getCurrentPlayer_startReactionStackWithStartingPlayer_returnsStartingPlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhasesWithStartingPlayer(3);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(3, result);
    }

    @Test
    void getCurrentPlayer_endReactionStack_returnsPlayerBeforeReactionStack() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhases();
        phasesStack.endPhase(1);
        phasesStack.endPhase(2);
        phasesStack.endPhase(3);
        phasesStack.endPhase(0);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(1, result);
    }

    @Test
    void getCurrentState_endReactionStack_returnsStateBeforeReactionStack() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhases();
        phasesStack.endPhase(2);
        phasesStack.endPhase(3);
        phasesStack.endPhase(0);
        phasesStack.endPhase(1);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.Main, result);
    }

    @Test
    void startReactionStack_startStartedStack_addPlayersToStack() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhases();
        phasesStack.endPhase(1);
        phasesStack.endPhase(2);
        phasesStack.startReactionPhases();
        phasesStack.endPhase(3);
        phasesStack.endPhase(0);
        phasesStack.endPhase(1);
        phasesStack.endPhase(2);
        GamePhase result = phasesStack.getCurrentPhase();
        assertEquals(GamePhase.Main, result);
    }

    @Test
    void startReactionStack_startStartedStack_addPlayersToStackReturnsToPreviousPlayer() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        phasesStack.startReactionPhases();
        phasesStack.endPhase(1);
        phasesStack.endPhase(2);
        phasesStack.startReactionPhases();
        phasesStack.endPhase(3);
        phasesStack.endPhase(0);
        phasesStack.endPhase(1);
        phasesStack.endPhase(2);
        int result = phasesStack.getCurrentPlayer();
        assertEquals(1, result);
    }

    @Test
    void addObserver_changePhase_invokeObserverOnPhaseChange() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        Observer<GamePhaseChanged> observer = mock(Observer.class);
        phasesStack.addObserver(observer);
        phasesStack.endPhase(1);

        verify(observer).update(any());
    }

    @Test
    void addObserver_noChangePhase_invokeObserverOnPhaseChange() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        Observer<GamePhaseChanged> observer = mock(Observer.class);
        phasesStack.addObserver(observer);
        phasesStack.endPhase(2);

        verify(observer, times(0)).update(any());
    }

    @Test
    void addObserver_changePhaseTwoObservers_invokeAllObserversOnPhaseChange() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        Observer<GamePhaseChanged> observer = mock(Observer.class);
        Observer<GamePhaseChanged> observer2 = mock(Observer.class);
        phasesStack.addObserver(observer);
        phasesStack.addObserver(observer2);
        phasesStack.endPhase(1);

        verify(observer).update(any());
        verify(observer2).update(any());
    }

    @Test
    void removeObserver_changePhaseRemoveOneObservers_invokeRemainingObserverOnPhaseChange() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        Observer<GamePhaseChanged> observer = mock(Observer.class);
        Observer<GamePhaseChanged> observer2 = mock(Observer.class);
        phasesStack.addObserver(observer);
        phasesStack.addObserver(observer2);

        phasesStack.removeObserver(observer);
        phasesStack.endPhase(1);

        verify(observer, times(0)).update(any());
        verify(observer2).update(any());
    }

    @Test
    void startTriggerPhase_startPhase_newPhaseAdded() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        int cardId = 6;
        TriggerType triggerType = TriggerType.CardAttacked;

        phasesStack.startTriggerPhase(triggerType, cardId);

        int result = phasesStack.getCurrentPlayer();
        GamePhase gamePhase = phasesStack.getCurrentPhase();
        int expectedPhaseOwner = cardId * 100 + triggerType.ordinal();
        assertEquals(expectedPhaseOwner, result);
        assertEquals(GamePhase.TriggerPhase, gamePhase);
    }

    @Test
    void startTriggerPhase_playerTryChangePhase_notChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        int cardId = 6;
        TriggerType triggerType = TriggerType.CardAttacked;
        phasesStack.startTriggerPhase(triggerType, cardId);

        phasesStack.endPhase(1);

        int result = phasesStack.getCurrentPlayer();
        GamePhase gamePhase = phasesStack.getCurrentPhase();
        int expectedPhaseOwner = cardId * 100 + triggerType.ordinal();
        assertEquals(expectedPhaseOwner, result);
        assertEquals(GamePhase.TriggerPhase, gamePhase);
    }

    @Test
    void startTriggerPhase_incorrectCardTryChangePhase_notChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        int cardId = 6;
        TriggerType triggerType = TriggerType.CardAttacked;
        phasesStack.startTriggerPhase(triggerType, cardId);

        phasesStack.endTriggerPhase(triggerType, cardId + 1);

        int result = phasesStack.getCurrentPlayer();
        GamePhase gamePhase = phasesStack.getCurrentPhase();
        int expectedPhaseOwner = cardId * 100 + triggerType.ordinal();
        assertEquals(expectedPhaseOwner, result);
        assertEquals(GamePhase.TriggerPhase, gamePhase);
    }

    @Test
    void startTriggerPhase_correctCardTryChangePhaseWithIncorrectTriggerType_notChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        int cardId = 6;
        TriggerType triggerType = TriggerType.CardAttacked;
        phasesStack.startTriggerPhase(triggerType, cardId);

        phasesStack.endTriggerPhase(TriggerType.DrawCard, cardId);

        int result = phasesStack.getCurrentPlayer();
        GamePhase gamePhase = phasesStack.getCurrentPhase();
        int expectedPhaseOwner = cardId * 100 + triggerType.ordinal();
        assertEquals(expectedPhaseOwner, result);
        assertEquals(GamePhase.TriggerPhase, gamePhase);
    }

    @Test
    void startTriggerPhase_correctCardTryChangePhaseWithCorrectTriggerType_returnToPreviousPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), GamePhase.Main, 1);
        int cardId = 6;
        TriggerType triggerType = TriggerType.CardAttacked;
        phasesStack.startTriggerPhase(triggerType, cardId);

        phasesStack.endTriggerPhase(triggerType, cardId);

        int result = phasesStack.getCurrentPlayer();
        GamePhase gamePhase = phasesStack.getCurrentPhase();
        assertEquals(1, result);
        assertEquals(GamePhase.Main, gamePhase);
    }

    @Test
    void endTechnicalPhase_isTechnicalPhase_endPhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.EndRound,1),new Pair<>(GamePhase.AttackExecution, 1)));

        phasesStack.endTechnicalPhase();
        phasesStack.endPhase(0);

        assertEquals(GamePhase.EndRound, phasesStack.getCurrentPhase());
    }

    @Test
    void endTechnicalPhase_isNotTechnicalPhase_notChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.EndRound,1),new Pair<>(GamePhase.Main, 1)));

        phasesStack.endTechnicalPhase();

        assertEquals(GamePhase.Main, phasesStack.getCurrentPhase());
    }

    @Test
    void endPhase_isTechnicalPhase_notChangePhase() {
        PhasesStack phasesStack = new PhasesStack(Arrays.asList(0, 1, 2, 3), Arrays.asList(new Pair<>(GamePhase.EndRound,1),new Pair<>(GamePhase.AttackExecution, 1)));

        phasesStack.endPhase(1);

        assertEquals(GamePhase.AttackExecution, phasesStack.getCurrentPhase());
    }
}