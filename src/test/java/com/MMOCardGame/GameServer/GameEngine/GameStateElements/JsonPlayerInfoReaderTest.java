package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.factories.JsonHeroInfoReader;
import com.MMOCardGame.GameServer.GameEngine.factories.PlayerFactory;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class JsonPlayerInfoReaderTest {
    String getOneHeroJson() {
        return "[{" +
                "\"id\":1,\n" +
                "\"name\":\"Nazwa\",\n" +
                "\"description\":\"opis\",\n" +
                "\"abilityCosts\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"startingCooldown\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"cooldown\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"abilityNames\":[\n" +
                "\"\",\"\",\"\",\"\"\n" +
                "]\n" +
                "}]\n";
    }

    String getTwoHeroJson() {
        return "[{" +
                "\"id\":1,\n" +
                "\"name\":\"Nazwa\",\n" +
                "\"description\":\"opis\",\n" +
                "\"abilityCosts\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"startingCooldown\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"cooldown\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"abilityNames\":[\n" +
                "\"\",\"\",\"\",\"\"\n" +
                "]\n" +
                "},{" +
                "\"id\":2,\n" +
                "\"name\":\"Nazwa\",\n" +
                "\"description\":\"opis\",\n" +
                "\"abilityCosts\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"startingCooldown\":[\n" +
                "1,2,3,4\n" +
                "],\n" +
                "\"cooldown\":[\n" +
                "1,2,3,5\n" +
                "],\n" +
                "\"abilityNames\":[\n" +
                "\"\",\"\",\"\",\"\"\n" +
                "]\n" +
                "}]";
    }

    @Test
    void readFromStream_emptyStream_throwsArgumentException() {
        JsonHeroInfoReader jsonHeroInfoReader = new JsonHeroInfoReader();
        ByteArrayInputStream inputStream = getMemoryInputStream("");

        assertThrows(IllegalArgumentException.class, () -> jsonHeroInfoReader.readFromStream(inputStream));
    }

    private ByteArrayInputStream getMemoryInputStream(String text) {
        return new ByteArrayInputStream(text.getBytes(Charset.forName("UTF-8")));
    }

    @Test
    void readFromStream_OneHero_returnHeroInfo() throws IOException {
        JsonHeroInfoReader jsonHeroInfoReader = new JsonHeroInfoReader();
        ByteArrayInputStream inputStream = getMemoryInputStream(getOneHeroJson());

        Map<Integer, PlayerFactory.PlayerInfo> result = jsonHeroInfoReader.readFromStream(inputStream);
        Map<Integer, PlayerFactory.PlayerInfo> expected = new HashMap<>();
        expected.put(1, new PlayerFactory.PlayerInfo(1, new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4}));
        assertEquals(expected, result);
    }

    @Test
    void readFromStream_TwoHero_returnAllHeroInfo() throws IOException {
        JsonHeroInfoReader jsonHeroInfoReader = new JsonHeroInfoReader();
        ByteArrayInputStream inputStream = getMemoryInputStream(getTwoHeroJson());

        Map<Integer, PlayerFactory.PlayerInfo> result = jsonHeroInfoReader.readFromStream(inputStream);
        Map<Integer, PlayerFactory.PlayerInfo> expected = new HashMap<>();
        expected.put(1, new PlayerFactory.PlayerInfo(1, new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4}));
        expected.put(2, new PlayerFactory.PlayerInfo(2, new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 5}, new int[]{1, 2, 3, 4}));
        assertEquals(expected, result);
    }
}