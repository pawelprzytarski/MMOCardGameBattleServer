package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition.Position;
import com.MMOCardGame.GameServer.common.Pair;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CardsFieldTest {
    Card getCard(int instanceId) {
        return new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
    }

    Card getRevealedCard(int instanceId) {
        Card card = new Card(1, instanceId, new CardStatistics(), null, CardType.Link, CardSubtype.Basic, "", "", new CardScript());
        card.setRevealed(true);
        return card;
    }

    @Test
    void getCardPosition_hasOneCard_returnsLibrary() {
        CardsField cardsField = new CardsField(Collections.singletonList(getCard(1)));

        Position result = cardsField.getCardPosition(1);

        assertEquals(Position.Library, result);
    }

    @Test
    void getCardPosition_hasThreeCards_returnsLibrary() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        Position[] results = new Position[3];
        results[0] = cardsField.getCardPosition(1);
        results[1] = cardsField.getCardPosition(2);
        results[2] = cardsField.getCardPosition(3);


        assertArrayEquals(new Position[]{Position.Library, Position.Library, Position.Library}, results);
    }

    @Test
    void getCardPosition_hasThreeCardsAndOneMoveCardToGraveyard_returnsGraveyardForMovedOtherLibrary() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToGraveyard(2);

        Position[] results = new Position[3];
        results[0] = cardsField.getCardPosition(1);
        results[1] = cardsField.getCardPosition(2);
        results[2] = cardsField.getCardPosition(3);

        assertArrayEquals(new Position[]{Position.Library, Position.Graveyard, Position.Library}, results);
    }

    @Test
    void getCardPosition_hasThreeCardsAndOneMoveCardToBattlefield_returnsBattlefieldForMovedOtherLibrary() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToBattlefield(2);

        Position[] results = new Position[3];
        results[0] = cardsField.getCardPosition(1);
        results[1] = cardsField.getCardPosition(2);
        results[2] = cardsField.getCardPosition(3);

        assertArrayEquals(new Position[]{Position.Library, Position.Battlefield, Position.Library}, results);
    }

    @Test
    void getCardPosition_hasThreeCardsAndOneMoveCardToHand_returnsHandForMovedOtherLibrary() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToHand(2);

        Position[] results = new Position[3];
        results[0] = cardsField.getCardPosition(1);
        results[1] = cardsField.getCardPosition(2);
        results[2] = cardsField.getCardPosition(3);

        assertArrayEquals(new Position[]{Position.Library, Position.Hand, Position.Library}, results);
    }

    @Test
    void getCardPosition_hasThreeCardsAndOneMoveCardToHandAndNextMoveToLibrary_returnsLibrary() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToHand(2);
        cardsField.moveCardToLibrary(2);

        Position[] results = new Position[3];
        results[0] = cardsField.getCardPosition(1);
        results[1] = cardsField.getCardPosition(2);
        results[2] = cardsField.getCardPosition(3);

        assertArrayEquals(new Position[]{Position.Library, Position.Library, Position.Library}, results);
    }

    @Test
    void getCard_hasThreeCardsAndOneMoveCardToHand_returnsAllCards() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToHand(2);

        Card[] results = new Card[3];
        results[0] = cardsField.getCard(1);
        results[1] = cardsField.getCard(2);
        results[2] = cardsField.getCard(3);

        assertArrayEquals(new Card[]{getCard(1), getCard(2), getCard(3)}, results);
    }

    @Test
    void getCard_hasThreeCardsAndOneMoveCardToBattlefield_returnsAllCards() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToBattlefield(2);

        Card[] results = new Card[3];
        results[0] = cardsField.getCard(1);
        results[1] = cardsField.getCard(2);
        results[2] = cardsField.getCard(3);

        assertArrayEquals(new Card[]{getCard(1), getRevealedCard(2), getCard(3)}, results);
    }

    @Test
    void getCard_hasThreeCards_returnsAllCards() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.moveCardToBattlefield(2);

        Card[] results = new Card[3];
        results[0] = cardsField.getCard(1);
        results[1] = cardsField.getCard(2);
        results[2] = cardsField.getCard(3);

        assertArrayEquals(new Card[]{getCard(1), getRevealedCard(2), getCard(3)}, results);
    }

    @Test
    void removeCard_hasTreeCardRemoveOne_getCardReturnsNull() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.removeCard(2);
        Card result = cardsField.getCard(2);

        assertNull(result);
    }

    @Test
    void removeCard_hasTreeCardRemoveOne_getCardPositionReturnsVoid() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.removeCard(2);
        Position result = cardsField.getCardPosition(2);

        assertEquals(Position.Void, result);
    }

    @Test
    void removeCard_hasTreeCardRemoveOne_getCardReturnsNullForRemovedAndReturnAllOtherCards() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.removeCard(2);
        cardsField.moveCardToBattlefield(3);

        Card[] results = new Card[3];
        results[0] = cardsField.getCard(1);
        results[1] = cardsField.getCard(2);
        results[2] = cardsField.getCard(3);

        assertArrayEquals(new Card[]{getCard(1), null, getRevealedCard(3)}, results);
    }

    @Test
    void removeCard_hasTreeCardRemoveOne_getCardPositionReturnsVoidForRemovedAndReturnCorrectPositionForOthers() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.removeCard(2);
        cardsField.moveCardToBattlefield(3);

        Position[] results = new Position[3];
        results[0] = cardsField.getCardPosition(1);
        results[1] = cardsField.getCardPosition(2);
        results[2] = cardsField.getCardPosition(3);

        assertArrayEquals(new Position[]{Position.Library, Position.Void, Position.Battlefield}, results);
    }

    @Test
    void addCard_hasTreeCardRemoveOne_getAllOldCardAndNewCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.addCard(Position.Battlefield, getCard(4));
        cardsField.moveCardToBattlefield(3);

        Card[] results = new Card[]{
                cardsField.getCard(1),
                cardsField.getCard(2),
                cardsField.getCard(3),
                cardsField.getCard(4),
        };

        assertArrayEquals(new Card[]{getCard(1), getCard(2), getRevealedCard(3), getCard(4)}, results);
    }

    @Test
    void addCard_hasTreeCardRemoveOne_returnNewCardOnSelectedPositionAndOldCardWithOldPositions() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3)));

        cardsField.addCard(Position.Battlefield, getCard(4));

        Position[] results = new Position[]{
                cardsField.getCardPosition(1),
                cardsField.getCardPosition(2),
                cardsField.getCardPosition(3),
                cardsField.getCardPosition(4),
        };

        assertArrayEquals(new Position[]{Position.Library, Position.Library, Position.Library, Position.Battlefield}, results);
    }

    @Test
    void getAllCardsPositions_hasInitialCards_returnAll() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3), getCard(4)));


        Collection<Pair<Card, Position>> results = cardsField.getAllCardsPositions();
        Collection<Pair<Card, Position>> expected = Arrays.asList(
                new Pair<>(getCard(1), Position.Library),
                new Pair<>(getCard(2), Position.Library),
                new Pair<>(getCard(3), Position.Library),
                new Pair<>(getCard(4), Position.Library)
        );

        assertArrayEquals(expected.toArray(), results.toArray());
    }

    @Test
    void getAllCardsPositions_hasMovedCards_returnAll() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3), getCard(4)));

        cardsField.moveCardToHand(1);
        cardsField.moveCardToGraveyard(3);
        cardsField.moveCardToBattlefield(4);

        Collection<Pair<Card, Position>> results = cardsField.getAllCardsPositions();
        Collection<Pair<Card, Position>> expected = Arrays.asList(
                new Pair<>(getCard(1), Position.Hand),
                new Pair<>(getCard(2), Position.Library),
                new Pair<>(getRevealedCard(3), Position.Graveyard),
                new Pair<>(getRevealedCard(4), Position.Battlefield)
        );

        assertArrayEquals(expected.toArray(), results.toArray());
    }

    @Test
    void getAllCardsPositions_removedOneCard_returnAllOthers() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3), getCard(4)));

        cardsField.moveCardToHand(1);
        cardsField.removeCard(2);
        cardsField.moveCardToGraveyard(3);
        cardsField.moveCardToBattlefield(4);

        Collection<Pair<Card, Position>> results = cardsField.getAllCardsPositions();
        Collection<Pair<Card, Position>> expected = Arrays.asList(
                new Pair<>(getCard(1), Position.Hand),
                new Pair<>(getRevealedCard(3), Position.Graveyard),
                new Pair<>(getRevealedCard(4), Position.Battlefield)
        );

        assertArrayEquals(expected.toArray(), results.toArray());
    }

    @Test
    void getAllCardsPositions_removedOneCardAndOneAdd_returnAllOthers() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2), getCard(3), getCard(4)));

        cardsField.moveCardToHand(1);
        cardsField.removeCard(2);
        cardsField.moveCardToGraveyard(3);
        cardsField.moveCardToBattlefield(4);
        cardsField.addCard(Position.Library, getCard(5));

        Collection<Pair<Card, Position>> results = cardsField.getAllCardsPositions();
        Collection<Pair<Card, Position>> expected = Arrays.asList(
                new Pair<>(getCard(1), Position.Hand),
                new Pair<>(getRevealedCard(3), Position.Graveyard),
                new Pair<>(getRevealedCard(4), Position.Battlefield),
                new Pair<>(getCard(5), Position.Library)
        );

        assertArrayEquals(expected.toArray(), results.toArray());
    }

    @Test
    void moveCardToHand_hasCardInLibrary_shouldNotRevealCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1)));

        cardsField.moveCardToHand(1);

        Card card = cardsField.getCard(1);

        assertFalse(card.isRevealed());
    }

    @Test
    void moveCardToBattlefield_hasCardInLibrary_shouldRevealCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1)));

        cardsField.moveCardToBattlefield(1);

        Card card = cardsField.getCard(1);

        assertTrue(card.isRevealed());
    }

    @Test
    void moveCardToGraveyard_hasCardInLibrary_shouldRevealCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1)));

        cardsField.moveCardToGraveyard(1);

        Card card = cardsField.getCard(1);

        assertTrue(card.isRevealed());
    }

    @Test
    void moveCardToVoid_hasCardInLibrary_shouldRevealCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1)));

        cardsField.moveCardToVoid(1);

        Card card = cardsField.getCard(1);

        assertTrue(card.isRevealed());
    }

    @Test
    void moveCardToLibrary_hasCardOnBattlefield_shouldCardBeRevealed() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1)));

        cardsField.moveCardToBattlefield(1);
        cardsField.moveCardToLibrary(1);

        Card card = cardsField.getCard(1);

        assertTrue(card.isRevealed());
    }

    @Test
    void getFirstCardAtPosition_fromLibraryAndHasStartingOrder_shouldReturnFirstCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        Card result = cardsField.getFirstCardAtPosition(Position.Library);

        assertEquals(getCard(1), result);
    }

    @Test
    void getFirstCardAtPosition_fromLibraryAndHasStartingOrderFirstCardMoved_shouldReturnSecondCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        Card result = cardsField.getFirstCardAtPosition(Position.Library);

        assertEquals(getCard(2), result);
    }

    @Test
    void getFirstCardAtPosition_fromLibraryAndHasStartingOrderAllCardsMoved_shouldReturnNull() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        cardsField.moveCardToHand(2);
        Card result = cardsField.getFirstCardAtPosition(Position.Library);

        assertNull(result);
    }

    @Test
    void getFirstCardAtPosition_fromHandAndHasStartingOrder_shouldReturnNull() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        Card result = cardsField.getFirstCardAtPosition(Position.Hand);

        assertNull(result);
    }

    @Test
    void getFirstCardAtPosition_fromHandAndHasStartingOrderFirstCardMoved_shouldReturnFirstCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        Card result = cardsField.getFirstCardAtPosition(Position.Hand);

        assertEquals(getCard(1), result);
    }

    @Test
    void getFirstCardAtPosition_fromHandAndHasStartingOrderAllCardsMoved_shouldReturnFirstCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        cardsField.moveCardToHand(2);
        Card result = cardsField.getFirstCardAtPosition(Position.Hand);

        assertEquals(getCard(1), result);
    }

    @Test
    void getFirstCardAtPosition_fromHandAndHasStartingOrderAllCardsMovedAndFirstMovedOneAgain_shouldReturnSecondCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        cardsField.moveCardToHand(2);
        cardsField.moveCardToBattlefield(1);
        Card result = cardsField.getFirstCardAtPosition(Position.Hand);

        assertEquals(getCard(2), result);
    }

    @Test
    void getFirstCardAtPosition_cardInHandMoveToHandZeroIndex_shouldReturnCardMovedAtIndex() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        cardsField.moveCardAtPositionIndex(2, Position.Hand, 0);
        Card result = cardsField.getFirstCardAtPosition(Position.Hand);

        assertEquals(getCard(2), result);
    }

    @Test
    void getFirstCardAtPosition_cardInHandInsertAtHandZeroIndex_shouldReturnInsertedCard() {
        CardsField cardsField = new CardsField(Arrays.asList(getCard(1), getCard(2)));

        cardsField.moveCardToHand(1);
        cardsField.insertCardAtPositionIndex(getCard(3), Position.Hand, 0);
        Card result = cardsField.getFirstCardAtPosition(Position.Hand);

        assertEquals(getCard(3), result);
    }
}