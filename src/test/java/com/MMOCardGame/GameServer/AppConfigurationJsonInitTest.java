package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.impl.AppConfigurationJsonInit;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class AppConfigurationJsonInitTest {
    String getCorrectJsonForSettings() {
        return "{" +
                "\"userServerPort\":1410," +
                "\"serviceServerPort\":1914," +
                "\"volatileResourcesLifeDuration\":10," +
                "\"innerServiceToken\":\"TestToken\"" +
                "}";
    }

    String getJsonWithIncorrectParamForSettings() {
        return "{" +
                "\"userServerPort\":1410," +
                "\"serviceServerPort\":1914," +
                "\"volatileResourcesLifeDuration\":10," +
                "\"innerServiceToken\":\"TestToken\"" +
                ",\"serverShouldStart\":false" +
                "}";
    }

    AppConfiguration.Settings getCorrectSettings() {
        AppConfiguration.Settings settings = new AppConfiguration.Settings();
        settings.serviceServerPort = 1914;
        settings.userServerPort = 1410;
        settings.volatileResourcesLifeDuration = 10;
        settings.innerServiceToken = "TestToken";
        return settings;
    }

    @Test
    void readSettingsFromStream_correctJson_returnCorrectSettings() {
        byte[] byteArray = getCorrectJsonForSettings().getBytes();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);

        AppConfiguration.Settings resultSettings = AppConfigurationJsonInit.readSettingsFromStream(byteArrayInputStream);
        assertEquals(getCorrectSettings(), resultSettings);
    }

    @Test
    void readSettingsFromStream_emptyJson_returnEmptySettings() {
        byte[] byteArray = "{}".getBytes();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);

        AppConfiguration.Settings resultSettings = AppConfigurationJsonInit.readSettingsFromStream(byteArrayInputStream);
        assertNotEquals(getCorrectSettings(), resultSettings);
    }

    @Test
    void readSettingsFromStream_JsonWithForcedParams_ignoreIncorrectParamAndReturnCorrectSettings() {
        byte[] byteArray = getJsonWithIncorrectParamForSettings().getBytes();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);

        AppConfiguration.Settings resultSettings = AppConfigurationJsonInit.readSettingsFromStream(byteArrayInputStream);
        assertEquals(getCorrectSettings(), resultSettings);
    }

    @Test
    void saveSettingsToStream_correctSettings_returnsCorrectJson() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        AppConfigurationJsonInit.saveSettingsToStream(byteArrayOutputStream, getCorrectSettings());

        assertEquals(getCorrectJsonForSettings(), new String(byteArrayOutputStream.toByteArray()));
    }

}