package com.MMOCardGame.GameServer.Servers;

import com.MMOCardGame.GameServer.Authentication.UserTokenAuthentication;
import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;
import com.MMOCardGame.GameServer.Servers.exceptions.NotFoundConnectionException;
import com.MMOCardGame.GameServer.Servers.exceptions.UserDuplicationException;
import com.MMOCardGame.GameServer.common.ObservableNotifier;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public abstract class UserConnectionsManager {
    private final UserTokenAuthentication userTokenAuthentication;
    private final Map<String, UserConnection> connectionMap;
    private final ObservableNotifier<UserConnection> createdConnectionObservable = new ObservableNotifier<>();
    private final ObservableNotifier<UserConnection> removedConnectionObservable = new ObservableNotifier<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public UserConnectionsManager(UserTokenAuthentication userTokenAuthentication) {
        this.userTokenAuthentication = userTokenAuthentication;
        connectionMap = new HashMap<>();
    }

    public UserConnection createConnection(String name, String token) throws AuthenticationException {
        userTokenAuthentication.checkUser(name, token);
        return createNewConnection(name);
    }

    private UserConnection createNewConnection(String name) {
        UserConnection connection;
        lock.writeLock().lock();
        try {
            checkForDuplicate(name);
            connection = createImplementationOfConnection(name);
            connectionMap.put(name, connection);
        } finally {
            lock.writeLock().unlock();
        }
        createdConnectionObservable.notifyAll(connection);
        return connection;
    }

    protected abstract UserConnection createImplementationOfConnection(String username);

    private void checkForDuplicate(String name) {
        if (connectionMap.containsKey(name))
            throw new UserDuplicationException("Connection for user " + name + " already exists");
    }

    public UserConnection getConnectionForUser(String name) {
        UserConnection connection = null;
        lock.readLock().lock();
        try {
            connection = connectionMap.get(name);
        } finally {
            lock.readLock().unlock();
        }

        if (connection == null) throw new NotFoundConnectionException("Not found connection for user: " + name);
        return connection;
    }

    public boolean hasConnectionForUser(String name) {
        lock.readLock().lock();
        try {
            return connectionMap.containsKey(name);
        } finally {
            lock.readLock().unlock();
        }
    }

    public void removeConnectionForUser(String name) {
        UserConnection connection = removeConnectionForUserSilent(name);
        removedConnectionObservable.notifyAll(connection);
    }

    public UserConnection removeConnectionForUserSilent(String name) {
        UserConnection connection = null;
        lock.writeLock().lock();
        try {
            if (!connectionMap.containsKey(name))
                throw new NotFoundConnectionException("Not found connection for user: " + name);
            connection = connectionMap.get(name);
            connectionMap.remove(name);
        } finally {
            lock.writeLock().unlock();
        }
        connection.close();
        return connection;
    }

    public void addListenerForCreate(UserConnectionObserver userConnectionListener) {
        createdConnectionObservable.addObserver(userConnectionListener);
    }

    public void removeListenerForCreate(UserConnectionObserver userConnectionListener) {
        createdConnectionObservable.removeObserver(userConnectionListener);
    }

    public void removeListenerForRemove(UserConnectionObserver userConnectionListener) {
        removedConnectionObservable.removeObserver(userConnectionListener);
    }

    public void addListenerForRemove(UserConnectionObserver userConnectionListener) {
        removedConnectionObservable.addObserver(userConnectionListener);
    }

    public synchronized Stream<UserConnection> stream() {
        lock.readLock().lock();
        try {
            return connectionMap.values().stream();
        } finally {
            lock.readLock().unlock();
        }
    }

    public synchronized Stream<UserConnection> parralelStream() {
        lock.readLock().lock();
        try {
            return connectionMap.values().parallelStream();
        } finally {
            lock.readLock().unlock();
        }
    }

    public synchronized void forEach(BiConsumer<? super String, ? super UserConnection> consumer) {
        lock.readLock().lock();
        try {
            connectionMap.forEach(consumer);
        } finally {
            lock.readLock().unlock();
        }
    }

    public UserConnection reconnectUserAndGetConnection(String userName, String innerToken) {
        UserConnection connection = getConnectionForUser(userName);
        if (!connection.getToken().toString().equals(innerToken))
            throw new AuthenticationException("Incorrect token");
        connection.startReconnect();
        return connection;
    }

    public synchronized void closeAllConnections() {
        lock.writeLock().lock();
        try {
            connectionMap.forEach((name, connection) -> connection.close());
            connectionMap.clear();
        } finally {
            lock.writeLock().unlock();
        }
    }
}
