package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Authentication.exceptions.AuthenticationException;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.exceptions.NotFoundConnectionException;
import com.MMOCardGame.GameServer.Servers.exceptions.UserDuplicationException;
import com.MMOCardGame.GameServer.common.CallbackTask;
import com.MMOCardGame.GameServer.common.CallbackTask2;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class GrpcUserServerService extends UserServerGrpc.UserServerImplBase {
    private final Logger logger = LoggerFactory.getLogger(GrpcUserServerService.class);
    private UserConnectionsManager connectionsManager;
    private UserTokenContainer tokenContainer;

    public GrpcUserServerService(UserConnectionsManager connectionsManager, UserTokenContainer tokenContainer) {
        this.connectionsManager = connectionsManager;
        this.tokenContainer = tokenContainer;
    }

    @Override
    public void connect(Hello request, StreamObserver<ConnectResponse> responseObserver) {
        ConnectResponse connectResponse = createNewUserConnection(request);
        sendResponseAndCloseStream(responseObserver, connectResponse);
    }

    private ConnectResponse createNewUserConnection(Hello request) {
        try {
            return tryCreateNewUserConnection(request);
        } catch (UserDuplicationException e) {
            logger.debug("Duplicated access attempt");
            return ConnectResponse.newBuilder()
                    .setResponse(HelloResponse.newBuilder().setReponseCode(ErrorCodes.AccessForbidden))
                    .build();
        } catch (AuthenticationException e) {
            return ConnectResponse.newBuilder()
                    .setResponse(HelloResponse.newBuilder().setReponseCode(ErrorCodes.AccessForbidden))
                    .build();
        } catch (Throwable e) {
            logger.error("Internal error", e);
            return ConnectResponse.newBuilder()
                    .setResponse(HelloResponse.newBuilder().setReponseCode(ErrorCodes.InternalError))
                    .build();
        }
    }

    private ConnectResponse tryCreateNewUserConnection(Hello request) {
        GrpcUserConnection connection = (GrpcUserConnection) connectionsManager.createConnection(request.getUserName(), request.getUserPasswordToken());
        tokenContainer.removeTokenForUser(request.getUserName());
        return ConnectResponse.newBuilder()
                .setResponse(HelloResponse.newBuilder()
                        .setReponseCode(0)
                        .setConnectionToken(connection.getToken().toString()))
                .setGameState(connection.getWholeGameState())
                .build();
    }

    @Override
    public void reconnect(ReconnectHello request, StreamObserver<ConnectResponse> responseObserver) {
        ConnectResponse connectResponse = reconnectUser(request);

        sendResponseAndCloseStream(responseObserver, connectResponse);
    }

    private ConnectResponse reconnectUser(ReconnectHello request) {
        try {
            return tryReconnectUser(request);
        } catch (AuthenticationException | NotFoundConnectionException e) {
            return ConnectResponse.newBuilder()
                    .setResponse(HelloResponse.newBuilder().setReponseCode(ErrorCodes.AccessForbidden))
                    .build();
        } catch (Throwable e) {
            logger.error("Internal error", e);
            return ConnectResponse.newBuilder()
                    .setResponse(HelloResponse.newBuilder().setReponseCode(ErrorCodes.InternalError))
                    .build();
        }
    }

    private ConnectResponse tryReconnectUser(ReconnectHello request) {
        GrpcUserConnection userConnection = (GrpcUserConnection) connectionsManager.reconnectUserAndGetConnection(request.getUserName(), request.getInnerToken());
        return ConnectResponse.newBuilder()
                .setResponse(HelloResponse.newBuilder()
                        .setReponseCode(0)
                        .setConnectionToken(userConnection.getToken().toString()))
                .setGameState(userConnection.getWholeGameState())
                .build();
    }

    private GrpcUserConnection getUserConnection(String username, String token) {
        UserConnection connection = connectionsManager.getConnectionForUser(username);
        if (connection.getToken().toString().equals(token))
            return (GrpcUserConnection) connection;
        else throw new AuthenticationException("Security token is incorrect");
    }

    @Override
    public void surrender(Hello request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getUserName(), request.getUserPasswordToken());
        connection.surrender();
        sendResponseAndCloseStream(responseObserver, SimpleResponse.newBuilder().build());
    }

    @Override
    public void returningMessagesStream(Hello request, StreamObserver<StreamingMessage> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getUserName(), request.getUserPasswordToken());
        connection.registerReturningMessagesStream(responseObserver);
    }

    @Override
    public void getAllGameState(Hello request, StreamObserver<GameStateMessage> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getUserName(), request.getUserPasswordToken());
        sendResponseAndCloseStream(responseObserver, connection.getWholeGameState());
    }

    @Override
    public void cancelGame(Hello request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getUserName(), request.getUserPasswordToken());
        doSimpleTaskAndSendMessage(responseObserver, connection::cancelGame);
    }

    private void doSimpleTaskAndSendMessage(StreamObserver<SimpleResponse> responseObserver, CallbackTask2<Runnable, CallbackTask<Throwable>> task) {
        try {
            task.run(() -> sendResponseAndCloseStream(responseObserver, SimpleResponse.newBuilder().build()),
                    throwable -> {
                        SimpleResponse response = SimpleResponse.newBuilder()
                                .setResponseCode(getErrorCode(throwable))
                                .build();
                        sendResponseAndCloseStream(responseObserver, response);
                    });
        } catch (Throwable e) {
            sendInternalErrorSimpleResponseAndLog(responseObserver, e);
        }
    }

    private int getErrorCode(Throwable throwable) {
        logger.error("Catch exception", throwable);
        if (throwable instanceof InconsistentPlayerCommand)
            return ErrorCodes.InconsistentClientState;
        if (throwable instanceof BadRequestException)
            return ErrorCodes.BadRequest;
        if (throwable instanceof NotFoundException)
            return ErrorCodes.NotFound;
        return ErrorCodes.InternalError;
    }

    private void sendInternalErrorSimpleResponseAndLog(StreamObserver<SimpleResponse> responseObserver, Throwable e) {
        sendResponseAndCloseStream(responseObserver, getInternalErrorSimpleResponse(e));
        logger.error("Catch exception", e);
    }

    private SimpleResponse getInternalErrorSimpleResponse(Throwable e) {
        try {
            logger.error("Catch exception", e);
            if (e instanceof InconsistentPlayerCommand)
                return SimpleResponse.newBuilder()
                        .setResponseCode(ErrorCodes.InconsistentClientState)
                        .build();
            if(e instanceof BadRequestException)
                return SimpleResponse.newBuilder()
                        .setResponseCode(ErrorCodes.BadRequest)
                        .build();
            if(e.getMessage()!=null)
            return SimpleResponse.newBuilder()
                    .setResponseCode(ErrorCodes.InternalError)
                    .setResponseText(e.getMessage())
                    .build();
            return SimpleResponse.newBuilder()
                    .setResponseCode(ErrorCodes.InternalError)
                    .build();
        } catch (Throwable err) {
            logger.error("Catch exception", err);
            return SimpleResponse.newBuilder()
                    .setResponseCode(ErrorCodes.InternalError)
                    .build();
        }
    }

    @Override
    public void useCard(CardUsedRequest request, StreamObserver<AddedToStackResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.useCard(request.getMessage(),
                    cardUsed -> sendResponseAndCloseStream(responseObserver, getAddedToStackResponseWithCardUsed(cardUsed)),
                    throwable -> sendResponseAndCloseStream(responseObserver, getAddedToStackResponseWithInternalError(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getAddedToStackResponseWithInternalError(e), e);
        }
    }

    private <U> void sendResponseAndCloseStream(StreamObserver<U> streamObserver, U message) {
        streamObserver.onNext(message);
        streamObserver.onCompleted();
    }

    private AddedToStackResponse getAddedToStackResponseWithCardUsed(CardUsed cardUsed) {
        return AddedToStackResponse.newBuilder()
                .setSimpleResponse(SimpleResponse.newBuilder().build())
                .setCardUsed(cardUsed)
                .build();
    }

    private AddedToStackResponse getAddedToStackResponseWithInternalError(Throwable throwable) {
        return AddedToStackResponse.newBuilder()
                .setSimpleResponse(getInternalErrorSimpleResponse(throwable))
                .build();
    }

    private <U> void sendErrorResponseAndLog(StreamObserver<U> responseObserver, U message, Throwable e) {
        sendResponseAndCloseStream(responseObserver, message);
        logger.error("Catch exception", e);
    }

    @Override
    public void endPhase(PhaseChangedRequest request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        doSimpleTaskAndSendMessage(responseObserver,
                (task, errorTask) -> connection.endPhase(request.getMessage(), task, errorTask));
    }

    @Override
    public void useHeroAbility(HeroAbilityUsedRequest request, StreamObserver<AbilityResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.useHeroAbility(request.getMessage(),
                    (message, integer) -> sendResponseAndCloseStream(responseObserver, getAbilityResponseWithMessage(message, integer)),
                    throwable -> sendResponseAndCloseStream(responseObserver, getAbilityResponseWithInternalError(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getAbilityResponseWithInternalError(e), e);
        }
    }

    private AbilityResponse getAbilityResponseWithMessage(CardCreated message, Integer integer) {
        return AbilityResponse.newBuilder()
                .setSimpleResponse(SimpleResponse.newBuilder().build())
                .setStackId(integer)
                .setCardCreated(message)
                .build();
    }

    @Override
    public void selectAttackersDefenders(AttackersDefendersSelectedRequest request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        doSimpleTaskAndSendMessage(responseObserver, (task, errorTask) -> connection.selectAttackersDefenders(request.getMessage(), task, errorTask));
    }

    private AbilityResponse getAbilityResponseWithInternalError(Throwable throwable) {
        return AbilityResponse.newBuilder()
                .setSimpleResponse(getInternalErrorSimpleResponse(throwable))
                .build();
    }

    @Override
    public void drawCards(HandChangedCardsRequest request, StreamObserver<HandChangedCardsResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.drawCards(request.getMessage(),
                    (message) -> sendResponseAndCloseStream(responseObserver, getHandChangeddCardsResponseWithMessage(message)),
                    throwable -> sendResponseAndCloseStream(responseObserver, getHandChangedCardsReponseWithInternalError(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getHandChangedCardsReponseWithInternalError(e), e);
        }
    }

    private HandChangedCardsResponse getHandChangedCardsReponseWithInternalError(Throwable throwable) {
        return HandChangedCardsResponse.newBuilder()
                .setSimpleResponse(getInternalErrorSimpleResponse(throwable))
                .build();
    }

    @Override
    public void discardCards(HandChangedCardsRequest request, StreamObserver<HandChangedCardsResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.discardCards(request.getMessage(),
                    (message) -> sendResponseAndCloseStream(responseObserver, getHandChangeddCardsResponseWithMessage(message)),
                    throwable -> sendResponseAndCloseStream(responseObserver, getHandChangedCardsReponseWithInternalError(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getHandChangedCardsReponseWithInternalError(e), e);
        }
    }

    @Override
    public void muligan(Hello request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getUserName(), request.getUserPasswordToken());
        doSimpleTaskAndSendMessage(responseObserver,connection::muligan);
    }

    @Override
    public void replaceCards(HandChangedCardsRequest request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.replaceCards(request.getMessage(),
                    () -> sendResponseAndCloseStream(responseObserver, SimpleResponse.newBuilder().build()),
                    throwable -> sendResponseAndCloseStream(responseObserver, getInternalErrorSimpleResponse(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getInternalErrorSimpleResponse(e), e);
        }
    }

    private HandChangedCardsResponse getHandChangeddCardsResponseWithMessage(HandChanged message) {
        return HandChangedCardsResponse.newBuilder()
                .setSimpleResponse(SimpleResponse.newBuilder().build())
                .setMessage(message)
                .build();
    }

    @Override
    public void keepStartingHand(Hello request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getUserName(), request.getUserPasswordToken());
        doSimpleTaskAndSendMessage(responseObserver, connection::getStartingHand);
    }

    @Override
    public void doTriggerReaction(TriggerReactionRequest request, StreamObserver<SimpleResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.doTriggerReaction(request.getMessage(),
                    () -> sendResponseAndCloseStream(responseObserver, SimpleResponse.newBuilder().build()),
                    throwable -> sendResponseAndCloseStream(responseObserver, getInternalErrorSimpleResponse(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getInternalErrorSimpleResponse(e), e);
        }
    }

    @Override
    public void targetSelectingTransaction(TargetSelectingTransactionRequest request, StreamObserver<TargetSelectingTransactionResponse> responseObserver) {
        GrpcUserConnection connection = getUserConnection(request.getHeader().getUserName(), request.getHeader().getUserPasswordToken());
        try {
            connection.doTargetSelectingTransaction(request.getMessage(),
                    (targetSelectingTransaction) -> sendResponseAndCloseStream(responseObserver, getTargetSelectingResponse(targetSelectingTransaction)),
                    throwable -> sendResponseAndCloseStream(responseObserver, getTargetSelectingResponseWithInternalError(throwable)));
        } catch (Throwable e) {
            sendErrorResponseAndLog(responseObserver, getTargetSelectingResponseWithInternalError(e), e);
        }
    }

    private TargetSelectingTransactionResponse getTargetSelectingResponse(TargetSelectingTransaction targetSelectingTransaction) {
        return TargetSelectingTransactionResponse.newBuilder()
                .setResponse(SimpleResponse.newBuilder().build())
                .setMessage(targetSelectingTransaction)
                .build();
    }

    private TargetSelectingTransactionResponse getTargetSelectingResponseWithInternalError(Throwable throwable) {
        return TargetSelectingTransactionResponse.newBuilder()
                .setResponse(getInternalErrorSimpleResponse(throwable))
                .build();
    }
}