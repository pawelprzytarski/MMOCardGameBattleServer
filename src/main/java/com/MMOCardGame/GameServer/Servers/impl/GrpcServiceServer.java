package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.Servers.AuthInterceptService;
import com.MMOCardGame.GameServer.Servers.ServiceServer;
import com.MMOCardGame.GameServer.factories.ExecutorFactory;
import io.grpc.ServerBuilder;

public class GrpcServiceServer extends GrpcServer implements ServiceServer {
    private AppConfiguration configuration;
    private GrpcServiceServerService serverService;

    public GrpcServiceServer(AppConfiguration configuration, UserTokenContainer tokenContainer, GameSessionManager gameSessionManager) {
        this.configuration = configuration;
        this.serverService = new GrpcServiceServerService(tokenContainer, gameSessionManager, configuration);
        serverType = "ServiceServer";
    }

    @Override
    protected io.grpc.Server buildServer() {
        return ServerBuilder.forPort(configuration.getServiceServerPort())
                .addService(serverService)
                .executor(ExecutorFactory.getTaskExecutor())
                .intercept(new AuthInterceptService(configuration))
                .build();
    }
}
