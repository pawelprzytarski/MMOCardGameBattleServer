package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.Servers.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public abstract class GrpcServer implements Server {
    String serverType = "Server";
    private Logger logger = LoggerFactory.getLogger(GrpcServer.class);
    private io.grpc.Server server;

    @Override
    public void start() throws IOException {
        logger.info("Started " + serverType);
        server = buildServer();
        server.start();
    }

    protected abstract io.grpc.Server buildServer();

    @Override
    public void stop() {
        logger.info("Stopped service server");
        if (server != null) {
            server.shutdown();
            server = null;
        }
    }

    @Override
    public void awaitTermination() {
        logger.info("Await " + serverType);
        if (server != null) {
            try {
                server.awaitTermination();
            } catch (InterruptedException e) {
                logger.warn("Exception catch in " + serverType, e);
                Thread.currentThread().interrupt();
            }
        }
    }
}
