package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class GrpcServiceServerService extends InnerServerGrpc.InnerServerImplBase {
    private final Logger logger= LoggerFactory.getLogger(GrpcServiceServerService.class);
    private UserTokenContainer tokenContainer;
    private GameSessionManager gameSessionManager;
    private AppConfiguration configuration;

    public GrpcServiceServerService(UserTokenContainer tokenContainer, GameSessionManager gameSessionManager, AppConfiguration configuration) {
        this.tokenContainer = tokenContainer;
        this.gameSessionManager = gameSessionManager;
        this.configuration = configuration;
    }

    @Override
    public void registerGameSession(NewSessionData request, StreamObserver<Reply> responseObserver) {
        processRegisterSessionRequest(request, responseObserver);
    }

    private void processRegisterSessionRequest(NewSessionData request, StreamObserver<Reply> responseObserver) {
        try {
            tryProcessRegisterSessionRequest(request, responseObserver);
        } catch (Throwable e) {
            logger.error("RegisterSessionRequest error", e);
            responseObserver.onNext(Reply.newBuilder().setErrorCode(ErrorCodes.InternalError).build());
            responseObserver.onCompleted();
        }
    }

    private void tryProcessRegisterSessionRequest(NewSessionData request, StreamObserver<Reply> responseObserver) {
        if (isNotValidSessionData(request.getGameType(), request.getUsersList(), request.getSessionId())) {
            responseObserver.onNext(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
            responseObserver.onCompleted();
        } else {
            registerGameSessionAndUserTokens(request.getGameType(), request.getUsersList(), request.getSessionId());
            responseObserver.onNext(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
            responseObserver.onCompleted();
        }
    }

    private void registerGameSessionAndUserTokens(int gameType, List<UserData> usersList, int sessionId) {
        registerGameSessions(gameType, usersList, sessionId);
        registerUserTokens(usersList);
    }

    private void registerUserTokens(List<UserData> usersList) {
        usersList.stream().forEach(userData -> tokenContainer.registerNewUserToken(userData.getUsername(), userData.getToken()));
    }

    private void registerGameSessions(int gameType, List<UserData> usersList, int sessionId) {
        GameSessionSettings sessionSettings = new GameSessionSettings();
        sessionSettings.setTypeOfMatch(gameType);
        sessionSettings.setListOfUsers(usersList.stream().map(UserData::getUsername).collect(Collectors.toList()));
        sessionSettings.setSelectedHeros(usersList.stream().collect(Collectors.toMap(UserData::getUsername, UserData::getHeroId)));
        usersList.forEach(userData -> sessionSettings.setCardsForUser(userData.getUsername(), userData.getCardIdsList()));
        sessionSettings.setSessionId(sessionId);
        gameSessionManager.registerNewGameSession(sessionSettings);
    }

    private boolean isNotValidSessionData(int gameType, List<UserData> users, int sessionId) {
        return (gameType == GameSessionSettings.MatchType.Team && users.size() != 4) || users.size() < 2
                || (gameType != GameSessionSettings.MatchType.Single && gameType != GameSessionSettings.MatchType.Team)
                || isIncorrectCards(users) || sessionId == 0;
    }

    private boolean isIncorrectCards(List<UserData> users) {
        boolean result = false;
        for (UserData data : users) {
            if (data.getCardIdsCount() < GameConstants.getDeckMinCount())
                result = true;
        }
        return result;
    }

    @Override
    public void registerCallbackUrl(CallbackUrl request, StreamObserver<Reply> responseObserver) {
        processRegisterCallbackRequest(request, responseObserver);
    }

    private void processRegisterCallbackRequest(CallbackUrl request, StreamObserver<Reply> responseObserver) {
        if (request.getCallbackSecureToken() != null && !request.getCallbackSecureToken().isEmpty()
                && request.getCallbackUrl() != null && !request.getCallbackUrl().isEmpty()) {
            updateAppConfiguration(request);
            responseObserver.onNext(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build());
            responseObserver.onCompleted();
        } else {
            responseObserver.onNext(Reply.newBuilder().setErrorCode(ErrorCodes.BadRequest).build());
            responseObserver.onCompleted();
        }
    }

    private void updateAppConfiguration(CallbackUrl request) {
        configuration.setCallbackUrl(request.getCallbackUrl());
        configuration.setCallbackToken(request.getCallbackSecureToken());
    }

    @Override
    public void dumpAllGameStates(Empty request, StreamObserver<Reply> responseObserver) {
        new Thread(() -> gameSessionManager.dumpAllSessions());
    }
}
