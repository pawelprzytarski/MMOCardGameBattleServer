package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.StreamingMessage;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.google.protobuf.Descriptors;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class GrpcUserConnection extends UserConnection {
    Logger logger= LoggerFactory.getLogger(GrpcUserConnection.class);
    private StreamObserver<StreamingMessage> messageStream = new BufferStreamObserver<>();

    public GrpcUserConnection(String userName) {
        super(userName);
        setPrepared(false);
    }

    @Override
    public void close() {
        messageStream.onCompleted();
    }

    @Override
    public void kickPlayer(String reason) {
        throw new RuntimeException("Not implemented yet");
    }

    @Override
    public void sendMessage(Object message) {
        Descriptors.FieldDescriptor descriptor = getFieldDescriptorForMessage(message);
        if (descriptor == null)
            throw new IllegalArgumentException("Message has unsupported type");
        sendMessageToStream(message, descriptor);
    }

    private void sendMessageToStream(Object message, Descriptors.FieldDescriptor descriptor) {
        StreamingMessage streamingMessage = StreamingMessage.newBuilder()
                .setType(descriptor.getIndex())
                .setField(descriptor, message)
                .build();
        try {
            messageStream.onNext(streamingMessage);
        }catch(Exception e){
            messageStream=new BufferStreamObserver<>();
            messageStream.onNext(streamingMessage);
            setPrepared(false);
            logger.error("Error during sending streaming message switching to buffer", e);
        }
    }

    private Descriptors.FieldDescriptor getFieldDescriptorForMessage(Object message) {
        String className = message.getClass().getSimpleName();
        Descriptors.FieldDescriptor descriptor = null;
        for (Descriptors.FieldDescriptor fieldDescriptor : StreamingMessage.getDescriptor().getFields()) {
            if (fieldDescriptor.getName().equals(className)) {
                descriptor = fieldDescriptor;
                break;
            }
        }
        return descriptor;
    }

    private void readAllActionsFromBufferStreamObserver(StreamObserver responseObserver, StreamObserver previousStreamObserver) {
        if (previousStreamObserver.getClass() == BufferStreamObserver.class) {
            BufferStreamObserver bufferStreamObserver = ((BufferStreamObserver) previousStreamObserver);
            bufferStreamObserver.listOfMessages.forEach(responseObserver::onNext);
            if (bufferStreamObserver.completed) responseObserver.onCompleted();
        }
    }

    public void registerReturningMessagesStream(StreamObserver<StreamingMessage> responseObserver) {
        readAllActionsFromBufferStreamObserver(responseObserver, messageStream);
        messageStream = responseObserver;
        setPrepared(true);
    }
}


class BufferStreamObserver<V> implements StreamObserver<V> {
    List<V> listOfMessages = new ArrayList<>();
    boolean completed = false;

    @Override
    public void onNext(V v) {
        if (!completed)
            listOfMessages.add(v);
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onCompleted() {
        completed = true;
    }
}