package com.MMOCardGame.GameServer.Servers.impl;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.UserServer;
import io.grpc.Server;
import io.grpc.ServerBuilder;

public class GRpcUserServer extends GrpcServer implements UserServer {
    private AppConfiguration configuration;
    private GrpcUserServerService userServerService;

    public GRpcUserServer(AppConfiguration configuration, UserConnectionsManager connectionsManager, UserTokenContainer tokenContainer) {
        serverType="UserServer";
        this.configuration = configuration;
        this.userServerService = new GrpcUserServerService(connectionsManager, tokenContainer);
    }

    @Override
    protected Server buildServer() {
        return ServerBuilder.forPort(configuration.getUserServerPort())
                .addService(userServerService)
                .build();
    }
}

