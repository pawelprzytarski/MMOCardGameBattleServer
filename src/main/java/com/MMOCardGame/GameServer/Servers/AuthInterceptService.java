package com.MMOCardGame.GameServer.Servers;

import com.MMOCardGame.GameServer.AppConfiguration;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.Status;

public class AuthInterceptService implements io.grpc.ServerInterceptor {
    private final String token;

    public AuthInterceptService(AppConfiguration configuration) {
        this.token = configuration.getInnerServiceToken();
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> next) {
        String token = metadata.get(Metadata.Key.of("Token", Metadata.ASCII_STRING_MARSHALLER));
        if (!this.token.equals(token))
            serverCall.close(Status.PERMISSION_DENIED, metadata);
        return next.startCall(serverCall, metadata);
    }
}
