package com.MMOCardGame.GameServer.Servers.factories;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainerFactory;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.Servers.ServiceServer;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.UserServer;
import com.MMOCardGame.GameServer.Servers.impl.GRpcUserServer;
import com.MMOCardGame.GameServer.Servers.impl.GrpcServiceServer;
import com.MMOCardGame.GameServer.factories.AppConfigurationProvider;
import dagger.Component;
import dagger.Module;
import dagger.Provides;

@Component(modules = {UserServerFactory.class, ServiceServerFactory.class, UserTokenContainerFactory.class})
public interface ServersFactory {
    UserServerBuilder getUserServerBuilder();

    ServiceServerBuilder getServiceServerBuilder();
}


@Module(includes = AppConfigurationProvider.class)
class UserServerFactory {
    @Provides
    UserServerBuilder getUserServer(AppConfiguration configuration, UserTokenContainer tokenContainer) {
        return new GrpcUserServerBuilder(tokenContainer).setAppConfiguration(configuration);
    }

    class GrpcUserServerBuilder implements UserServerBuilder {
        private AppConfiguration configuration = null;
        private UserConnectionsManager connectionsManager = null;
        private UserTokenContainer tokenContainer;

        public GrpcUserServerBuilder(UserTokenContainer tokenContainer) {
            this.tokenContainer = tokenContainer;
        }

        @Override
        public UserServerBuilder setAppConfiguration(AppConfiguration appConfiguration) {
            this.configuration = appConfiguration;
            return this;
        }

        @Override
        public UserServerBuilder setConnectionManager(UserConnectionsManager connectionManager) {
            this.connectionsManager = connectionManager;
            return this;
        }

        @Override
        public UserServer build() {
            if (connectionsManager == null) throw new NullPointerException("UserConnectionsManger cannot be null");
            return new GRpcUserServer(configuration, connectionsManager, tokenContainer);
        }
    }
}

@Module
class ServiceServerFactory {
    @Provides
    ServiceServerBuilder getServiceServer(AppConfiguration configuration, UserTokenContainer tokenContainer) {
        return new GrpcServiceServerBuilder().setAppConfiguration(configuration).setTokenContainer(tokenContainer);
    }

    class GrpcServiceServerBuilder implements ServiceServerBuilder {
        private AppConfiguration configuration;
        private UserTokenContainer tokenContainer;
        private GameSessionManager gameSessionManager;

        @Override
        public ServiceServerBuilder setAppConfiguration(AppConfiguration appConfiguration) {
            configuration = appConfiguration;
            return this;
        }

        @Override
        public ServiceServerBuilder setTokenContainer(UserTokenContainer tokenContainer) {
            this.tokenContainer = tokenContainer;
            return this;
        }

        @Override
        public ServiceServerBuilder setGameSessionManager(GameSessionManager gameSessionManager) {
            this.gameSessionManager = gameSessionManager;
            return this;
        }

        @Override
        public ServiceServer build() {
            if (configuration == null || tokenContainer == null || gameSessionManager == null)
                throw new NullPointerException();
            return new GrpcServiceServer(configuration, tokenContainer, gameSessionManager);
        }
    }
}