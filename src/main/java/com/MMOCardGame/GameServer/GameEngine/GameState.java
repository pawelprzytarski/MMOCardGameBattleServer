package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.*;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardCreatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardStatisticsChangeEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import com.MMOCardGame.GameServer.common.Pair;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;
import com.google.common.annotations.VisibleForTesting;
import lombok.*;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@EqualsAndHashCode(exclude = {"triggers"})
@ToString(exclude = {"triggers"})
public class GameState implements Serializable {
    @VisibleForTesting
    @Setter(value = AccessLevel.PROTECTED)
    @Getter
    private Triggers triggers = new Triggers(this);
    @Getter
    private Map<String, Integer> listOfActiveUsers;
    @Getter
    private Map<Integer, CardsField> playersCardsFields = new HashMap<>();
    @Setter
    @Getter
    private Map<Integer, Player> players = new HashMap<>();
    @Getter
    private PhasesStack phasesStack;
    @Getter
    private int[] playersOrder;
    @Getter
    private ReactionStack reactionStack = new ReactionStack();
    private Map<Integer, Map<Integer, Integer>> attackers=new HashMap<>();//playerId, instanceId, transactionId
    @Getter @Setter
    private int playedLinks=0;
    @Getter @Setter
    private int availableCountOfLinksToPlayed = GameConstants.getDefaultAvailableCountOfLinksToPlayed();

    public Map<Integer, Integer> getAttackersForPlayer(int player){
        if(!attackers.containsKey(player))
            attackers.put(player, new HashMap<>());
        return attackers.get(player);
    }

    public GameState() {
        phasesStack = null;
    }

    public GameState(int[] playersOrder) {
        this.playersOrder = playersOrder;
        initPhasesStack(playersOrder);
    }

    public GameState(PhasesStack phasesStack) {
        this.phasesStack = phasesStack;
    }

    public int getRoundPlayer(){
        return phasesStack.getRoundPlayer();
    }

    private void initPhasesStack(int[] playersOrder) {
        phasesStack = new PhasesStack(Arrays.stream(playersOrder).boxed().collect(Collectors.toList()));
    }

    public void setPlayersOrder(int[] playersOrder) {
        this.playersOrder = playersOrder;
        initPhasesStack(playersOrder);
    }

    public Collection<String> getListOfActiveUsers() {
        return listOfActiveUsers.keySet();
    }

    public void setListOfActiveUsers(List<String> listOfActiveUsers) {
        this.listOfActiveUsers = new HashMap<>();
        for (int i = 0; i < listOfActiveUsers.size(); i++) {
            this.listOfActiveUsers.put(listOfActiveUsers.get(i), i);
        }
    }

    public void setUserAsInactive(String user) {
        listOfActiveUsers.remove(user);
    }

    public int getCurrentPlayer() {
        return phasesStack.getCurrentPlayer();
    }

    public GamePhase getCurrentPhase() {
        return phasesStack.getCurrentPhase();
    }

    public void removeCard(int instanceId) {
        playersCardsFields.forEach((integer, cardsField) -> cardsField.removeCard(instanceId));
    }

    public void insertCard(Card card, CardPosition position) {
        if (!playersCardsFields.containsKey(position.getOwner()))
            throw new NotFoundException("Not found user");
        if(position.getPosition()!= CardPosition.Position.Library && position.getPosition()!= CardPosition.Position.Hand)
            card.setRevealed(true);
        addTriggerToCardObservable(card);
        playersCardsFields.get(position.getOwner()).addCard(position.getPosition(), card);
        triggers.invokeTrigger(new CardCreatedEvent(card, position));
        card.setOwner(position.getOwner());
        card.setPosition(position.getPosition());
    }

    public void insertCardRevealed(Card card, CardPosition position) {
        card.setRevealed(true);
        insertCard(card,position);
    }

    public void moveCardToBattlefield(int instanceId) {
        playersCardsFields.forEach((integer, cardsField) -> cardsField.moveCardToBattlefield(instanceId));
    }

    public void moveCardToGraveyard(int instanceId) {
        playersCardsFields.forEach((integer, cardsField) -> cardsField.moveCardToGraveyard(instanceId));
    }

    public void moveCardToHand(int instanceId) {
        playersCardsFields.forEach((integer, cardsField) -> cardsField.moveCardToHand(instanceId));
    }

    public void moveCardToLibrary(int instanceId) {
        playersCardsFields.forEach((integer, cardsField) -> cardsField.moveCardToLibrary(instanceId));
    }

    public Card getCard(int instanceId) {
        for (CardsField cardsField : playersCardsFields.values()) {
            Card card = cardsField.getCard(instanceId);
            if (card != null)
                return card;
        }
        return null;
    }

    public int getCardOwner(int instanceID) {
        CardPosition cardPosition = getCardPosition(instanceID);
        if (cardPosition != null)
            return cardPosition.getOwner();
        return -1;
    }

    public Card getUserCard(int instanceId, int owner) {
        return playersCardsFields.get(owner).getCard(instanceId);
    }

    public CardPosition getCardPosition(int instanceId) {
        for (int player : playersCardsFields.keySet()) {
            CardPosition.Position cardPosition = playersCardsFields.get(player).getCardPosition(instanceId);
            int index = playersCardsFields.get(player).getCardPositionIndex(instanceId);
            if (cardPosition != CardPosition.Position.Void)
                return new CardPosition(player, cardPosition, index);
        }
        return null;
    }

    public void changeCardOwner(int instanceId, int newOwner) {
        CardPosition cardPosition = getCardPosition(instanceId);
        if (cardPosition != null) {
            Card card = playersCardsFields.get(cardPosition.getOwner()).getCard(instanceId);
            card.setOwner(newOwner);
            playersCardsFields.get(cardPosition.getOwner()).removeCard(instanceId);
            playersCardsFields.get(newOwner).addCard(cardPosition.getPosition(), card);
            triggers.invokeTrigger(new CardMovedEvent(card, cardPosition, new CardPosition(newOwner, cardPosition.getPosition(), cardPosition.getIndex())));
        }
    }

    public void endPhase(int playerId) {
        phasesStack.endPhase(playerId);
    }

    public int getPlayerId(String username) {
        return listOfActiveUsers.get(username);
    }

    public String getPlayerName(int id) {
        List<String> result = Arrays.asList((String) null);
        listOfActiveUsers.forEach((s, integer) -> {
            if (integer == id)
                result.set(0, s);
        });
        return result.get(0);
    }

    public void setCardsForUser(int player, Collection<Card> cards) {
        playersCardsFields.put(player, new CardFieldWithTriggersActions(cards, triggers, player));
        cards.forEach(this::addTriggerToCardObservable);
        cards.forEach(card->card.setOwner(player));
    }

    private void addTriggerToCardObservable(Card card) {
        card.getObservable().addObserver(object -> triggers.invokeTrigger(new CardStatisticsChangeEvent(object, card)));
    }

    public void setHeroForUser(int playerId, Player player) {
        players.put(playerId, player);
        player.setTriggers(triggers);
    }

    public Collection<Pair<Card, CardPosition.Position>> getPlayerCardsPositions(int id) {
        return playersCardsFields.get(id).getAllCardsPositions();
    }

    public Collection<Integer> getListOfActiveUsersIds() {
        return listOfActiveUsers.values();
    }

    public CardsField getCardsFieldForPlayer(int player) {
        return playersCardsFields.get(player);
    }

    public Player getPlayer(int player) {
        return players.get(player);
    }

    public void removePlayer(int player) {
        if (!playersCardsFields.containsKey(player)) return;
        players.remove(player);
        unregisterCardsTriggers(player);
        playersCardsFields.remove(player);
        listOfActiveUsers.remove(getPlayerName(player));
    }

    private void unregisterCardsTriggers(int player) {
        playersCardsFields.get(player)
                .getAllCardsPositions()
                .forEach(cardPositionPair ->
                        triggers.unregisterCardScript(cardPositionPair.getFirst().getCardScript()));
    }

    public Set<Card> getAllCards() {
        return playersCardsFields.entrySet().stream()
                .flatMap(integerCardsFieldEntry -> integerCardsFieldEntry
                        .getValue()
                        .getAllCardsPositions()
                        .stream()
                        .map(Pair::getFirst))
                .collect(Collectors.toSet());
    }

    public Set<Pair<Card, CardPosition>> getAllCardsPositions() {
        return playersCardsFields.entrySet().stream()
                .flatMap(integerCardsFieldEntry -> integerCardsFieldEntry
                        .getValue()
                        .getAllCardsPositions()
                        .stream()
                        .map(cardPositionPair -> new Pair<>(cardPositionPair.getFirst(), new CardPosition(integerCardsFieldEntry.getKey(),
                                cardPositionPair.getSecond(), getCardPositionIndex(cardPositionPair.getFirst().getInstanceId())))))
                .collect(Collectors.toSet());
    }

    private int getCardPositionIndex(int instanceId) {
        AtomicInteger index = new AtomicInteger(-1);
        playersCardsFields.forEach((integer, cardsField) -> {
            int cardFieldIndex = cardsField.getCardPositionIndex(instanceId);
            if (cardFieldIndex >= 0)
                index.set(cardFieldIndex);
        });
        return index.get();
    }

    public void revealCard(int instanceId) {
        for (CardsField cardsField : playersCardsFields.values()) {
            cardsField.revealCard(instanceId);
        }
    }
}
