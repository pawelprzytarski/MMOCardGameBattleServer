package com.MMOCardGame.GameServer.GameEngine.Enums;

public enum GamePhase {
    UndefinedPhase,
    Waiting,
    FirstHandDraw,
    PlayerRoundPhasesStart,//for internal use
    Untap,
    Main,
    Attack,
    Defence,
    EndRound,
    PlayerRoundPhasesEnd,//for internal use
    ReactionPhase,
    TriggerPhase,
    AttackExecution,
}

