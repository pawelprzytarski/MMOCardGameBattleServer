package com.MMOCardGame.GameServer.GameEngine.Enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(exclude = "index")
@AllArgsConstructor
public class CardPosition implements Serializable {
    int owner;
    Position position;
    int index;

    public CardPosition(int owner, Position position) {
        this.owner = owner;
        this.position = position;
    }

    public enum Position {
        UndefinedPosition,
        Library,
        Hand,
        Battlefield,
        Graveyard,
        Void,
    }
}
