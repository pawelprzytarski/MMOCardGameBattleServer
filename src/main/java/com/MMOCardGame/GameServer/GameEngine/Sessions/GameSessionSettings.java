package com.MMOCardGame.GameServer.GameEngine.Sessions;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class GameSessionSettings {
    private List<String> listOfUsers;
    private Map<String, Integer> selectedHeros = new HashMap<>();
    private Map<String, List<Integer>> usersCards = new HashMap<>();
    private int typeOfMatch;
    private int sessionId;

    public List<String> getListOfUsers() {
        return listOfUsers;
    }

    public void setCardsForUser(String username, List<Integer> cardIds) {
        usersCards.put(username, cardIds);
    }

    public void setSelectedHerosForUser(String username, int heroId) {
        selectedHeros.put(username, heroId);
    }

    public final class MatchType {
        public final static int Single = 1;
        public final static int Team = 2;
    }
}
