package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameSessionDump implements Serializable {
    private Date dumpDate = Date.from(Instant.now());
    private GameState gameState;
    private Set<String> connectedUsers;
    private String reason;
}
