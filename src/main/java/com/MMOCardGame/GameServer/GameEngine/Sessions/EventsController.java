package com.MMOCardGame.GameServer.GameEngine.Sessions;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GamePhaseChanged;
import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.ReactionStackController;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.ProtoMessagesHelper;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.*;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.*;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

class EventsController {
    Logger logger = LoggerFactory.getLogger(EventsController.class);
    private GameSession gameSession;
    private GameState gameState;
    private Set<UserConnection> activeConnections;
    @Getter
    private ReactionStackController reactionStackController;
    private boolean executingTriggerScript = false;
    private AttacksExecutor attacksExecutor;

    public EventsController(GameState gameState, Set<UserConnection> activeConnections, GameSession gameSession) {
        this.gameState = gameState;
        this.activeConnections = activeConnections;
        this.gameSession = gameSession;
        reactionStackController = new ReactionStackController(gameState, activeConnections);
        attacksExecutor = new AttacksExecutor(gameState, activeConnections);
    }

    public EventsController(GameSession gameSession, GameState gameState, Set<UserConnection> activeConnections, ReactionStackController reactionStackController, AttacksExecutor attacksExecutor) {
        this.gameSession = gameSession;
        this.gameState = gameState;
        this.activeConnections = activeConnections;
        this.reactionStackController = reactionStackController;
        this.attacksExecutor = attacksExecutor;
    }

    public void setObserversOnGameSessionObjects() {
        gameState.getPhasesStack().addObserverAtBegin(this::endPhaseEvents);
        gameState.getTriggers().registerSimpleScript((CardMovedScript) this::doCardMoveActions);
        gameState.getTriggers().registerSimpleScript((PlayerLifeChangedScript) this::playerLifeChange);
        gameState.getTriggers().registerSimpleScript((PlayerDefeatedScript) this::defeatPlayer);
        gameState.getTriggers().registerSimpleScript((CardRevealedScript) this::cardRevealed);
        gameState.getTriggers().registerSimpleScript((CardStatisticsChangeScript) this::cardStatisticsChange);
        gameState.getTriggers().registerSimpleScript((CardCreatedScript) this::cardCreated);
        gameState.getTriggers().registerSimpleScript((PlayerLinkPointsChangedScript) this::linkPointsChanged);
        gameState.getTriggers().getObservable().addObserver(this::executeTriggerScripts);
    }

    private void linkPointsChanged(PlayerLinkPointsChangedEvent event) {
        PlayerLinkPointsUpdated playerLinkPointsUpdated = PlayerLinkPointsUpdated.newBuilder()
                .setPlayer(event.getPlayerId())
                .setPoints(CardCosts.newBuilder()
                        .setWhiteHats(event.getNewPoints().getWhiteHats())
                        .setScanners(event.getNewPoints().getScanners())
                        .setPsychos(event.getNewPoints().getPsychotronnics())
                        .setRaiders(event.getNewPoints().getRaiders())
                        .setNeutral(event.getNewPoints().getNeutral())
                        .build())
                .build();
        sendToAllActiveConnections(playerLinkPointsUpdated);
    }

    private void cardCreated(CardCreatedEvent event) {
        sendCardCreatedMessageToAll(event);
        sendCardRevealAfterCardCreateIfNeeded(event);
    }

    private void sendCardCreatedMessageToAll(CardCreatedEvent event) {
        CardCreated cardCreated = CardCreated.newBuilder()
                .setCardId(event.getCard().getId())
                .setInstanceId(event.getCard().getInstanceId())
                .setCardPositionValue(event.getCardPosition().getPosition().ordinal())
                .setOwner(event.getCardPosition().getOwner())
                .build();
        sendToAllActiveConnections(cardCreated);
    }

    private void sendCardRevealAfterCardCreateIfNeeded(CardCreatedEvent event) {
        if (event.getCard().isRevealed()) {
            var revealMessage = ProtoMessagesHelper.getCardRevealedMessageFromCardAndPosition(event.getCard(), event.getCardPosition());
            sendToAllActiveConnections(revealMessage);
        } else if (event.getCardPosition().getPosition().equals(CardPosition.Position.Hand)) {
            var revealMessage = ProtoMessagesHelper.getCardRevealedMessageFromCardAndPosition(event.getCard(), event.getCardPosition());
            sendToConnection(event.getCardPosition().getOwner(), revealMessage);
        }
    }

    public void executeTriggerScripts(Boolean ignored) {
        if (gameState.getCurrentPhase().equals(GamePhase.TriggerPhase) && !executingTriggerScript) {
            executingTriggerScript = true;
            runTriggerActions();
            executingTriggerScript = false;
        }
    }

    private void runTriggerActions() {
        int codedCardId = gameState.getCurrentPlayer();
        CardScript cardScript = getTriggerCardScript();
        var triggerType = gameState.getPhasesStack().getDecodedTriggerType();
        notifyPlayersAboutTriggerAction();
        if (!cardScript.isTransactionNeeded(triggerType)) {
            startTriggerActionWithoutTransaction(cardScript, triggerType, codedCardId);
        } else {
            runTriggerTransaction(cardScript, triggerType);
        }
    }

    private void notifyPlayersAboutTriggerAction() {
        var triggerActivatedMessage = TriggerActivated.newBuilder()
                .setCardInstanceId(gameState.getPhasesStack().getDecodedCardId())
                .setTriggerTypeValue(gameState.getPhasesStack().getDecodedTriggerType().ordinal())
                .setStackId(-1)
                .build();
        sendToAllActiveConnections(triggerActivatedMessage);
    }

    private CardScript getTriggerCardScript() {
        int cardId = gameState.getPhasesStack().getDecodedCardId();
        return gameState.getCard(cardId).getCardScript();
    }

    private void startTriggerActionWithoutTransaction(CardScript cardScript, TriggerType triggerType, int codedCardId) {
        cardScript.startTriggerActions(triggerType, -1);
        gameState.endPhase(codedCardId);
    }

    private void runTriggerTransaction(CardScript cardScript, TriggerType triggerType) {
        TargetSelectingTransaction transactionResponse = getTransactionStepForPlayer(cardScript, triggerType);
        sendTransactionToPlayer(cardScript, triggerType, transactionResponse);
    }

    private void sendTransactionToPlayer(CardScript cardScript, TriggerType triggerType, TargetSelectingTransaction transactionResponse) {
        int playerId = cardScript.getTriggerTransactionPlayer(triggerType);
        activeConnections.forEach(userConnection -> {
            if (userConnection.getUserId() == playerId)
                userConnection.sendMessage(transactionResponse);
        });
    }

    private TargetSelectingTransaction getTransactionStepForPlayer(CardScript cardScript, TriggerType triggerType) {
        var targetSelectingTransaction = TargetSelectingTransaction.newBuilder()
                .setType(TargetSelectingTransactionType.StartTransaction)
                .setTransactionId(100 + triggerType.ordinal())//TODO add correct value for transaction type
                .build();
        return cardScript.runTransactionAction(targetSelectingTransaction, gameState);
    }

    private void cardStatisticsChange(CardStatisticsChangeEvent cardStatisticsChangeEvent) {
        notifyAllAboutCardStatisticsChange(cardStatisticsChangeEvent);
        invokeCardDeathActionIfNeeded(cardStatisticsChangeEvent);
    }

    private void invokeCardDeathActionIfNeeded(CardStatisticsChangeEvent cardStatisticsChangeEvent) {
        if (cardStatisticsChangeEvent.cardStatistics.getDefence() <= 0 && cardStatisticsChangeEvent.card.getType() == CardType.Unit)
            cardStatisticsChangeEvent.card.getDefeatAction().run(cardStatisticsChangeEvent.getGameState());
    }

    private void notifyAllAboutCardStatisticsChange(CardStatisticsChangeEvent event) {
        var message = ProtoMessagesHelper.getCardStatisticsChangeMessage(event.cardStatistics, event.getGameState().getCardPosition(event.card.getInstanceId()));
        sendToAllActiveConnections(message);
    }

    private void cardRevealed(CardRevealedEvent cardRevealedEvent) {
        CardRevealed cardRevealed = ProtoMessagesHelper.getCardRevealedMessageFromEvent(cardRevealedEvent);
        sendToAllActiveConnections(cardRevealed);
    }

    private void doCardMoveActions(CardMovedEvent event) {
        revealCardForOwnerIfCardMovedToHand(event);
        notifyAllAboutCardMove(event);
    }

    private void revealCardForOwnerIfCardMovedToHand(CardMovedEvent event) {
        if (event.getOldPosition().getPosition() == CardPosition.Position.Library
                && event.getNewPosition().getPosition() == CardPosition.Position.Hand) {
            CardRevealed cardMoved = ProtoMessagesHelper.getCardRevealedMessageFromCardAndPosition(event.getMovedCard(), event.getNewPosition());
            sendToConnection(event.getNewPosition().getOwner(), cardMoved);
        }
    }

    private <U> void sendToConnection(int owner, U message) {
        activeConnections.stream()
                .filter(userConnection -> userConnection.getUserId() == owner)
                .forEach(userConnection -> userConnection.sendMessage(message));
    }

    private void notifyAllAboutCardMove(CardMovedEvent event) {
        CardMoved cardMoved = ProtoMessagesHelper.getCardMovedMessageFromEvent(event);
        sendToAllActiveConnections(cardMoved);
    }

    private void endPhaseEvents(GamePhaseChanged event) {
        if (shouldDoReactionStackAction(event))
            event = doReactionStackActionAndChangeEventIfNeeded(event);
        sendPhaseChangedMessageIfNeeded(event);
        invokeTriggers(event);
        invokeAttackActionIfNeeded(event);
        doEndMainPhaseActions(event);

        if(event.getCurrent().getFirst()==GamePhase.Untap){
            final GamePhaseChanged finalEvent = event;
            gameState.getAllCardsPositions().forEach(cardCardPositionPair -> {
                if(cardCardPositionPair.getSecond().getPosition() == CardPosition.Position.Battlefield
                    && cardCardPositionPair.getFirst().getType() == CardType.Unit) {
                    int overloadPoints = cardCardPositionPair.getFirst().getOverloadPoints();
                    cardCardPositionPair.getFirst().resetStatiticsToBase();
                    if(cardCardPositionPair.getSecond().getOwner() != finalEvent.getCurrent().getSecond())
                        cardCardPositionPair.getFirst().setOverloadPoints(overloadPoints);
                }
            });
        }
    }

    private void doEndMainPhaseActions(GamePhaseChanged event) {
        if (isMainPhase(event.getCurrent().getFirst())) {
            gameState.setPlayedLinks(0);
        }
        if(isMainPhase(event.getPrevious().getFirst()) && isMainPhase(event.getCurrent().getFirst()))
            resetLinksPoints();
    }

    private void resetLinksPoints() {
        gameState.getPlayers().forEach((integer, player) -> player.getLinksPoints().reset());
    }

    private void invokeAttackActionIfNeeded(GamePhaseChanged event) {
        if (event.getCurrent().getFirst().equals(GamePhase.AttackExecution)) {
            attacksExecutor.prepareAttacks(event.getCurrent().getSecond());

            attacksExecutor.executeAttacksWhileInPhase();
            attacksExecutor.endPhaseIfNoMoreAttacks();
        }
        if (event.getCurrent().getFirst().equals(GamePhase.EndRound)) {
            attacksExecutor.cleanGameState();
        }
    }

    private void invokeTriggers(GamePhaseChanged event) {
        gameState.getTriggers().invokeTrigger(new PhaseEndedEvent(event.getPrevious(), event.getCurrent()));
        invokeMainPhaseChangedTrigger(event);
    }

    private void invokeMainPhaseChangedTrigger(GamePhaseChanged event) {
        if (isMainPhase(event.getCurrent().getFirst())
                && isMainPhase(event.getPrevious().getFirst())) {
            gameState.getTriggers().invokeTrigger(new MainPhaseEndedEvent(event.getPrevious(), event.getCurrent()));
        }
    }

    private void sendPhaseChangedMessageIfNeeded(GamePhaseChanged event) {
        if (event.getCurrent().getFirst() == GamePhase.TriggerPhase && event.getPrevious().getFirst() != GamePhase.TriggerPhase) {
            sendInfoAboutStartTriggerPhases(event);
        } else if (event.getCurrent().getFirst() != GamePhase.TriggerPhase && event.getPrevious().getFirst() == GamePhase.TriggerPhase) {
            sendInfoAboutStopTriggerPhases(event);
        } else if (event.getCurrent().getFirst() != GamePhase.TriggerPhase) {
            PhaseChanged phaseChanged = ProtoMessagesHelper.getPhaseChangedFromGamePhaseChanged(event);
            sendToAllActiveConnections(phaseChanged);
        }
    }

    private void sendInfoAboutStartTriggerPhases(GamePhaseChanged event) {
        PhaseChanged phaseChanged = PhaseChanged.newBuilder()
                .setOldPlayer(event.getPrevious().getSecond())
                .setOldPhaseValue(event.getPrevious().getFirst().ordinal())
                .setNewPlayer(-1)
                .setNewPhaseValue(GamePhase.TriggerPhase.ordinal())
                .build();
        sendToAllActiveConnections(phaseChanged);
    }

    private void sendInfoAboutStopTriggerPhases(GamePhaseChanged event) {
        PhaseChanged phaseChanged = PhaseChanged.newBuilder()
                .setNewPlayer(event.getCurrent().getSecond())
                .setNewPhaseValue(event.getCurrent().getFirst().ordinal())
                .setOldPlayer(-1)
                .setOldPhaseValue(GamePhase.TriggerPhase.ordinal())
                .build();
        sendToAllActiveConnections(phaseChanged);
    }

    private GamePhaseChanged doReactionStackActionAndChangeEventIfNeeded(GamePhaseChanged event) {
        reactionStackController.doAction();
        if (gameState.getReactionStack().getNextAction() != null) {
            startNewSessionOfReactionsAndChangeEvent(event);
        }
        return event;
    }

    private void startNewSessionOfReactionsAndChangeEvent(GamePhaseChanged event) {
        int startingPlayer = gameState.getReactionStack().getNextAction().getOwnerPlayer();
        gameState.getPhasesStack().startReactionPhasesWithStartingPlayerWithoutNotify(startingPlayer);
        event.setCurrent(new Pair<>(GamePhase.ReactionPhase, startingPlayer));
    }

    private boolean shouldDoReactionStackAction(GamePhaseChanged event) {
        return event.getCurrent().getFirst() != GamePhase.ReactionPhase && event.getPrevious().getFirst() == GamePhase.ReactionPhase;
    }

    private boolean isMainPhase(GamePhase phase) {
        return phase != GamePhase.ReactionPhase && phase != GamePhase.AttackExecution
                && phase != GamePhase.TriggerPhase;
    }

    private <U> void sendToAllActiveConnections(U message) {
        activeConnections.forEach(userConnection -> userConnection.sendMessage(message));
    }

    private void playerLifeChange(PlayerLifeChangedEvent event) {
        notifyAllAboutLifeChange(event);
    }

    private void notifyAllAboutLifeChange(PlayerLifeChangedEvent event) {
        sendToAllActiveConnections(PlayerDefenceChanged.newBuilder()
                .setPlayer(event.getPlayer())
                .setNewDefenceValue(event.getNewLife())
                .build());
    }

    public void defeatPlayer(PlayerDefeatedEvent event) {
        gameState.getPhasesStack().removePlayer(event.getPlayer());
        try {
            removeDefeatedPlayerFromSession(event.getPlayer());
            checkRemainingPlayersWon();
        } catch (Throwable e) {
            logger.error("Defeat player exception: ", e);
        }
    }

    public void removeDefeatedPlayerFromSession(int player) {
        notifyAllAboutPlayerGameEnd(player, GameResult.Defeated);
        gameState.removePlayer(player);
        removePlayerConnection(player, GameResult.Defeated);
    }

    private void removePlayerConnection(int player, GameResult gameResult) {
        UserSessionEndInfo.Result result = gameResult == GameResult.Defeated ? UserSessionEndInfo.Result.LOST : UserSessionEndInfo.Result.WIN;
        activeConnections.stream()
                .filter(userConnection -> userConnection.getUserId() == player)
                .forEach(connection -> gameSession.removeUserFromSession(connection, result, gameResult.toString()));
    }

    private void notifyAllAboutPlayerGameEnd(int player, GameResult gameResult) {
        sendToAllActiveConnections(PlayerGameEnd.newBuilder()
                .setPlayer(player)
                .setResult(gameResult)
                .build());
    }

    private void checkRemainingPlayersWon() {
        if (gameState.getListOfActiveUsersIds().size() == 1) {
            Set<Integer> players = new HashSet<>(gameState.getListOfActiveUsersIds());
            players.forEach(player -> {
                notifyAllAboutPlayerGameEnd(player, GameResult.Won);
                removePlayerConnection(player, GameResult.Won);
            });
        }
    }
}
