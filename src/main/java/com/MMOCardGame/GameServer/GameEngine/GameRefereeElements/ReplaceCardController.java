package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.HandChanged;
import com.MMOCardGame.GameServer.Servers.UserConnection;

import java.util.List;
import java.util.Map;

public class ReplaceCardController extends FirstHandChangeReferee {

    public ReplaceCardController(GameState gameState, Map<Integer, UserConnection> connections, RandomCardsPicker randomCardsPicker) {
        super(gameState, connections, randomCardsPicker);
    }

    public void prepareReplaceCards(HandChanged handChange) {
        checkCanReplaceCards(handChange);
        player = handChange.getPlayer();
        changeCardsInHandAndGetNewCards(handChange);
    }

    private void changeCardsInHandAndGetNewCards(HandChanged handChange) {
        removedCards = handChange.getCardInstancesList();
        moveCardsToLibrary(handChange.getCardInstancesList());
        sendInfoAboutRemove();
        selectedCards = moveNewCardsToHandAndGetThem(removedCards.size());
        sendInfoAboutDraw();
    }

    private void checkCanReplaceCards(HandChanged handChange) {
        checkPlayerCanReplaceCards(handChange);
        checkAllCardsToReplaceAreInHand(handChange);
        acceptPlayerHand(handChange);
    }

    private void checkPlayerCanReplaceCards(HandChanged handChange) {
        if (gameState.getPlayer(handChange.getPlayer()).getMuligansCount() != 0
                || gameState.getPlayer(handChange.getPlayer()).isAcceptedHand())
            throw new InconsistentPlayerCommand();
    }

    private void checkAllCardsToReplaceAreInHand(HandChanged handChange) {
        if (handChange.getCardInstancesList().size() == 0)
            throw new BadRequestException();
        handChange.getCardInstancesList().forEach(integer -> {
            CardPosition position = gameState.getCardPosition(integer);
            if (position == null || position.getPosition() != CardPosition.Position.Hand
                    || position.getOwner() != handChange.getPlayer())
                throw new BadRequestException();
        });
    }
}
