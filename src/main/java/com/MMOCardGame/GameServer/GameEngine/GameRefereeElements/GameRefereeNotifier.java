package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.UserConnection;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class GameRefereeNotifier implements Serializable {
    protected final transient GameState gameState;
    protected final transient Map<Integer, UserConnection> connectionsMap;

    public GameRefereeNotifier(GameState gameState, Collection<UserConnection> connections) {
        this.gameState = gameState;
        this.connectionsMap = connections.stream().collect(Collectors.toMap(UserConnection::getUserId, o -> o));
    }

    public GameRefereeNotifier(GameState gameState, Map<Integer, UserConnection> connections) {
        this.gameState = gameState;
        this.connectionsMap = connections;
    }

    public void addConnection(UserConnection userConnection) {
        connectionsMap.put(userConnection.getUserId(), userConnection);
    }

    protected <U> void sendMessageToAllConnections(U message) {
        connectionsMap.forEach((integer, userConnection) -> userConnection.sendMessage(message));
    }

    protected <U> void sendMessageToPlayerAndOthers(int player, U messageToPlayer, U messageToOthers) {
        connectionsMap.forEach((playerId, userConnection) -> {
            if (playerId == player)
                userConnection.sendMessage(messageToPlayer);
            else userConnection.sendMessage(messageToOthers);
        });
    }

    protected <U> void sendMessageToPlayer(int player, U messageToPlayer) {
        UserConnection connection = connectionsMap.get(player);
        if (connection != null)
            connection.sendMessage(messageToPlayer);
    }

    protected <U> void sendMessageToAllPlayersExcept(int player, U messageToOthers) {
        connectionsMap.forEach((playerId, userConnection) -> {
            if (playerId != player)
                userConnection.sendMessage(messageToOthers);
        });
    }
}
