package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameConstants;
import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Sessions.exceptions.InconsistentPlayerCommand;
import com.MMOCardGame.GameServer.Servers.UserConnection;

import java.util.Map;
import java.util.stream.Collectors;

public class MuliganController extends FirstHandChangeReferee {

    public MuliganController(GameState gameState, Map<Integer, UserConnection> connections, RandomCardsPicker randomCardsPicker) {
        super(gameState, connections, randomCardsPicker);
    }

    public void prepareMuligan(int player) {
        checkCanDoMuligan(player);
        this.player = player;
        replaceAllCardsInHandAndGetNewCards(player);
    }

    private void replaceAllCardsInHandAndGetNewCards(int player) {
        removedCards = moveAllCardsToLibrary(player).stream().map(Card::getInstanceId).collect(Collectors.toList());
        sendInfoAboutRemove();
        selectedCards =  moveNewCardsToHandAndGetThem(getCardsCountForMuligan());
        sendInfoAboutDraw();
    }

    private int getCardsCountForMuligan() {
        return GameConstants.getMaxMuligansCount()
                - gameState.getPlayer(player).getMuligansCount();
    }

    private void checkCanDoMuligan(int player) {
        if (gameState.getPlayer(player).getMuligansCount() == GameConstants.getMaxMuligansCount())
            throw new InconsistentPlayerCommand();
        gameState.getPlayer(player).doMuligan();
    }

    public void endPhaseIfLastMuligan() {
        if (GameConstants.getMaxMuligansCount() == gameState.getPlayer(player).getMuligansCount()) {
            gameState.endPhase(player);
            gameState.getPlayers().get(player).acceptHand();
        }
    }
}
