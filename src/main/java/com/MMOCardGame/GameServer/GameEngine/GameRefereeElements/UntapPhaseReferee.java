package com.MMOCardGame.GameServer.GameEngine.GameRefereeElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GamePhaseChanged;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.ProtoMessagesHelper;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.DrawCardEvent;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardChangeType;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.Setter;

import java.util.Collections;
import java.util.Map;

public class UntapPhaseReferee extends GameRefereeNotifier {
    @Setter
    RandomCardsPicker randomCardsPicker;
    private int playerId;

    public UntapPhaseReferee(GameState gameState, Map<Integer, UserConnection> connections, RandomCardsPicker randomCardsPicker) {
        super(gameState, connections);
        this.randomCardsPicker = randomCardsPicker;
        gameState.getPhasesStack().addObserver(this::untapStarted);
    }

    private void untapStarted(GamePhaseChanged gamePhaseChanged) {
        if (gamePhaseChanged.getCurrent().getFirst() == GamePhase.Untap && gamePhaseChanged.getPrevious().getFirst() == GamePhase.EndRound) {
            playerId = gamePhaseChanged.getCurrent().getSecond();
            doUntapPhaseTasks();
        }
    }

    private void doUntapPhaseTasks() {
        untapCards();
        int selectedCard = moveDrawCardToHandAndGetIfPossible();
        if (selectedCard == -1)
            killPlayer();
        else notifyAboutDrawAndInvokeTrigger(selectedCard);
    }

    private void killPlayer() {
        var player = gameState.getPlayer(this.playerId);
        player.changeLife(-player.getLife());
    }

    private void notifyAboutDrawAndInvokeTrigger(int selectedCard) {
        notifyAllAboutDraw(selectedCard);
        invokeTrigger(selectedCard);
    }

    private void invokeTrigger(int selectedCard) {
        gameState.getTriggers().invokeTrigger(new DrawCardEvent(gameState.getCard(selectedCard)));
    }

    private void notifyAllAboutDraw(int selectedCard) {
        //var playerMessage = ProtoMessagesHelper.getHandChangedDrawFromCards(Collections.singletonList(selectedCard), playerId, CardChangeType.Draw);
        //var othersMessage = ProtoMessagesHelper.getHandChangedDrawWithSize(1, playerId, CardChangeType.Draw);
        //sendMessageToAllPlayersExcept(playerId, othersMessage);
        //sendMessageToPlayer(playerId, playerMessage);

    }

    private int moveDrawCardToHandAndGetIfPossible() {
        Card card = gameState.getCardsFieldForPlayer(playerId).getFirstCardAtPosition(CardPosition.Position.Library);
        if (card == null)
            return -1;
        int selectedCard = card.getInstanceId();
        gameState.moveCardToHand(selectedCard);
        return selectedCard;
    }

    private void untapCards() {
        gameState.getCardsFieldForPlayer(playerId).getAllCardsPositions()
                .stream().filter(cardPositionPair -> cardPositionPair.getSecond() == CardPosition.Position.Battlefield)
                .forEach(this::notifyAllAboutCardUntap);
    }

    private void notifyAllAboutCardUntap(Pair<Card, CardPosition.Position> cardPositionPair) {
        Card card = cardPositionPair.getFirst();
        if (card.getOverloadPoints() != 0) {
            card.decrementOverloadPoints();
        }
    }
}
