package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GamePhaseChanged {
    Pair<GamePhase, Integer> previous;
    Pair<GamePhase, Integer> current;
}
