package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.DrawCardEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface DrawCardScript {
    void drawCard(DrawCardEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.DrawCard;
    }
}
