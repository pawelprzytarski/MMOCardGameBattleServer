package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface CardMovedScript {
    void cardMoved(CardMovedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.CardMoved;
    }
}
