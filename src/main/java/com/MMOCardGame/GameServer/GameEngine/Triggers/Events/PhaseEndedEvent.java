package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PhaseEndedEvent extends TriggerEvent {
    int oldPlayer;
    GamePhase oldPhase;
    int newPlayer;
    GamePhase newPhase;

    public PhaseEndedEvent(GamePhase oldPhase, int oldPlayer, GamePhase newPhase, int newPlayer) {
        this.oldPlayer = oldPlayer;
        this.oldPhase = oldPhase;
        this.newPlayer = newPlayer;
        this.newPhase = newPhase;
    }

    public PhaseEndedEvent(Pair<GamePhase, Integer> oldPhase, Pair<GamePhase, Integer> newPhase) {
        this.oldPlayer = oldPhase.getSecond();
        this.oldPhase = oldPhase.getFirst();
        this.newPlayer = newPhase.getSecond();
        this.newPhase = newPhase.getFirst();
    }
}


