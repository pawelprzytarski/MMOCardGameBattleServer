package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CardMovedEvent extends TriggerEvent {
    Card movedCard;
    CardPosition oldPosition;
    CardPosition newPosition;

    public CardMovedEvent(Card movedCard, CardPosition oldPosition, CardPosition newPosition) {
        this.movedCard = movedCard;
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
    }

    public CardMovedEvent(Card card, CardPosition.Position oldPosition, CardPosition.Position newPosition) {
        this.movedCard = card;
        this.oldPosition = new CardPosition(-1, oldPosition, -1);
        this.newPosition = new CardPosition(-1, newPosition, -1);
    }

    @Override
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
        int owner = gameState.getCardOwner(movedCard.getInstanceId());
        oldPosition.setOwner(owner);
        newPosition.setOwner(owner);
    }
}
