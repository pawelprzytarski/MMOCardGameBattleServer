package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class CardRevealedEvent extends TriggerEvent {
    Card card;
    CardPosition position;
}
