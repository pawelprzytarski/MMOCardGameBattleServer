package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardStatisticsChangeEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface CardStatisticsChangeScript {
    void cardStatisticsChanged(CardStatisticsChangeEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.CardStatisticsChanged;
    }
}
