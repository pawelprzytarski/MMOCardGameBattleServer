package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CardCreatedEvent extends TriggerEvent {
    private Card card;
    private CardPosition cardPosition;

    public CardCreatedEvent(Card card, CardPosition cardPosition) {
        this.card = card;
        this.cardPosition = cardPosition;
    }
}
