package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerLifeChangedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface PlayerLifeChangedScript {
    void playerLifeChanged(PlayerLifeChangedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.PlayerLifeChanged;
    }
}
