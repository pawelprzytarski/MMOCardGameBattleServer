package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerDefeatedEvent extends TriggerEvent {
    int player;
}
