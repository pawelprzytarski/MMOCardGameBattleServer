package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PlayerLinkPointsChangedEvent extends TriggerEvent {
    int playerId;
    CardInfoReader.CardInfo.Costs oldPoints;
    CardInfoReader.CardInfo.Costs newPoints;
}
