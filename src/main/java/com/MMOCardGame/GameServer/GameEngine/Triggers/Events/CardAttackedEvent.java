package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CardAttackedEvent extends TriggerEvent {
    Object attackedBy;
    Card attackedCard;

    public CardAttackedEvent(Object attackedBy, Card attackedCard) {
        this.attackedBy = attackedBy;
        this.attackedCard = attackedCard;
    }
}
