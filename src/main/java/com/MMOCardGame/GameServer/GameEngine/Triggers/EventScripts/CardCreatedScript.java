package com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardCreatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.TriggerType;

@TriggerScript
public interface CardCreatedScript {
    void cardCreated(CardCreatedEvent event);

    default TriggerType getTriggerType() {
        return TriggerType.CardCreated;
    }
}
