package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.common.Pair;

public class MainPhaseEndedEvent extends PhaseEndedEvent {
    public MainPhaseEndedEvent(GamePhase oldPhase, int oldPlayer, GamePhase newPhase, int newPlayer) {
        super(oldPhase, oldPlayer, newPhase, newPlayer);
    }

    public MainPhaseEndedEvent(Pair<GamePhase, Integer> oldPhase, Pair<GamePhase, Integer> newPhase) {
        super(oldPhase, newPhase);
    }
}
