package com.MMOCardGame.GameServer.GameEngine.Triggers.Events;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.StatisticsChange;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class CardStatisticsChangeEvent extends TriggerEvent {
    public StatisticsChange cardStatistics;
    public Card card;

    public CardStatisticsChangeEvent(StatisticsChange statisticsChange, Card card) {
        this.cardStatistics = statisticsChange;
        this.card = card;
    }
}
