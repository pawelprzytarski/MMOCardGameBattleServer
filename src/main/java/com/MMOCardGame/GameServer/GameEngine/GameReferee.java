package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.*;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.UseCardController;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardUsed;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.HandChanged;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
@ToString
public class GameReferee extends GameRefereeNotifier {
    private FirstHandPickerReferee firstHandPicker;
    private UntapPhaseReferee untapPhaseReferee;
    private RandomCardsPicker randomCardsPicker = new RandomCardsPicker();
    @Setter
    private ReactionStackController reactionStackController = null;

    public GameReferee(GameSession gameSession) {
        this(gameSession.getAllGameState(), gameSession.getActiveConnections());

    }

    public GameReferee(GameState gameState, Collection<UserConnection> connections) {
        super(gameState, connections);
        firstHandPicker = new FirstHandPickerReferee(gameState, connections);
        untapPhaseReferee = new UntapPhaseReferee(gameState, connectionsMap, randomCardsPicker);
    }

    @Override
    public void addConnection(UserConnection userConnection) {
        super.addConnection(userConnection);
        firstHandPicker.addConnection(userConnection);
        untapPhaseReferee.addConnection(userConnection);
    }

    public void setRandomCardsPicker(RandomCardsPicker randomCardsPicker) {
        this.randomCardsPicker = randomCardsPicker;
        firstHandPicker.setRandomCardsPicker(randomCardsPicker);
        untapPhaseReferee.setRandomCardsPicker(randomCardsPicker);
    }

    public void muligan(int player) {
        var muliganController = new MuliganController(gameState, connectionsMap, randomCardsPicker);
        muliganController.prepareMuligan(player);
        muliganController.endPhaseIfLastMuligan();
    }

    public void replaceCards(HandChanged handChange) {
        ReplaceCardController replaceCardController = new ReplaceCardController(gameState, connectionsMap, randomCardsPicker);
        replaceCardController.prepareReplaceCards(handChange);
    }

    public CardUsed useCardIfPossible(CardUsed message, int player) {
        if (reactionStackController != null) {
            UseCardController useCardController = new UseCardController(gameState, reactionStackController);
            useCardController.prepareUseCardAction(message, player);
            useCardController.addReactionStackElementAndNotifyPlayers();
            useCardController.invokeRelatedActions();
            return useCardController.getResult();
        }
        return null;
    }
}
