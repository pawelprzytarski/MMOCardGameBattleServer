package com.MMOCardGame.GameServer.GameEngine.factories;

import dagger.Component;
import dagger.Module;
import dagger.Provides;

@Component(modules = {GameSessionManagerProvider.class})
public interface GameSessionManagerFactory {
    GameSessionManagerBuilder getGameSessionsManagerBuilder();
}

@Module(includes = {GameStateBuilderProvider.class, GameRefereeBuilderProvider.class})
class GameSessionManagerProvider {
    @Provides
    GameSessionManagerBuilder getGameSessionsManager(GameStateBuilder gameStateBuilder, GameRefereeBuilder gameRefereeBuilder) {
        return new BasicGameSessionManagerBuilder().setGameSessionFactory(new GameSessionFactory()
                .setGameRefereeBuilder(gameRefereeBuilder)
                .setGameStateBuilder(gameStateBuilder));
    }
}