package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.GameReferee;
import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.RandomCardsPicker;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;

public class GameRefereeBuilder {
    private GameSession gameSession;
    private GameSessionSettings gameSettings;
    private RandomCardsPicker randomCardsPicker = new RandomCardsPicker();

    public static GameRefereeBuilder newBuilder() {
        return new GameRefereeBuilder();
    }

    public GameRefereeBuilder setGameSession(GameSession gameSession) {
        this.gameSession = gameSession;
        return this;
    }

    public GameRefereeBuilder setGameSettings(GameSessionSettings gameSettings) {
        this.gameSettings = gameSettings;
        return this;
    }

    public GameRefereeBuilder setRandomCardPicker(RandomCardsPicker randomCardPicker) {
        this.randomCardsPicker = randomCardPicker;
        return this;
    }

    public GameReferee build() {
        GameReferee gameReferee = new GameReferee(gameSession);
        gameReferee.setRandomCardsPicker(randomCardsPicker);
        return gameReferee;
    }
}
