package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Sessions.*;
import com.MMOCardGame.GameServer.common.ObservableNotifier;

public class GameSessionFactory {
    GameStateBuilder gameStateBuilder;
    private GameRefereeBuilder gameRefereeBuilder;

    public GameSessionFactory setGameStateBuilder(GameStateBuilder gameStateBuilder) {
        this.gameStateBuilder = gameStateBuilder;
        return this;
    }

    public GameSessionFactory setGameRefereeBuilder(GameRefereeBuilder gameRefereeBuilder) {
        this.gameRefereeBuilder = gameRefereeBuilder;
        return this;
    }

    public GameSession create(GameSessionSettings gameSessionSettings, ObservableNotifier<UserSessionEndInfo> sessionEndObservable, GameSessionManager gameSessionManager) {
        return new GameSessionController(gameSessionSettings, sessionEndObservable, gameSessionManager, gameStateBuilder, gameRefereeBuilder);
    }
}
