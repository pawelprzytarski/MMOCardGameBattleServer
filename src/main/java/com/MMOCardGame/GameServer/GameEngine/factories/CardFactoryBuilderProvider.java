package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardFactoryBuilder;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader;
import com.MMOCardGame.GameServer.GameEngine.Cards.JsonCardInfoReader;
import com.MMOCardGame.GameServer.factories.AppConfigurationProvider;
import dagger.Module;
import dagger.Provides;

import java.io.IOException;
import java.util.List;

@Module(includes = AppConfigurationProvider.class)
public class CardFactoryBuilderProvider {
    static CardFactoryBuilder cardFactory = null;

    @Provides
    public CardFactoryBuilder getCardFactory(AppConfiguration configuration) {
        if (cardFactory == null) {
            try {
                List<CardInfoReader.CardInfo> cardInfoList = getCardsInfo(configuration);
                createCardFactory(configuration, cardInfoList);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return cardFactory;
    }

    private void createCardFactory(AppConfiguration configuration, List<CardInfoReader.CardInfo> cardInfoList) throws IOException {
        cardFactory = new CardFactoryBuilder();
        cardFactory.convertListOfCardInfo(cardInfoList);
        cardFactory.findScriptClasses(configuration.getCardScriptsPackages());
    }

    private List<CardInfoReader.CardInfo> getCardsInfo(AppConfiguration configuration) throws IOException {
        JsonCardInfoReader jsonCardInfoReader = new JsonCardInfoReader();
        List<CardInfoReader.CardInfo> cardInfoList = null;
        cardInfoList = jsonCardInfoReader.readFromFile(configuration.getCardsPath());
        return cardInfoList;
    }
}
