package com.MMOCardGame.GameServer.GameEngine.factories;

import dagger.Module;
import dagger.Provides;

@Module
public class GameRefereeBuilderProvider {
    @Provides
    GameRefereeBuilder getGameRefereeBuilder() {
        return new GameRefereeBuilder();
    }
}
