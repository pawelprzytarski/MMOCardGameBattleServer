package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.GameStateElements.HeroInfoReader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonHeroInfoReader implements HeroInfoReader {
    @Override
    public Map<Integer, PlayerFactory.PlayerInfo> readFromStream(InputStream stream) throws IOException {
        if (stream.available() <= 0) throw new IllegalArgumentException();

        try (InputStreamReader inputStreamReader = new InputStreamReader(stream)) {
            Type listType = new TypeToken<ArrayList<PlayerFactory.PlayerInfo>>() {
            }.getType();
            return ((List<PlayerFactory.PlayerInfo>) new Gson().fromJson(inputStreamReader, listType))
                    .stream().collect(Collectors.toMap(PlayerFactory.PlayerInfo::getId, info -> info));

        }
    }
}
