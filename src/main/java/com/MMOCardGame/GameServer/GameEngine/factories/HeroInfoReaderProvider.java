package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.GameStateElements.HeroInfoReader;
import dagger.Module;
import dagger.Provides;

@Module
public class HeroInfoReaderProvider {
    @Provides
    HeroInfoReader getHeroInfoReader() {
        return new JsonHeroInfoReader();
    }
}
