package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.UserSessionEndInfo;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.common.Observer;
import com.MMOCardGame.GameServer.common.exceptions.NotFoundException;

public class BasicGameSessionManagerBuilder implements GameSessionManagerBuilder {
    private UserConnectionsManager connectionsManager;
    private GameSessionFactory gameSessionFactory;
    private Observer<UserSessionEndInfo> infoObserver;

    @Override
    public GameSessionManagerBuilder setUserConnectionsManager(UserConnectionsManager userConnectionsManager) {
        connectionsManager = userConnectionsManager;
        return this;
    }

    @Override
    public GameSessionManagerBuilder setGameSessionFactory(GameSessionFactory gameSessionFactory) {
        this.gameSessionFactory = gameSessionFactory;
        return this;
    }

    @Override
    public GameSessionManagerBuilder setInfoObserver(Observer<UserSessionEndInfo> infoObserver) {
        this.infoObserver = infoObserver;
        return this;
    }

    @Override
    public GameSessionManager build() {
        checkParams();

        GameSessionManager sessionManager = new GameSessionManager(gameSessionFactory);
        registerGameSessionListeners(sessionManager);


        return sessionManager;
    }

    private void registerGameSessionListeners(GameSessionManager sessionManager) {
        connectionsManager.addListenerForCreate(sessionManager::registerConnectionToSession);
        connectionsManager.addListenerForRemove(sessionManager::removeConnectionFromSession);
        sessionManager.addUserSessionEndCallback(infoObserver);
        sessionManager.addUserSessionEndCallback((userSessionEndInfo)->{
                connectionsManager.removeConnectionForUserSilent(userSessionEndInfo.userLogin);
        });
    }

    private void checkParams() {
        if (connectionsManager == null)
            throw new NullPointerException("UserConnectionsManager cannot be null");
        if (gameSessionFactory == null)
            throw new NullPointerException("GameSessionFactory cannot be null");
        if (infoObserver == null)
            throw new NullPointerException("InfoObserver cannot be null");
    }
}
