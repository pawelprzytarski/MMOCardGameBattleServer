package com.MMOCardGame.GameServer.GameEngine.factories;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardFactoryBuilder;
import dagger.Module;
import dagger.Provides;

import java.util.concurrent.ThreadLocalRandom;

@Module(includes = {CardFactoryBuilderProvider.class, PlayerFactoryProvider.class})
public class GameStateBuilderProvider {
    @Provides
    public GameStateBuilder gameStateBuilder(CardFactoryBuilder cardFactory, PlayerFactory heroFactory) {
        return new GameStateBuilder()
                .setCardFactory(cardFactory.build())
                .setPlayerFactory(heroFactory)
                .setRandomGenerator(ThreadLocalRandom.current());
    }
}
