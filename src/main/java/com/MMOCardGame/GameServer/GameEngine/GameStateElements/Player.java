package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerDefeatedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.PlayerLifeChangedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;
import lombok.*;

import java.io.Serializable;

@EqualsAndHashCode(exclude = "triggers")
@ToString(exclude = "triggers")
public class Player implements Serializable {
    @Getter
    private final int heroId;
    @Getter
    private final int userId;
    private final int[] abilitiesCooldown;
    private final int[] abilitiesCooldownChangeOnUse;
    private final int[] costs;
    @Getter
    private int life;
    @Getter
    private int muligansCount = 0;
    @Getter
    private boolean acceptedHand = false;
    @Getter
    @Setter
    private int startedRounds = 0;
    private Triggers triggers;
    @Getter
    private boolean canDie = true;
    @Getter
    private LinksPoints linksPoints;

    public Player(int heroId, int userId, int life, int[] abilitiesCooldown, int[] abilitiesCooldownChangeOnUse, int[] costs) {
        this.heroId = heroId;
        this.userId = userId;
        this.abilitiesCooldown = abilitiesCooldown;
        this.abilitiesCooldownChangeOnUse = abilitiesCooldownChangeOnUse;
        this.costs = costs;
        this.life = life;
        this.linksPoints = new LinksPoints(userId);
    }

    public void doMuligan() {
        muligansCount++;
    }

    public void acceptHand() {
        acceptedHand = true;
    }

    public int getCurrentCooldownForAbility(int abilityId) {
        return abilitiesCooldown[abilityId];
    }

    public void changeCooldownForAbility(int abilityId, int value) {
        abilitiesCooldown[abilityId] += value;
    }

    public int getCostForAbility(int abilityId) {
        return costs[abilityId];
    }

    public boolean userAbilityIfCan(int abilityId) {
        if (abilitiesCooldown[abilityId] > 0)
            return false;
        abilitiesCooldown[abilityId] += abilitiesCooldownChangeOnUse[abilityId];

        return true;
    }

    public void changeLife(int value) {
        int oldValue = life;
        life += value;
        if (triggers != null)
            triggers.invokeTrigger(new PlayerLifeChangedEvent(userId, oldValue, life));
        checkPlayerDie();
    }

    private void checkPlayerDie() {
        if (life <= 0 && canDie)
            triggers.invokeTrigger(new PlayerDefeatedEvent(userId));
    }

    public void setCanDie(boolean value) {
        canDie = value;
        checkPlayerDie();
    }

    public void setTriggers(Triggers triggers) {
        this.triggers = triggers;
        linksPoints.setTriggers(triggers);
    }
}
