package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.Exceptions.BadRequestException;
import com.MMOCardGame.GameServer.GameEngine.GameRefereeElements.ReactionStackController;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardCosts;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardUsed;

import java.util.List;

public class UseCardController {
    private final ReactionStackController reactionStackController;
    private final GameState gameState;
    private boolean correctRequest;
    private CardUsed message;
    private int player;
    private Card card;
    private CardUsed cardUsedResult = null;
    private LinksPoints usedPoints;
    private boolean playCard;
    private int abilityId;

    public UseCardController(GameState gameState, ReactionStackController reactionStackController) {
        this.gameState = gameState;
        this.reactionStackController = reactionStackController;
    }

    public void prepareUseCardAction(CardUsed message1, int player1) {
        this.message = message1;
        this.player = player1;
        correctRequest = true;
        if (checkCardExists(message)) return;
        playCard = gameState.getCardPosition(card.getInstanceId()).getPosition() == CardPosition.Position.Hand;
        if (checkIsCorrectPhase()) return;
        if (checkCannotPlayLink()) return;
        LinksPoints oldPoints = new LinksPoints(gameState.getPlayer(player).getLinksPoints());
        checkPlayerHasLinksPointsAndRemoveThem();
        usedPoints = gameState.getPlayer(player).getLinksPoints().getDifference(oldPoints);
    }

    private boolean checkCannotPlayLink() {
        if(playCard && card.getType()== CardType.Link){
            if(gameState.getPlayedLinks()>=gameState.getAvailableCountOfLinksToPlayed()){
                correctRequest=false;
                return true;
            }
        }
        return false;
    }

    private void checkPlayerHasLinksPointsAndRemoveThem() {
        if (!gameState.getPlayer(player).getLinksPoints().removePointsIfPossible(getLinksPointsFromMessage(message)))
            correctRequest = false;
    }

    private boolean checkIsCorrectPhase() {
        List<GamePhase> availablePhases = getCardAvailableGamePhases();
        if (!availablePhases.contains(gameState.getCurrentPhase())) {
            correctRequest = false;
            return true;
        }
        return false;
    }

    private List<GamePhase> getCardAvailableGamePhases() {
        List<GamePhase> availablePhases;
        if (playCard)
            availablePhases = card.getCardScript().getAvailablePhasesForPlay();
        else availablePhases = card.getCardScript().getAvailablePhasesForAbility();
        return availablePhases;
    }

    private boolean checkCardExists(CardUsed message) {
        Card card = gameState.getCard(message.getCardInstanceId());
        if (card == null || card.getOverloadPoints() > 0) {
            correctRequest = false;
            return true;
        } else this.card = card;
        return false;
    }

    private LinksPoints getLinksPointsFromMessage(CardUsed message) {
        int psychos, whiteHats, raiders, scanners, neutral;
        if (playCard) {
            whiteHats = card.getCurrentStatistics().getWhiteHatCost();
            scanners = card.getCurrentStatistics().getScannersCost();
            psychos = card.getCurrentStatistics().getPsychoCost();
            raiders = card.getCurrentStatistics().getRaidersCost();
            neutral = card.getCurrentStatistics().getNeutralCost();
        } else {
            abilityId = message.getCardAbilityId();
            whiteHats = card.getCardScript().getAbilityCosts(message.getCardAbilityId()).getWhiteHats();
            scanners = card.getCardScript().getAbilityCosts(message.getCardAbilityId()).getScanners();
            psychos = card.getCardScript().getAbilityCosts(message.getCardAbilityId()).getPsychotronnics();
            raiders = card.getCardScript().getAbilityCosts(message.getCardAbilityId()).getRaiders();
            neutral = card.getCardScript().getAbilityCosts(message.getCardAbilityId()).getNeutral();
        }

        var messageCostsSum=message.getCardCosts().getWhiteHats() + message.getCardCosts().getPsychos()
                + message.getCardCosts().getScanners() + message.getCardCosts().getRaiders()
                + message.getCardCosts().getNeutral();

        if (messageCostsSum!=0 && (messageCostsSum < raiders + psychos + whiteHats + scanners + neutral
                || message.getCardCosts().getRaiders() < raiders
                || message.getCardCosts().getScanners() < scanners
                || message.getCardCosts().getPsychos() < psychos
                || message.getCardCosts().getWhiteHats() < whiteHats))
            throw new BadRequestException();

        if(messageCostsSum==0)
            return new LinksPoints(neutral, raiders,scanners,psychos,whiteHats);
        return new LinksPoints(message.getCardCosts().getNeutral(),
                message.getCardCosts().getRaiders(),
                message.getCardCosts().getScanners(),
                message.getCardCosts().getPsychos(),
                message.getCardCosts().getWhiteHats());
    }

    public void addReactionStackElementAndNotifyPlayers() {
        if (correctRequest) {
            gameState.revealCard(card.getInstanceId());
            cardUsedResult = reactionStackController.pushCardActionWithDecisionAndTargets(card, player, message.getCardAbilityId(), castLinksPointsToCosts(usedPoints));
        }
    }

    private CardCosts castLinksPointsToCosts(LinksPoints usedPoints) {
        return CardCosts.newBuilder()
                .setPsychos(usedPoints.psycho)
                .setScanners(usedPoints.scanners)
                .setRaiders(usedPoints.raiders)
                .setNeutral(usedPoints.neutral)
                .setWhiteHats(usedPoints.whiteHats)
                .build();
    }

    public CardUsed getResult() {
        if (correctRequest)
            return cardUsedResult;
        else return null;
    }

    public void invokeRelatedActions() {
        if (correctRequest) {
            if (playCard) {
                gameState.revealCard(card.getInstanceId());
                if(card.getType()==CardType.Link)
                    gameState.setPlayedLinks(gameState.getPlayedLinks()+1);
            }
            else if(cardUsedResult.getStackId()!=-1)
                card.changeOverloadPoints(card.getCardScript().getAbilityOverloadCost(abilityId));
        }
    }
}
