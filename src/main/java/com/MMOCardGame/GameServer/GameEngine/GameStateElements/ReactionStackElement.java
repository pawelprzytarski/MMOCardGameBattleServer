package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(exclude = {"task", "canceledTask"})
public class ReactionStackElement implements Serializable {
    final transient Runnable task;
    final transient Runnable canceledTask;
    final int id;
    final int ownerPlayer;
    boolean canceled = false;

    public ReactionStackElement(Runnable task, Runnable canceledTask, int id, int ownerPlayer) {
        this.canceledTask = canceledTask;
        this.task = task;
        this.id = id;
        this.ownerPlayer = ownerPlayer;
    }
}
