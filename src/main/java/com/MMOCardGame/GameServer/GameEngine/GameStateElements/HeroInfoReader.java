package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.factories.PlayerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface HeroInfoReader {
    default Map<Integer, PlayerFactory.PlayerInfo> readFromFile(String heroInfoPath) throws IOException {
        try (InputStream file = new FileInputStream(heroInfoPath)) {
            return readFromStream(file);
        }
    }

    Map<Integer, PlayerFactory.PlayerInfo> readFromStream(InputStream stream) throws IOException;
}
