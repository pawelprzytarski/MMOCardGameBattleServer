package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition.Position;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardRevealedEvent;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Triggers;

import java.util.Collection;

public class CardFieldWithTriggersActions extends CardsField {
    private final Triggers triggers;
    private final int owner;

    public CardFieldWithTriggersActions(Collection<Card> initCards, Triggers triggers, int owner) {
        super(initCards);
        this.triggers = triggers;
        this.owner = owner;
    }

    @Override
    public void addCard(Position position, Card card) {
        super.addCard(position, card);
        if (card != null && position == Position.Battlefield)
            triggers.registerCardScript(card.getCardScript());
    }

    @Override
    public void removeCard(int instanceId) {
        Card card = getCard(instanceId);
        Position position = getCardPosition(instanceId);
        super.removeCard(instanceId);
        if (card != null && position == Position.Battlefield)
            triggers.unregisterCardScript(card.getCardScript());
    }

    @Override
    protected void moveCardToPosition(int instanceId, Position newPosition) {
        Card card = getCard(instanceId);
        Position oldPosition = getCardPosition(instanceId);
        boolean cardRevealed = card != null && card.isRevealed();
        super.moveCardToPosition(instanceId, newPosition);
        cardMoveTriggersActions(newPosition, card, oldPosition, cardRevealed);
    }

    private void cardMoveTriggersActions(Position newPosition, Card card, Position oldPosition, boolean cardRevealed) {
        if (newPosition == oldPosition || card == null)
            return;
        if (oldPosition != Position.Void) {
            triggersRegistration(newPosition, card, oldPosition);
        } else triggers.invokeTrigger(new CardMovedEvent(card, oldPosition, newPosition));
    }

    @Override
    public void revealCard(int instanceId) {
        Card card = getCard(instanceId);
        if (card == null) return;
        boolean previousRevealState = card.isRevealed();
        super.revealCard(instanceId);
        if (card.isRevealed() != previousRevealState)
            triggers.invokeTrigger(new CardRevealedEvent(getCard(instanceId), new CardPosition(owner, getCardPosition(instanceId), getCardPositionIndex(instanceId))));
    }

    @Override
    public void unrevealCard(int instanceId) {
        Card card = getCard(instanceId);
        if (card == null) return;
        boolean previousRevealState = card.isRevealed();
        super.unrevealCard(instanceId);
        if (card.isRevealed() != previousRevealState)
            triggers.invokeTrigger(new CardRevealedEvent(getCard(instanceId), new CardPosition(owner, getCardPosition(instanceId), getCardPositionIndex(instanceId))));
    }

    private void triggersRegistration(Position newPosition, Card card, Position oldPosition) {
        if (oldPosition == Position.Battlefield) {
            triggers.invokeTrigger(new CardMovedEvent(card, oldPosition, newPosition));
            triggers.unregisterCardScript(card.getCardScript());
        } else if (newPosition == Position.Battlefield) {
            triggers.registerCardScript(card.getCardScript());
            triggers.invokeTrigger(new CardMovedEvent(card, oldPosition, newPosition));
        } else triggers.invokeTrigger(new CardMovedEvent(card, oldPosition, newPosition));
    }
}
