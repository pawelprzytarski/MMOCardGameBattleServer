package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.common.ObservableNotifier;
import com.MMOCardGame.GameServer.common.Observer;
import lombok.ToString;

import java.io.Serializable;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

@ToString
public class ReactionStack implements Serializable {
    private final Stack<ReactionStackElement> reactionStackElements = new Stack<>();
    private final AtomicInteger lastId = new AtomicInteger(0);
    private final transient ObservableNotifier<ReactionStackElement> addActionObservable = new ObservableNotifier<>();
    private final transient ObservableNotifier<ReactionStackElement> afterDoActionObservable = new ObservableNotifier<>();
    private final transient ObservableNotifier<BeforeReactionStackActionEvent> beforeDoActionObservable = new ObservableNotifier<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReactionStack that = (ReactionStack) o;
        return Objects.equals(reactionStackElements, that.reactionStackElements) &&
                Objects.equals(lastId.get(), that.lastId.get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(reactionStackElements, lastId.get());
    }

    public int getElementsCount() {
        return reactionStackElements.size();
    }

    public ReactionStackElement getNextAction() {
        if (reactionStackElements.empty()) return null;
        return reactionStackElements.peek();
    }

    public ReactionStackElement doAction() {
        if (reactionStackElements.empty()) return null;
        ReactionStackElement element = reactionStackElements.peek();
        if (runBeforeDoActionEventAndGetCannotContinue(element)) return null;
        doActionTask(element);
        afterDoActionObservable.notifyAll(element);
        return element;
    }

    private void doActionTask(ReactionStackElement element) {
        reactionStackElements.pop();
        if (element.isCanceled())
            element.getCanceledTask().run();
        else element.getTask().run();
    }

    private boolean runBeforeDoActionEventAndGetCannotContinue(ReactionStackElement element) {
        BeforeReactionStackActionEvent event = new BeforeReactionStackActionEvent(element);
        beforeDoActionObservable.notifyAll(event);
        return event.canceled;
    }

    public ReactionStackElement pushAction(Runnable runnable, Runnable canceledTask, int owner) {
        ReactionStackElement element = new ReactionStackElement(runnable, canceledTask, lastId.getAndIncrement(), owner);
        reactionStackElements.push(element);
        addActionObservable.notifyAll(element);
        return element;
    }

    public ReactionStackElement getElementById(int taskId) {
        return reactionStackElements.stream().filter(reactionStackElement -> reactionStackElement.getId() == taskId).findAny().orElse(null);
    }

    public void addCreateElementObserver(Observer<ReactionStackElement> observer) {
        addActionObservable.addObserver(observer);
    }

    public void removeCreateElementObserver(Observer<ReactionStackElement> observer) {
        addActionObservable.removeObserver(observer);
    }

    public void addBeforeDoActionObserver(Observer<BeforeReactionStackActionEvent> observer) {
        beforeDoActionObservable.addObserver(observer);
    }

    public void removeBeforeDoActionObserver(Observer<BeforeReactionStackActionEvent> observer) {
        beforeDoActionObservable.removeObserver(observer);
    }

    public void addAfterDoActionObserver(Observer<ReactionStackElement> observer) {
        afterDoActionObservable.addObserver(observer);
    }

    public void removeAfterDoActionObserver(Observer<ReactionStackElement> observer) {
        afterDoActionObservable.removeObserver(observer);
    }
}

