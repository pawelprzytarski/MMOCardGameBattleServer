package com.MMOCardGame.GameServer.GameEngine.GameStateElements;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition.Position;
import com.MMOCardGame.GameServer.common.Pair;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode
@ToString
public class CardsField implements Serializable {
    private final Map<Integer, Pair<Card, Position>> cardsMap;
    private final Map<Position, List<Card>> positionCardMap;

    public CardsField(Collection<Card> initCards) {
        cardsMap = initCards.stream().collect(Collectors.toMap(Card::getInstanceId, card -> new Pair<>(card, Position.Library)));
        positionCardMap = EnumSet.allOf(Position.class).stream().collect(Collectors.toMap(o -> o, o -> new ArrayList<>()));
        positionCardMap.get(Position.Library).addAll(initCards);
    }

    public Position getCardPosition(int instanceId) {
        Pair<Card, Position> pair = cardsMap.get(instanceId);
        if (pair == null)
            return Position.Void;
        return pair.getSecond();
    }

    public void removeCard(int instanceId) {
        cardsMap.remove(instanceId);
    }

    public void moveCardToBattlefield(int instanceId) {
        moveCardToPosition(instanceId, Position.Battlefield);
    }

    public void moveCardToVoid(int instanceId) {
        moveCardToPosition(instanceId, Position.Void);
    }

    public void moveCardToGraveyard(int instanceId) {
        moveCardToPosition(instanceId, Position.Graveyard);
    }

    protected void moveCardToPosition(int instanceId, Position newPosition) {
        Pair<Card, Position> pair = cardsMap.get(instanceId);
        if (pair != null) {
            Position oldPosition = pair.getSecond();
            revealCardIfNeeded(instanceId, newPosition, oldPosition);
            pair.setSecond(newPosition);
            moveCardInPositionCache(newPosition, pair, oldPosition);
            pair.getFirst().setPosition(newPosition);
        }
    }

    private void moveCardInPositionCache(Position newPosition, Pair<Card, Position> pair, Position oldPosition) {
        positionCardMap.get(oldPosition).remove(pair.getFirst());
        positionCardMap.get(newPosition).add(pair.getFirst());
    }

    private void revealCardIfNeeded(int instanceId, Position newPosition, Position oldPosition) {
        if ((oldPosition == Position.Library || oldPosition == Position.Hand) && (newPosition != Position.Library && newPosition != Position.Hand))
            revealCard(instanceId);
    }

    public void moveCardToHand(int instanceId) {
        moveCardToPosition(instanceId, Position.Hand);
    }

    public void moveCardToLibrary(int instanceId) {
        moveCardToPosition(instanceId, Position.Library);
    }

    public Card getCard(int instanceId) {
        Pair<Card, Position> pair = cardsMap.get(instanceId);
        if (pair == null)
            return null;
        else return pair.getFirst();
    }

    public void revealCard(int instanceId) {
        Card card = getCard(instanceId);
        if (card != null)
            card.setRevealed(true);
    }

    public void unrevealCard(int instanceId) {
        Card card = getCard(instanceId);
        if (card != null)
            card.setRevealed(false);
    }

    public void addCard(Position position, Card card) {
        cardsMap.put(card.getInstanceId(), new Pair<>(card, position));
    }

    public Collection<Pair<Card, Position>> getAllCardsPositions() {
        return cardsMap.values();
    }

    public Card getFirstCardAtPosition(Position position) {
        if (positionCardMap.get(position).isEmpty())
            return null;
        return positionCardMap.get(position).get(0);
    }

    public void moveCardAtPositionIndex(int instanceId, Position position, int index) {
        Card card = getCard(instanceId);
        if (card != null) {
            moveCardAtPositionIndex(card, position, index);
        }
    }

    public void moveCardAtPositionIndex(Card card, Position position, int index) {
        moveCardToPosition(card.getInstanceId(), position);
        positionCardMap.get(position).remove(card);
        positionCardMap.get(position).add(index, card);
    }

    public void insertCardAtPositionIndex(Card card, Position position, int index) {
        cardsMap.put(card.getInstanceId(), new Pair<>(card, position));
        positionCardMap.get(position).add(index, card);
        card.setPosition(position);
    }

    public int getCardPositionIndex(int instanceId) {
        Card card = getCard(instanceId);
        if (card != null) {
            Position position = getCardPosition(instanceId);
            return positionCardMap.get(position).indexOf(card);
        }
        return -1;
    }
}

