package com.MMOCardGame.GameServer.GameEngine.Cards;

import lombok.Data;

import java.io.Serializable;

@Data
public class CardStatistics implements Serializable {
    private int defence;
    private int attack;
    private int neutralCost;
    private int raidersCost;
    private int psychoCost;
    private int whiteHatCost;
    private int scannersCost;
    private int overloadPoints;

    public CardStatistics() {
    }

    public CardStatistics(int defence, int attack, int neutralCost, int raidersCost, int psychoCost, int whiteHatCost, int scannersCost, int overloadPoints) {
        this.defence = defence;
        this.attack = attack;
        this.neutralCost = neutralCost;
        this.raidersCost = raidersCost;
        this.psychoCost = psychoCost;
        this.whiteHatCost = whiteHatCost;
        this.scannersCost = scannersCost;
        this.overloadPoints = overloadPoints;
    }

    public CardStatistics(CardStatistics other) {
        this(other.defence, other.attack, other.neutralCost, other.raidersCost, other.psychoCost, other.whiteHatCost, other.scannersCost, other.overloadPoints);
    }

    public void resetTo(CardStatistics other) {
        this.defence = other.defence;
        this.attack = other.attack;
        this.neutralCost = other.neutralCost;
        this.raidersCost = other.raidersCost;
        this.psychoCost = other.psychoCost;
        this.whiteHatCost = other.whiteHatCost;
        this.scannersCost = other.scannersCost;
        this.overloadPoints = other.overloadPoints;
    }
}

