package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.StepEnvironment;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions.CantStartTransactionException;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.OptionsInfo;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection.TransactionsCollection;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

public class Transaction {


    @Getter
    @Setter
    private OptionsInfo lastOptionsSentToClient;
    @Getter
    @Setter
    private OptionsInfo lastOptionsSelectedByClient;

    @Getter
    private int id;
    @Getter
    private int currentStepIndex;
    @Getter
    private List<RunningTransactionStep> steps = new LinkedList<>();
    @Getter
    private TransactionDefinition definition;

    private SelectedOptionsValidator optionsValidator = new SelectedOptionsValidator();
    private SelectedOptionsMessages selectedOptionsMessages = new SelectedOptionsMessages();

    public Transaction(TransactionDefinition definition) {
        id = TransactionIdGenerator.generateTransactionId();
        currentStepIndex = 0;
        this.definition = definition;

        initializeRunningTransactionSteps();
    }

    private Transaction() {
        currentStepIndex = 0;
        definition = null;
    }

    public static Transaction getEmpty(TransactionMessage notDefinedResponse) {
        Transaction emptyTransaction = new Transaction();
        emptyTransaction.id = notDefinedResponse.getTransactionId();
        emptyTransaction.definition=new TransactionDefinition(notDefinedResponse.getStartedFor());
        emptyTransaction.selectedOptionsMessages.addToSelectedTargetsMessages(notDefinedResponse);
        return emptyTransaction;
    }

    private void initializeRunningTransactionSteps() {
        if (!definition.hasAnySteps()) {
            throw new CantStartTransactionException();
        }
        for (var stepDefinition : definition.getStepsDefinitions()) {
            steps.add(new RunningTransactionStep(stepDefinition));
        }
    }

    public void handleOptionsSelection(TransactionMessage optionsSelectionMessage,
                                       StepEnvironment stepEnvironment) {
        List<TargetSelectingOption> selectedByClient = optionsSelectionMessage.getOptionsInfo().getOptions();
        getCurrentStep().handleSelectedTargets(selectedByClient, stepEnvironment);
        lastOptionsSelectedByClient = optionsSelectionMessage.getOptionsInfo();

        optionsSelectionMessage.setStartedFor(definition.getStartedFor());
        optionsSelectionMessage.getOptionsInfo().setOptionsType(lastOptionsSentToClient.getOptionsType());

        selectedOptionsMessages.addToSelectedTargetsMessages(optionsSelectionMessage);
    }

    public void goToNextStep(TransactionsCollection transactions) {
        if (hasStepAtIndex(currentStepIndex + 1)) {
            currentStepIndex++;
        } else {
            transactions.finishTransaction(id);
        }
    }

    public boolean hasStepAtIndex(int index) {
        return steps.size() > index && index >= 0;
    }

    public OptionsInfo getCurrentStepPossibleOptions(GameState gameState, int instanceId) {
        return getCurrentStep().getOptionsInfo(gameState, instanceId);
    }

    public void addStepAfterCurrent(RunningTransactionStep newStep) {
        steps.add(currentStepIndex, newStep);
    }

    public boolean validateOptions(List<TargetSelectingOption> selectedByClient) {
        return optionsValidator.validate(selectedByClient, lastOptionsSentToClient);
    }

    public List<TransactionMessage> getMessagesWithSelectedOptions() {
        var messagesWithSelectedOptions = selectedOptionsMessages.getMessagesWithSelectedOptions();
        for(var message :messagesWithSelectedOptions){
            message.setDescription(definition.getFinalMessageDescription(message));
        }
        return messagesWithSelectedOptions;
    }

    public RunningTransactionStep getCurrentStep() {
        return steps.get(currentStepIndex);
    }

}
