package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions.TransactionDefinitionForThisTypeNotFoundException;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.EnumMap;
import java.util.Map;

public class TransactionsDefinitionsCollection {

    private Map<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition> definitions =
            new EnumMap<>(TargetSelectingTransaction.StartTransactionFor.class);

    public void addDefinition(TransactionDefinition definition) {
        definitions.put(definition.getStartedFor(), definition);
    }

    public TransactionDefinition getDefinition(TargetSelectingTransaction.StartTransactionFor type) {
        if (contains(type)) {
            return definitions.get(type);
        } else {
            throw new TransactionDefinitionForThisTypeNotFoundException(type);
        }
    }

    public boolean contains(TargetSelectingTransaction.StartTransactionFor type) {
        return definitions.containsKey(type);
    }

}
