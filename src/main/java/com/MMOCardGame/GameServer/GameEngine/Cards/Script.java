package com.MMOCardGame.GameServer.GameEngine.Cards;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Script {
    int[] ids();

    ScriptType type();

    enum ScriptType {
        Card,
        NamedAbility
    }
}


