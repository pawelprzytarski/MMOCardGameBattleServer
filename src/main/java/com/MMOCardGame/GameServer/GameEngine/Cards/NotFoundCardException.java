package com.MMOCardGame.GameServer.GameEngine.Cards;

public class NotFoundCardException extends RuntimeException {
    public NotFoundCardException() {
    }

    public NotFoundCardException(String message) {
        super(message);
    }

    public NotFoundCardException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundCardException(Throwable cause) {
        super(cause);
    }

    public NotFoundCardException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
