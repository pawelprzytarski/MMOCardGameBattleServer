package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions;

public class CantStartTransactionException extends RuntimeException {
    public CantStartTransactionException(){
        super("Could't start transaction with definition without any step defined.");
    }
}