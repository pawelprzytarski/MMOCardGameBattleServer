package com.MMOCardGame.GameServer.GameEngine.Cards;

public enum CardSubtype {
    Undefined,
    None,
    Basic,
    Legendary,
}
