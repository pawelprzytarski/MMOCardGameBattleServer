package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;

@Script(ids = 7, type = Script.ScriptType.Card)
public class OverclockScript extends CardScript {

    private static final int ATTACK_INC = 2;

    @Override
    public boolean canDie(){
        return false;
    }

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                gameState
                        .getPlayerCardsPositions(parentCard.getOwner())
                        .stream()
                        .filter(pair -> pair.getSecond().equals(CardPosition.Position.Battlefield) &&
                                        pair.getFirst().getType().equals(CardType.Unit))
                        .forEach(pair -> pair.getFirst().changeAttack(ATTACK_INC));
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }


}
