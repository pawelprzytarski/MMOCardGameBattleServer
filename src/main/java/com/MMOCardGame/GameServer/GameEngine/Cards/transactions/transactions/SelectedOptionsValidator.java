package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.OptionsInfo;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;

import java.util.List;

public class SelectedOptionsValidator {

    public boolean validate(List<TargetSelectingOption> selectedByClient, OptionsInfo lastOptionsSentToClient){
        if(lastOptionsSentToClient == null){
            return false;
        }
        if(selectedByClient.size() > lastOptionsSentToClient.getCountOfTargets()){
            return false;
        }
        var sentFromServer = lastOptionsSentToClient.getOptions();
        return checkIfAllOptionsSelectedByClientWereSentFromServer(selectedByClient, sentFromServer);
    }

    private boolean checkIfAllOptionsSelectedByClientWereSentFromServer(List<TargetSelectingOption> selectedByClient, List<TargetSelectingOption> sentToClient) {
        for(var selectedOption : selectedByClient){
            if(!contains(sentToClient, selectedOption)){
                return false;
            }
        }
        return true;
    }

    private boolean contains(List<TargetSelectingOption> optionsList, TargetSelectingOption option){
        for(var optionIterator : optionsList){
            if(optionsEquals(optionIterator, option)){
                return true;
            }
        }
        return false;
    }

    private boolean optionsEquals(TargetSelectingOption option1, TargetSelectingOption option2){
        return (option1 == null && option2 == null) ||
               (option1 != null && option2 != null &&
                option1.getId() == option2.getId() &&
                option1.getTargetType().equals(option2.getTargetType()));
    }

}
