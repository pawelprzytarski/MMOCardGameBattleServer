package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;

public interface MessageHandler {

    TransactionMessage handle(TransactionMessage request,
                              TransactionEnvironment transactionEnvironment);

}
