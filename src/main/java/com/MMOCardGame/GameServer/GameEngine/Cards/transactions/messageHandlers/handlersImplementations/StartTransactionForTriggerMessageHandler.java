package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.MessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.TransactionEnvironment;

// When message has values in fields: type=StartTransaction and transactionStartedFor=Trigger
// transactionId indicates trigger type, not transaction id, and should be treated differently than usual,
// that's why it's handled in separate handler. It's 'hidden' message type.
// For now it's doing the same what StarTransactionMessageHandler because triggers are implemented only partially.
public class StartTransactionForTriggerMessageHandler implements MessageHandler {

    @Override
    public TransactionMessage handle(TransactionMessage request,
                                     TransactionEnvironment transactionEnvironment) {
        var handler = new StartTransactionMessageHandler();

        return handler.handle(request, transactionEnvironment);
    }

}
