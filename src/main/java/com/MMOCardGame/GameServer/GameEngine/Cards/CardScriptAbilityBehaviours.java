package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.common.CallbackTask;
import com.MMOCardGame.GameServer.common.CallbackTaskWithReturn;
import lombok.Setter;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CardScriptAbilityBehaviours extends CardScriptTransactionsSupport {
    @Setter
    private int defendOverloadCost = 1;
    @Setter
    private int attackOverloadCost = 1;
    @Setter
    private int abilityOverloadCost = 1;

    public CallbackTask<GameState> getDeathAction() {
        return null;
    }

    public CardAbilityTask playAction(@Nonnull GameState gameState, int decisionId/*=-1 if none*/) {
        List<CardAbilityTask> cardAbilityTasks;
        cardAbilityTasks = new ArrayList<>(Collections.singletonList(getPlayAction(gameState, decisionId)));
        cardAbilityTasks.addAll(getSubscriptsTasks(obj -> obj.playAction(gameState, decisionId)));
        return flatListOfTaskToSingleTask(cardAbilityTasks, decisionId);
    }

    private CardAbilityTask flatListOfTaskToSingleTask(List<CardAbilityTask> cardAbilityTasks, int abilityId) {
        var nonNullTasks = cardAbilityTasks.stream().filter(Objects::nonNull).collect(Collectors.toList());

        if (nonNullTasks.isEmpty())
            return null;
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                for (var task : nonNullTasks) {
                    task.run(gameState);
                }
            }

            @Override
            public void runCanceled(GameState gameState) {
                for (var task : nonNullTasks) {
                    task.runCanceled(gameState);
                }
            }

            @Override
            public int getAbilityId() {
                return abilityId;
            }
        };
    }

    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                gameState.moveCardToBattlefield(parentCard.getInstanceId());
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

    private List<CardAbilityTask> getSubscriptsTasks(CallbackTaskWithReturn<CardAbilityTask, CardScript> method) {
        List<CardAbilityTask> cardAbilityTasks = new ArrayList<>();
        for (CardScript cardScript : subscripts) {
            var task = method.run(cardScript);
            if (task != null)
                cardAbilityTasks.add(task);
        }
        return cardAbilityTasks;
    }

    public CardAbilityTask abilityAction(@Nonnull GameState gameState, int decisionId/*=-1 if none*/) {
        var actionsList = getSubscriptsTasks(obj -> obj.abilityAction(gameState, decisionId));
        var action = getAbilityAction(gameState, decisionId);
        if (action != null)
            actionsList.add(action);
        return flatListOfTaskToSingleTask(actionsList, decisionId);
    }

    protected CardAbilityTask getAbilityAction(GameState gameState, int transactionId) {
        return null;
    }

    public List<GamePhase> getAvailablePhasesForPlay() {
        List<GamePhase> list = getSubscriptsAvailablePhases(CardScript::getAvailablePhasesForPlay);
        list.add(GamePhase.Main);
        return list;
    }

    protected List<GamePhase> getSubscriptsAvailablePhases(CallbackTaskWithReturn<List<GamePhase>, CardScript> method) {
        List<GamePhase> cardAbilityTasks = new ArrayList<>();
        for (CardScript cardScript : subscripts)
            cardAbilityTasks.addAll(method.run(cardScript));
        return cardAbilityTasks;
    }

    public List<GamePhase> getAvailablePhasesForAbility() {
        List<GamePhase> list = getSubscriptsAvailablePhases(CardScript::getAvailablePhasesForAbility);
        list.add(GamePhase.Main);
        return list;
    }

    public int getAbilityOverloadCost(int id) {
        return abilityOverloadCost;
    }

    public int getDefendOverloadCost() {
        return defendOverloadCost;
    }

    public int getAttackOverloadCost() {
        return attackOverloadCost;
    }

    public CardInfoReader.CardInfo.Costs getAbilityCosts(int abilityId) {
        var abilityCosts = parentCard.getAbilityCosts(abilityId);
        if(abilityCosts != null){
            return abilityCosts;
        }
        return new CardInfoReader.CardInfo.Costs();
    }
}
