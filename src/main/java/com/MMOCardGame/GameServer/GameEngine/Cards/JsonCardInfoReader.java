package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonCardInfoReader implements CardInfoReader {
    @Override
    public List<CardInfo> readFromStream(InputStream stream) throws IOException, IllegalArgumentException {
        if (stream.available() == 0) throw new IllegalArgumentException();
        try (InputStreamReader inputStreamReader = new InputStreamReader(stream)) {
            Type listType = new TypeToken<ArrayList<CardInfo>>() {
            }.getType();
            return new Gson().fromJson(inputStreamReader, listType);
        }
    }
}
