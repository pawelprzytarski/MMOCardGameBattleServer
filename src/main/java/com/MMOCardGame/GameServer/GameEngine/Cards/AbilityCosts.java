package com.MMOCardGame.GameServer.GameEngine.Cards;

import lombok.Data;

@Data
public class AbilityCosts {
    private int neutralCost;
    private int raidersCost;
    private int psychoCost;
    private int whiteHatCost;
    private int scannersCost;
}
