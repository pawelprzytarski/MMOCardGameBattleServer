package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts.LinkCardBaseScript;
import com.MMOCardGame.GameServer.GameEngine.GameStateElements.LinksPoints;

@Script(ids = 4, type = Script.ScriptType.Card)
public class PsychotronicsLinkScript extends LinkCardBaseScript {

    @Override
    protected LinksPoints getPointsTooAdd(){
        return new LinksPoints(0, 0, 0, 1, 0);
    }

}
