package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts.ScriptBaseScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionDefinition;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.steps.TargetsStepDefinition;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingOption;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetType;

import java.util.ArrayList;
import java.util.EnumMap;

@Script(ids = 12, type = Script.ScriptType.Card)
public class ForegroundExecutionScript extends ScriptBaseScript {

    private static final int ATTACK_INC = 2;

    @Override
    public boolean canDie(){
        return false;
    }

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                var selectedTargets = new ArrayList<TargetSelectingOption>();
                for (var transactionMessage : getSelectedOptionsDuringTransaction(transactionId)){
                    selectedTargets.addAll(transactionMessage.getOptionsList());
                }
                if(selectedTargets.size() != 1){
                    return;
                }
                var selectedTarget = selectedTargets.get(0);
                if(selectedTarget.getTargetType().equals(TargetType.Card)){
                    gameState.getCard(selectedTarget.getId()).changeAttack(ATTACK_INC);
                }
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

    @Override
    protected EnumMap getTransactionsDefinitions() {
        var definitions =
                new EnumMap<TargetSelectingTransaction.StartTransactionFor, TransactionDefinition>(
                        TargetSelectingTransaction.StartTransactionFor.class
                );

        var playTransactionDefinition = getTransactionDefinitionForPlay();
        definitions.put(playTransactionDefinition.getStartedFor(), playTransactionDefinition);

        return definitions;
    }

    private TransactionDefinition getTransactionDefinitionForPlay(){
        var playTransactionDefinition = new TransactionDefinition(TargetSelectingTransaction.StartTransactionFor.Play);
        playTransactionDefinition.addStep(new TargetsStepDefinition(
                (gameState, player) -> false,
                (gameState, card) ->    card.getType().equals(CardType.Unit) &&
                                        gameState
                                                .getCardPosition(card.getInstanceId())
                                                .getPosition()
                                                .equals(CardPosition.Position.Battlefield),
                1));
        return playTransactionDefinition;
    }

}
