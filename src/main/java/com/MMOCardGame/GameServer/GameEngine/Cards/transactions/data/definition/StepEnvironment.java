package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StepEnvironment {

    private Transaction transaction;
    private GameState gameState;

}
