package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.MessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.TransactionEnvironment;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;

public class StartTransactionMessageHandler implements MessageHandler {

    @Override
    public TransactionMessage handle(TransactionMessage request,
                                     TransactionEnvironment transactionEnvironment) {
        var response = request.getResponseTemplate();

        var transactions = transactionEnvironment.getTransactions();
        var definitions = transactionEnvironment.getDefinitions();
        var gameState = transactionEnvironment.getGameState();
        var instanceId = transactionEnvironment.getInstanceId();

        if(!definitions.contains(request.getStartedFor())){
            var notDefinedResponse = buildResultNotDefinedResponse(response);
            transactions.rememberEmptyTransaction(notDefinedResponse);
            return notDefinedResponse;
        }
        var definition = definitions.getDefinition(request.getStartedFor());
        var transaction = transactions.tryStartAndGetNewTransaction(definition);

        response.setDescription("Transaction: " + transaction.getId() + " started.");

        var possibleOptions = transaction.getCurrentStepPossibleOptions(gameState, instanceId);
        transaction.setLastOptionsSentToClient(possibleOptions);

        response.setOptionsInfo(possibleOptions);
        response.setTransactionId(transaction.getId());
        response.setType(TargetSelectingTransactionType.AvailableOptions);

        return response;
    }

    private TransactionMessage buildResultNotDefinedResponse(TransactionMessage response) {
        response.setDescription("Transaction definition for this type is not defined.");
        response.setType(TargetSelectingTransactionType.Result);
        return response;
    }
}
