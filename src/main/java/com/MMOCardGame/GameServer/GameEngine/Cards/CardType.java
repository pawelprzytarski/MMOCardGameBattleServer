package com.MMOCardGame.GameServer.GameEngine.Cards;

public enum CardType {
    Undefined,
    Link,//Link
    Unit,//Jednostka
    ComputingUnit,//Jednostka obliczeniowa
    Script,//SKrypt
    ServiceProgram,//program-usługa
    Operation,//Działanie
}
