package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardSubscript;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Enums.GamePhase;

import java.util.List;

@Script(ids = 3, type= Script.ScriptType.NamedAbility)
public class MaterializacjaKoduScript extends CardSubscript {

    @Override
    public List<GamePhase> getAvailablePhasesForPlay() {
        List<GamePhase> list = getSubscriptsAvailablePhases(CardScript::getAvailablePhasesForPlay);
        list.add(GamePhase.Main);
        list.add(GamePhase.Attack);
        list.add(GamePhase.Defence);
        list.add(GamePhase.EndRound);
        list.add(GamePhase.ReactionPhase);
        return list;
    }

}
