package com.MMOCardGame.GameServer.GameEngine.Cards;

import lombok.Data;

@Data
public class StatisticsChange extends CardStatistics {
    private int id;
    private int instanceId;
    private CardType type;
    private CardSubtype subtype;
    private String className;
    private String subclassName;

    public StatisticsChange(Card card) {
        super(card.getCurrentStatistics());
        id = card.getId();
        instanceId = card.getInstanceId();
        type = card.getType();
        subtype = card.getSubtype();
        className = card.getClassName();
        subclassName = card.getSubclassName();
    }

    public StatisticsChange(CardStatistics other, int id, int instanceId, int overloadPoints, CardType type, CardSubtype subtype, String className, String subclassName) {
        super(other);
        this.id = id;
        this.instanceId = instanceId;
        this.type = type;
        this.subtype = subtype;
        this.className = className;
        this.subclassName = subclassName;
    }

    public StatisticsChange(int defence, int attack, int neutralCost, int raidersCost, int psychoCost, int whiteHatCost, int scannersCost, int id, int instanceId, int overloadPoints, CardType type, CardSubtype subtype, String className, String subclassName) {
        super(defence, attack, neutralCost, raidersCost, psychoCost, whiteHatCost, scannersCost, overloadPoints);
        this.id = id;
        this.instanceId = instanceId;
        this.type = type;
        this.subtype = subtype;
        this.className = className;
        this.subclassName = subclassName;
    }
}
