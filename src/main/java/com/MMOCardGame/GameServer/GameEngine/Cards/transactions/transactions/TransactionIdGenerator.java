package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions;

import java.util.concurrent.atomic.AtomicInteger;

public class TransactionIdGenerator {
    private final static AtomicInteger transactionIdCounter = new AtomicInteger();

    public static int generateTransactionId(){
        return transactionIdCounter.incrementAndGet();
    }
}
