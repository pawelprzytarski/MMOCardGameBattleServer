package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.exceptions;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

public class TransactionDefinitionForThisTypeNotFoundException extends RuntimeException {
    public TransactionDefinitionForThisTypeNotFoundException(TargetSelectingTransaction.StartTransactionFor type){
        super(type.name());
    }
}
