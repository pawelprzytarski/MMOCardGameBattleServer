package com.MMOCardGame.GameServer.GameEngine.Cards;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

public class CardFactoryBuilder {
    private Map<Integer, Class<? extends CardScript>> cardScriptMap;
    private Map<Integer, CardInfoReader.CardInfo> cardInfoMap;
    private Map<Integer, Class<? extends CardScript>> namedAbilityScriptsMap;
    private Random random = ThreadLocalRandom.current();

    public CardFactoryBuilder findScriptClasses(Collection<String> packages) throws IOException {
        CardScriptFinder cardScriptFinder = new CardScriptFinder(packages);
        cardScriptMap = cardScriptFinder.findCardScripts();
        namedAbilityScriptsMap = cardScriptFinder.findNamedAbilitiesScripts();
        return this;
    }

    public CardFactoryBuilder setRandom(Random random) {
        this.random = random;
        return this;
    }

    public CardFactoryBuilder setCardScriptFinder(CardScriptFinder cardScriptFinder) {
        cardScriptMap = cardScriptFinder.findCardScripts();
        namedAbilityScriptsMap = cardScriptFinder.findNamedAbilitiesScripts();
        return this;
    }

    public CardFactoryBuilder convertListOfCardInfo(List<CardInfoReader.CardInfo> cardsInfo) {
        cardInfoMap = new ConcurrentHashMap<>();
        cardsInfo.parallelStream().forEach(cardInfo -> {
            cardInfo.prepareToUse();
            cardInfoMap.put(cardInfo.id, cardInfo);
        });
        return this;
    }

    public CardFactory build() {
        if (cardScriptMap == null || namedAbilityScriptsMap == null || cardInfoMap == null)
            throw new NullPointerException();
        return new CardFactory(cardInfoMap, cardScriptMap, namedAbilityScriptsMap, random);
    }

}
