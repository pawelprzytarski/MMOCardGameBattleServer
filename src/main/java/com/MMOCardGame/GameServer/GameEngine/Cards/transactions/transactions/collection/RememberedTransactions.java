package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.Transaction;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransaction;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class RememberedTransactions {

    private Map<Integer, Transaction> transactionsById = new HashMap<>();
    private Map<TargetSelectingTransaction.StartTransactionFor, Transaction> transactionsByType =
            new EnumMap<>(TargetSelectingTransaction.StartTransactionFor.class);

    public void rememberTransaction(Transaction transaction) {
        if (contains(transaction.getDefinition().getStartedFor())) {
            remove(transaction.getId());
        }
        transactionsById.put(transaction.getId(), transaction);
        transactionsByType.put(transaction.getDefinition().getStartedFor(), transaction);
    }

    public void remove(int id) {
        if (!contains(id)) {
            return;
        }
        transactionsByType.remove(transactionsById.get(id).getDefinition().getStartedFor());
        transactionsById.remove(id);
    }

    public void remove(TargetSelectingTransaction.StartTransactionFor type) {
        if (!contains(type)) {
            return;
        }
        transactionsById.remove(transactionsByType.get(type).getId());
        transactionsByType.remove(type);
    }

    public boolean contains(TargetSelectingTransaction.StartTransactionFor type) {
        return transactionsByType.containsKey(type);
    }

    public boolean contains(int id) {
        return transactionsById.containsKey(id);
    }

    public Transaction getTransaction(int id) {
        return transactionsById.get(id);
    }

}
