package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.definition.TransactionsDefinitionsCollection;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.transactions.collection.TransactionsCollection;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TransactionEnvironment {

    private GameState gameState;
    private TransactionsCollection transactions;
    private TransactionsDefinitionsCollection definitions;
    private int instanceId;

}
