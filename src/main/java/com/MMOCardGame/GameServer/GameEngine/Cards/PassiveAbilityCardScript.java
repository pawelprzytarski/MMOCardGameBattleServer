package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.Triggers.EventScripts.CardMovedScript;
import com.MMOCardGame.GameServer.GameEngine.Triggers.Events.CardMovedEvent;
import com.MMOCardGame.GameServer.common.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public abstract class PassiveAbilityCardScript extends CardScript implements CardMovedScript {
    protected CardPosition parentPosition = new CardPosition(-1, CardPosition.Position.Library, -1);

    private static boolean isOnAffectedPosition(CardPosition card, List<CardPosition> cardPositions) {
        return cardPositions.contains(card);
    }

    @Override
    public void cardMoved(CardMovedEvent event) {
        if (event.getMovedCard() == getParentCard()) {
            addOrRemovePassiveEffectForAllCards(event);
        } else if (parentPosition.getPosition().equals(CardPosition.Position.Battlefield)) {
            addEffectToCardIfNeeded(event);
            removeEffectFromCardIfNeeded(event);
        }
    }

    private void addOrRemovePassiveEffectForAllCards(CardMovedEvent event) {
        List<CardPosition> oldAffectedPositions = getAffectedPositions();
        CardPosition oldParentPosition = parentPosition;
        parentPosition = event.getNewPosition();
        if (shouldRemovePassiveEffectForAllCards(event, oldAffectedPositions)) {
            removePassiveEffectFromAllCardsFromOldPosition(event, oldParentPosition);
        }
        if (shouldAddPassiveEffectForAllCards(event, oldAffectedPositions))
            addPassiveEffectForAllCards(event.getGameState());
    }

    private boolean shouldAddPassiveEffectForAllCards(CardMovedEvent event, List<CardPosition> oldAffectedPositions) {
        return (event.getNewPosition().getPosition() == CardPosition.Position.Battlefield &&
                event.getOldPosition().getPosition() != CardPosition.Position.Battlefield)
                || !oldAffectedPositions.equals(getAffectedPositions());
    }

    private void removePassiveEffectFromAllCardsFromOldPosition(CardMovedEvent event, CardPosition oldParentPosition) {
        parentPosition = oldParentPosition;
        removePassiveEffectFromAllCards(event.getGameState());
        parentPosition = event.getNewPosition();
    }

    private boolean shouldRemovePassiveEffectForAllCards(CardMovedEvent event, List<CardPosition> oldAffectedPositions) {
        return (event.getNewPosition().getPosition() != CardPosition.Position.Battlefield &&
                event.getOldPosition().getPosition() == CardPosition.Position.Battlefield)
                || !oldAffectedPositions.equals(getAffectedPositions());
    }

    protected void removeEffectFromCardIfNeeded(CardMovedEvent event) {
        List<CardPosition> affectedPositions = getAffectedPositions();
        if (affectedPositions.contains(event.getOldPosition()) && !affectedPositions.contains(event.getNewPosition()))
            removeEffectFromCard(event.getMovedCard());
    }

    protected void addEffectToCardIfNeeded(CardMovedEvent event) {
        List<CardPosition> affectedPositions = getAffectedPositions();
        if (!affectedPositions.contains(event.getOldPosition()) && affectedPositions.contains(event.getNewPosition()))
            addEffectToCard(event.getMovedCard());
    }

    protected void removePassiveEffectFromAllCards(GameState gameState) {
        Stream<Pair<Card, CardPosition>> stream = getAffectedCardsStream(gameState);
        stream.forEach(pair -> removeEffectFromCard(pair.getFirst()));
    }

    protected void addPassiveEffectForAllCards(GameState gameState) {
        Stream<Pair<Card, CardPosition>> stream = getAffectedCardsStream(gameState);
        stream.forEach(pair -> addEffectToCard(pair.getFirst()));
    }

    private Stream<Pair<Card, CardPosition>> getAffectedCardsStream(GameState gameState) {
        List<CardPosition> affectedPositions = getAffectedPositions();
        return gameState.getAllCardsPositions()
                .stream()
                .filter(pair -> isOnAffectedPosition(pair.getSecond(), affectedPositions) && isIncludedParent(pair.getFirst()));
    }

    private boolean isIncludedParent(Card first) {
        return first != getParentCard() || isShouldIncludeMyself();
    }

    protected List<CardPosition> getAffectedPositions() {
        return Arrays.asList(new CardPosition(parentPosition.getOwner(), CardPosition.Position.Battlefield, -1));
    }

    protected boolean isShouldIncludeMyself() {
        return false;
    }

    protected abstract void addEffectToCard(Card card);

    protected abstract void removeEffectFromCard(Card card);
}
