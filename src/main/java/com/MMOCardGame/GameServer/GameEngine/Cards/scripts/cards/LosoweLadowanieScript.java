package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.*;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;
import com.MMOCardGame.GameServer.GameEngine.factories.DaggerCardFactoryFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Script(ids = 28, type = Script.ScriptType.Card)
public class LosoweLadowanieScript extends CardScript {

    private static final int HUSARZ_ID = 5;
    private static final int LIDER_SIATKI_ID = 27;
    private static final int ZARZADCA_CENTRALI_ID = -1;
    private static final int SINOSU_ID = 9;

    private List<Integer> randomCardsIds = Arrays.asList(HUSARZ_ID, ZARZADCA_CENTRALI_ID, SINOSU_ID, LIDER_SIATKI_ID);

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                CardFactory cardFactory = DaggerCardFactoryFactory.create().getCardFactoryBuilder().build();
                Random rand = new Random();
                for(int i = 0; i < 3; i++){
                    int cardId = randomCardsIds.get(rand.nextInt(randomCardsIds.size()));
                    Card card = cardFactory.createCard(cardId);
                    gameState.insertCard(card, new CardPosition(parentCard.getOwner(), CardPosition.Position.Battlefield));
                }
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

}
