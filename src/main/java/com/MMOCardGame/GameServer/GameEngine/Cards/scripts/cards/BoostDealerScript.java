package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardInfoReader;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards.baseScripts.UnitBaseScript;
import com.MMOCardGame.GameServer.GameEngine.GameState;

@Script(ids = 17, type = Script.ScriptType.Card)
public class BoostDealerScript extends UnitBaseScript {

    private static final int ATTACK_INC = 2;

    @Override
    protected CardAbilityTask getAbilityAction(GameState gameState, int transactionId) {
        return e -> {
            parentCard.getBaseStatistics().setAttack(parentCard.getBaseStatistics().getAttack() + ATTACK_INC);
            parentCard.changeAttack(ATTACK_INC);
        };
    }

    @Override
    public int getAbilityOverloadCost(int transactionId) {
        return parentCard.getAbilityCosts(1).getOverload();
    }

    @Override
    public CardInfoReader.CardInfo.Costs getAbilityCosts(int transactionId) {
        return parentCard.getAbilityCosts(1);
    }
}
