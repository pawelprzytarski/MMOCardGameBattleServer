package com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.handlersImplementations;

import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.data.message.TransactionMessage;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.MessageHandler;
import com.MMOCardGame.GameServer.GameEngine.Cards.transactions.messageHandlers.TransactionEnvironment;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.TargetSelectingTransactionType;

public class AbortMessageHandler implements MessageHandler {

    @Override
    public TransactionMessage handle(TransactionMessage request,
                                     TransactionEnvironment transactionEnvironment) {
        var response = request.getResponseTemplate();

        response.setDescription("Aborting transaction: " + request.getTransactionId());

        transactionEnvironment.getTransactions().abortTransaction(request.getTransactionId());

        response.setType(TargetSelectingTransactionType.Abort);

        return response;
    }
}
