package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardType;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.GameEngine.GameState;

@Script(ids = 20, type = Script.ScriptType.Card)
public class PoszukiwanieLukScript extends CardScript {

    private static final int CARD_DAMAGE = 1;

    @Override
    protected CardAbilityTask getPlayAction(GameState gameState, int transactionId) {
        return new CardAbilityTask() {
            @Override
            public void run(GameState gameState) {
                gameState.getAllCards()
                        .stream()
                        .filter(c -> c.getPosition() == CardPosition.Position.Battlefield && c.getType() == CardType.Unit)
                        .forEach(c -> c.changeDefence(-CARD_DAMAGE));
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }

            @Override
            public void runCanceled(GameState gameState) {
                gameState.moveCardToGraveyard(parentCard.getInstanceId());
            }
        };
    }

}
