package com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities;

import com.MMOCardGame.GameServer.GameEngine.Cards.CardAbilityTask;
import com.MMOCardGame.GameServer.GameEngine.Cards.CardScript;
import com.MMOCardGame.GameServer.GameEngine.Cards.Script;
import com.MMOCardGame.GameServer.GameEngine.GameState;

@Script(ids = 11, type= Script.ScriptType.NamedAbility)
public class SkanPodatnosciScript extends CardScript {

    private static final int ATTACK_INC = 1;

    @Override
    protected CardAbilityTask getAbilityAction(GameState gameState, int transactionId) {
        return e -> parentCard.changeAttack(ATTACK_INC);
    }

}
