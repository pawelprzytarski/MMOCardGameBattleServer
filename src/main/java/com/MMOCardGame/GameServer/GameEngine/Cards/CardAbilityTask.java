package com.MMOCardGame.GameServer.GameEngine.Cards;

import com.MMOCardGame.GameServer.GameEngine.GameState;

public interface CardAbilityTask {
    void run(GameState gameState);

    default void runCanceled(GameState gameState) {
    }

    default int getAbilityId() {
        return -1;
    }
}
