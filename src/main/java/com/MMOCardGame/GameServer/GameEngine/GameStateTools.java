package com.MMOCardGame.GameServer.GameEngine;

import com.MMOCardGame.GameServer.GameEngine.Cards.Card;
import com.MMOCardGame.GameServer.GameEngine.Enums.CardPosition;
import com.MMOCardGame.GameServer.common.Pair;

import java.util.List;
import java.util.stream.Collectors;

public abstract class GameStateTools {
    public static List<Card> getPlayerCardsFromLibrary(int player, GameState gameState) {
        CardPosition.Position position = CardPosition.Position.Library;
        return getPlayerCardsFromPosition(player, gameState, position);
    }

    public static List<Card> getPlayerCardsFromPosition(int player, GameState gameState, CardPosition.Position position) {
        return gameState.getCardsFieldForPlayer(player).getAllCardsPositions().stream()
                .filter(cardPositionPair -> cardPositionPair.getSecond() == position)
                .map(Pair::getFirst)
                .collect(Collectors.toList());
    }
}
