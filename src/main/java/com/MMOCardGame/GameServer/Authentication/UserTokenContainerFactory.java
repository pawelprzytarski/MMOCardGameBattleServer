package com.MMOCardGame.GameServer.Authentication;

import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.Authentication.impl.InMemoryUserTokenContainer;
import dagger.Module;
import dagger.Provides;

@Module
public class UserTokenContainerFactory {
    static UserTokenContainer tokenContainer;

    @Provides
    static UserTokenContainer getUserTokenContainer(AppConfiguration configuration) {
        if (tokenContainer == null)
            tokenContainer = new InMemoryUserTokenContainer(configuration);
        return tokenContainer;
    }
}


