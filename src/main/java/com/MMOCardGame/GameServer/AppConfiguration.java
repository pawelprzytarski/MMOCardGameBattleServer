package com.MMOCardGame.GameServer;

import com.MMOCardGame.GameServer.exceptions.ArgumentParsingException;
import lombok.Data;
import org.apache.commons.cli.CommandLine;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public abstract class AppConfiguration {
    protected Settings settings;
    private String callbackUrl = null;
    private String callbackToken = null;

    public AppConfiguration() {
        settings = new Settings();
        loadDefaultSettings();
    }

    protected AppConfiguration(Settings settings) {
        this.settings = settings;
    }

    private void loadDefaultSettings() {
        settings.userServerPort = 1410;
        settings.serviceServerPort = 1914;
        settings.volatileResourcesLifeDuration = 10;
        settings.innerServiceToken = UUID.randomUUID().toString();
        settings.cardScriptsPackages = Arrays.asList("com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards",
                "com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities");
        settings.cardsPath = "./cards.json";
        settings.heroInfoPath = "./players.json";
    }

    protected void initEmptyData() {
        if (settings.userServerPort == 0)
            settings.userServerPort = 1410;
        if (settings.serviceServerPort == 0)
            settings.serviceServerPort = 1914;
        if (settings.volatileResourcesLifeDuration == 0)
            settings.volatileResourcesLifeDuration = 10;
        if (settings.cardScriptsPackages == null)
            settings.cardScriptsPackages = Arrays.asList("com.MMOCardGame.GameServer.GameEngine.Cards.scripts.cards",
                    "com.MMOCardGame.GameServer.GameEngine.Cards.scripts.abilities");
        if (settings.cardsPath == null)
            settings.cardsPath = "./cards.json";
        if (settings.heroInfoPath == null)
            settings.heroInfoPath = "./players.json";
    }

    public abstract void initFromConsoleParams(CommandLine commandLine) throws ArgumentParsingException;

    public int getServiceServerPort() {
        return settings.serviceServerPort;
    }

    public int getUserServerPort() {
        return settings.userServerPort;
    }

    public int getVolatileResourcesLifeDuration() {
        return settings.volatileResourcesLifeDuration;
    }

    public String getInnerServiceToken() {
        return settings.innerServiceToken;
    }

    public String getCallbackUrl() {
        if (callbackUrl == null)
            return GameConstants.getCallbackUrl();
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackToken() {
        return callbackToken;
    }

    public void setCallbackToken(String callbackToken) {
        this.callbackToken = callbackToken;
    }

    public String getCardsPath() {
        return settings.cardsPath;
    }

    public Collection<String> getCardScriptsPackages() {
        return settings.cardScriptsPackages;
    }

    public String getHeroInfoPath() {
        return settings.heroInfoPath;
    }

    @Data
    protected static class Settings {
        public int userServerPort;
        public int serviceServerPort;
        public int volatileResourcesLifeDuration;
        public String innerServiceToken;
        public String cardsPath;
        public List<String> cardScriptsPackages;
        public String heroInfoPath;
    }
}
