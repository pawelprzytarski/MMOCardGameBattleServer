package com.MMOCardGame.GameServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class CallbackRestRequestSender {
    private final Logger logger = LoggerFactory.getLogger(CallbackRestRequestSender.class);
    private final AppConfiguration configuration;

    public CallbackRestRequestSender(AppConfiguration configuration) {
        this.configuration = configuration;
    }

    public void sendCallbackInfo(String json) {
        if (configuration.getCallbackUrl() == null || configuration.getCallbackUrl().length() == 0)
            return;
        try {
            trySendCallbackInfo(json);
        } catch (IOException e) {
            logger.error("Sending callback info error", e);
        }
    }

    private void trySendCallbackInfo(String json) throws IOException {
        HttpURLConnection http = getPreparedHttpConnection(json);
        sendJson(json, http);
    }

    private void sendJson(String json, HttpURLConnection http) throws IOException {
        try (OutputStream os = http.getOutputStream()) {
            os.write(json.getBytes(StandardCharsets.UTF_8));
        } finally {
            http.disconnect();
        }
    }

    private HttpURLConnection getPreparedHttpConnection(String json) throws IOException {
        HttpURLConnection http = getHttpURLConnection();
        http.setRequestMethod("PUT");
        http.setDoOutput(true);
        http.setFixedLengthStreamingMode(json.length());
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        return http;
    }

    private HttpURLConnection getHttpURLConnection() throws IOException {
        URL url = new URL(configuration.getCallbackUrl());
        URLConnection con = url.openConnection();
        return (HttpURLConnection) con;
    }
}
