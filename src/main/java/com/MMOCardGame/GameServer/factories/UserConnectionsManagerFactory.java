package com.MMOCardGame.GameServer.factories;

import com.MMOCardGame.GameServer.Authentication.UserTokenContainer;
import com.MMOCardGame.GameServer.Authentication.UserTokenContainerFactory;
import com.MMOCardGame.GameServer.Servers.UserConnection;
import com.MMOCardGame.GameServer.Servers.UserConnectionsManager;
import com.MMOCardGame.GameServer.Servers.impl.GrpcUserConnection;
import dagger.Component;
import dagger.Module;
import dagger.Provides;

@Component(modules = {UserConnectionMangerProvider.class, AppConfigurationProvider.class, UserTokenContainerFactory.class})
public interface UserConnectionsManagerFactory {
    UserConnectionsManager getUserConnectionManager();
}

@Module
class UserConnectionMangerProvider {
    @Provides
    UserConnectionsManager getUserConnectionManager(UserTokenContainer tokenContainer) {
        return new UserConnectionsManager(tokenContainer) {
            @Override
            protected UserConnection createImplementationOfConnection(String username) {
                return new GrpcUserConnection(username);
            }
        };
    }
}
