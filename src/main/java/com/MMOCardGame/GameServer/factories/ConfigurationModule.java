package com.MMOCardGame.GameServer.factories;

import com.MMOCardGame.GameServer.AppConfiguration;
import dagger.Component;

@Component(modules = AppConfigurationProvider.class)
public interface ConfigurationModule {
    AppConfiguration getAppConfiguration();
}
