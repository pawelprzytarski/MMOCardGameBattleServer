package com.MMOCardGame.GameServer.factories;


import com.MMOCardGame.GameServer.AppConfiguration;
import com.MMOCardGame.GameServer.impl.AppConfigurationCommandLineParsing;
import com.MMOCardGame.GameServer.impl.AppConfigurationJsonInit;
import dagger.Module;
import dagger.Provides;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Module
public class AppConfigurationProvider {
    static AppConfiguration configuration = null;
    private static Logger logger = LoggerFactory.getLogger(AppConfigurationProvider.class);

    @Provides
    static AppConfiguration getAppConfiguration() {
        if (configuration == null) {
            try {
                configuration = new AppConfigurationJsonInit();
            } catch (Throwable e) {
                logger.debug("No existing default config file");
                configuration = new AppConfigurationCommandLineParsing();
            }
        }
        return configuration;
    }
}