package com.MMOCardGame.GameServer.common;

public interface TaskWithReturn<R> {
    R run();
}
