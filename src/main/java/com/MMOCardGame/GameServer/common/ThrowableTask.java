package com.MMOCardGame.GameServer.common;

public interface ThrowableTask {
    void run() throws Throwable;
}
