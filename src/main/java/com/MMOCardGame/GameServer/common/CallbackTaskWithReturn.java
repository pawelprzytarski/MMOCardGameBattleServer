package com.MMOCardGame.GameServer.common;

public interface CallbackTaskWithReturn<R, T> {
    R run(T obj);
}

