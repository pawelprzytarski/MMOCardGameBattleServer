package com.MMOCardGame.GameServer.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Pair<V, K> implements Serializable {
    V first;
    K second;
}
