package com.MMOCardGame.GameServer.common;

import com.google.common.reflect.ClassPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ReflectionHelper {
    Class<?> annotationType;
    Set<ClassPath.ClassInfo> classInfoSet;
    Logger logger = LoggerFactory.getLogger(ReflectionHelper.class);

    public ReflectionHelper(String packageName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        ClassPath classPath = null;
        try {
            classPath = ClassPath.from(classLoader);
        } catch (IOException e) {
            logger.error("Cannot create ClassPath", e);
            throw new RuntimeException(e);
        }
        classInfoSet = classPath.getTopLevelClassesRecursive(packageName);
    }

    public Set<Class<?>> findAnnatatedTypes(Class<?> annotationType) {
        this.annotationType = annotationType;
        return classInfoSet.parallelStream().map(ClassPath.ClassInfo::load)
                .filter(this::isScriptAnnotated)
                .collect(Collectors.toSet());
    }

    private boolean isScriptAnnotated(Class<?> aClass) {
        return Arrays.stream(aClass.getAnnotations()).anyMatch(annotation -> annotation.annotationType().equals(annotationType));
    }
}
