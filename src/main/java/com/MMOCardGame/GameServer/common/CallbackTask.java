package com.MMOCardGame.GameServer.common;

public interface CallbackTask<T> {
    void run(T obj);
}

