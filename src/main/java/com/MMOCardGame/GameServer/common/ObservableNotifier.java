package com.MMOCardGame.GameServer.common;

import java.util.LinkedHashSet;
import java.util.Set;

public class ObservableNotifier<E> implements Observable<E> {
    private Set<Observer<E>> observers = new LinkedHashSet<>();

    @Override
    public synchronized void addObserver(Observer<E> observer) {
        observers.add(observer);
    }

    @Override
    public synchronized void removeObserver(Observer<E> observer) {
        observers.remove(observer);
    }

    public void notifyAll(E object) {
        Object[] array;

        synchronized (this) {
            array = observers.toArray();
        }

        for (Object observer : array) {
            ((Observer<E>) observer).update(object);
        }
    }

    public synchronized void deleteObservers() {
        observers.clear();
    }

    @Override
    public synchronized int countObservers() {
        return observers.size();
    }

    @Override
    public synchronized void addObserverAtBegin(Observer<E> gamePhaseChangedObserver) {
        Set<Observer<E>> newObservers = new LinkedHashSet<>();
        newObservers.add(gamePhaseChangedObserver);
        newObservers.addAll(observers);
        observers = newObservers;
    }
}

